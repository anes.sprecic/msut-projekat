#include "stm32f4xx.h"
#include "usart.h"
#include "delay.h"
#include "mp45dt02.h"
#include "pdm_filter.h"
#include "cs43l22.h"
#include "math.h"
#include "string.h"

#define IRQ_IDLE			0
#define IRQ_DETECTED		1
#define IRQ_WAIT4LOW		2
#define IRQ_DEBOUNCE		3
#define ADC_BUFF_SIZE			(DAC_BUFF_SIZE)

#define SAMPLE_HIST_SIZE 24000



enum {
	OPEN,
	CLOSED,
	CLOSING
} gate = OPEN;

#define PI 					3.14159
#define DAC_BUFF_SIZE		2

void getData4DAC();
void serviceIRQA(void);
volatile uint16_t g_dac_buff[DAC_BUFF_SIZE];
volatile uint16_t buff0[DAC_BUFF_SIZE];
volatile uint16_t buff1[DAC_BUFF_SIZE];

volatile uint16_t samples[SAMPLE_HIST_SIZE];
volatile int counter = 0;

volatile int g_data_ready = 0;


// DERVISA
volatile uint32_t g_irq_cnt = 0;
volatile uint8_t g_gpioa_irq_state = (IRQ_IDLE);
volatile uint32_t g_irq_timer = 0;
uint16_t arrayLED1[10] = {0xFFF0,0xFFF9,0xFFF4,0xFFF0,0xFFF9,0xFFF2,0xFFF2,0xFFF8,0xFFF0,0xFFF0}; //PD
uint16_t arrayLED2[10] = {0xFFFC,0xFFFF,0xFFFA,0xFFFB,0xFFF9,0xFFF9,0xFFF8,0xFFFF,0xFFF8,0xFFF9}; //PE
uint16_t glob = 0;
volatile uint32_t change_effect = 0;
void 		initADC3(uint16_t * glob);
void initADC3(uint16_t * glob){
	
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN; // ADC3 PC0 KANAL 10
	RCC->APB2ENR |= RCC_APB2ENR_ADC3EN;
	GPIOC->MODER |= GPIO_MODER_MODER0; 
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR0;
	
	ADC3->CR1 = 0x03000800;	// 6-bit
	
	ADC3->SQR3 = 0x0000000A;
	ADC3->CR2 = 0x00000200;
	ADC->CCR = 0x00000000;	// 42MHz
	ADC3->CR2 |= ADC_CR2_ADON; 	
	
	
	// TIM2 for ADC3
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN; 								// enable TIM2 
	TIM2->PSC = 0x0064-0x0001;											
																		// 84MHz/100 = 840kHz 
	TIM2->ARR = 0x0054;													
	TIM2->CR1 = 0x0084;													// ARPE On, CMS disable, Up counting
																		// UEV disable
	TIM2->CR2 = 0x0000;
	TIM2->CR2 = TIM_CR2_MMS_1;											// Update event is used to as trigger output (TRGO)

	TIM2->EGR |= TIM_EGR_UG;											// update event
	TIM2->CR1 |= TIM_CR1_CEN;	
	
	
	// ADC with DMA
	ADC3->CR2 = ADC_CR2_DMA;											// enable DMA
	ADC3->CR2 |= ADC_CR2_DDS;											// enable DDS
	ADC3->CR2 |= ADC_CR2_EXTEN_0;										
	ADC3->CR2 |= (ADC_CR2_EXTSEL_2) | (ADC_CR2_EXTSEL_1);				// TIM2 TRIG								
	ADC3->CR2 |= ADC_CR2_ADON;
	
	
	// DMA
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;									 
	
	DMA2_Stream1->CR = 0x00000000;										
	while((DMA2_Stream1->CR & DMA_SxCR_EN) == DMA_SxCR_EN);				 
	
	DMA2->LISR = 0x00000000;	
	DMA2->HISR = 0x00000000;	
	
	DMA2_Stream1->PAR = (uint32_t)&ADC3->DR;							 
	DMA2_Stream1->M0AR = (uint32_t)glob;								 
	DMA2_Stream1->NDTR = 1;											 
	
	DMA2_Stream1->CR |= DMA_SxCR_CHSEL_1;								
	DMA2_Stream1->CR |= DMA_SxCR_PL;									 																	
	DMA2_Stream1->CR |= DMA_SxCR_MINC;									 
																																		
	DMA2_Stream1->CR |= DMA_SxCR_CIRC;									
	DMA2_Stream1->CR |= DMA_SxCR_PSIZE_0;	
																		
	DMA2_Stream1->CR |= DMA_SxCR_MSIZE_0;								
																		
	DMA2_Stream1->CR &= ~DMA_SxCR_DIR;									  
																		 
	DMA2_Stream1->CR |= DMA_SxCR_EN;	
	
}

// DERVISA

void initADC1(void)
{/// initialize ADC on PA1 -> ADC123_IN1 (pin 23 on chip)
	//wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	// ADC initialization
	//------------------------------------------------------------------ 
	
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN; 								/** GPIOA Periph clock enable */	
	RCC->APB2ENR |= RCC_APB2ENR_ADC2EN; 								/** ADC1 Periph clock enable */	
	GPIOA->MODER |= GPIO_MODER_MODER1;  								/** Set Analog mode on pin 1 */
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR1; 								/** No pullup or pulldown */
	
	ADC2->CR1 = 0x00000800;												// 12-bit resolution (15 ADCCLK cycles), 1 channel
																		// discontinious mode on regular channels
																		// single conversion mode
	ADC2->SMPR1 = 0x00000000;											// conversion time 3 cycles of 84MHz clock!
	ADC2->SQR1 = 0x000000000;											// select one channel in regular sequence
	ADC2->SQR3 = 0x000000001;											// select channel 1
	ADC2->CR2 =  0x000000200;
	ADC->CCR |= 0x00030000;												// ADC clock is (0.6,14MHz) 84/8 = 14MHz!!!
}

uint16_t getADC1(void)
{
	uint16_t r_val;
	
	ADC2->CR2 |= ADC_CR2_ADON;											// turn on the ADC
	ADC2->CR2 |= ADC_CR2_SWSTART;										// start conversion of regular channels
	
	while((ADC2->SR & ADC_SR_EOC) != ADC_SR_EOC);						// wait for regular channel to complete the conversion
	
	r_val = ADC2->DR;													// get the data

	ADC2->CR2 &= ~ADC_CR2_ADON;											// turn off the ADC
	return r_val; 
}

void initDmaADC1(uint16_t * dBuff1, uint16_t * dBuff2, uint16_t size)
{
	///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	/// Init ADCs
	/// ADC1 -> PC5 IN15
	///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;								/** GPIOC Periph clock enable */
	
	GPIOC->MODER |= GPIO_MODER_MODER5;									/** Set Analog mode on pin 5 */
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR5; 								/** No pullup or pulldown */

	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN; 								/** ADC1 Periph clock enable */					
					
	
	ADC1->CR1 = 0x00000800;												// 12-bit resolution (15 ADCCLK cycles), 1 channel
	//ADC1->CR1 = ADC_CR1_RES|ADC_CR1_DISCEN;								// 6 bit resolution (9 ADCCLK cycles),1 channel	
																		// Discontinious mode on regular channels
																		// single conversion mode
	ADC1->SMPR1 = 0x00000000;											// conversion time 3 cycles of 84MHz clock!
	ADC1->SMPR2 = 0x00000000;											// conversion time 3 cycles of 84MHz clock!
	ADC1->SQR1 = 0x000000000;											// select one channel in regular sequence
	ADC1->SQR2 = 0x000000000;
	ADC1->SQR3 = 0x00000000F;											// select channel 15
	ADC1->CR2 =  0x000000200;
	
	ADC->CCR |= 0x00010000;												// ADC clock 84/4 = 21MHz!
	
	
	///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	/// setup TIM3 for ADC1 TRGO event
	///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN; 								// enable TIM3 
	// 50khz sample rate
	TIM3->PSC = 7 - 1;											// set TIM3 counting prescaler to 42 (p406)
	
	// setup tim4 in slave mode to trigger interupt every 256 samples
	// trigger interupt every 256 samples (5ms worth of samples)
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;

	TIM4->CCMR2 = 0b01;
	TIM4->CCER |= 0b1010;
	TIM4->SMCR = 0b0100111;
	TIM4->EGR |= TIM_EGR_UG;
	TIM4->CR1 |= TIM_CR1_CEN;

	TIM4->DIER = 0b1;
	NVIC_SetPriority(TIM4_IRQn, 0);
	NVIC_EnableIRQ(TIM4_IRQn);
	TIM4->ARR = size;													// reload value set to 10 kHz!
	// TIM4

	TIM3->ARR = 125;

	TIM3->CR1 = 0x0084;													// ARPE On, CMS disable, Up counting
																		// UEV disable, TIM3 enable(p392)
	TIM3->CR2 = 0x0000;
	TIM3->CR2 |= TIM_CR2_MMS_1;											// Update event is used to as trigger output (TRGO)


	TIM3->EGR |= TIM_EGR_UG;											// update event, reload all config p363
	TIM3->CR1 |= TIM_CR1_CEN;											// start counter

	///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	/// Setup ADCs with DMA
	///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	ADC1->CR2 = ADC_CR2_DMA;											// enable DMA
	ADC1->CR2 |= ADC_CR2_DDS;											// enable DDS
	ADC1->CR2 |= ADC_CR2_EXTEN_0;										// trigger detection on the rising edge
	ADC1->CR2 |= (ADC_CR2_EXTSEL_3);									// 1000: Timer 3 TRGO event
	ADC1->CR2 |= ADC_CR2_ADON;
	///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	/// Setup DMA2 controller for ADC1 p179
	///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;									// enable DMA2 clock
	
	DMA2_Stream0->CR = 0x00000000;										// disable stream 0 (ADC1)
	while((DMA2_Stream0->CR & DMA_SxCR_EN) == DMA_SxCR_EN);				// wait until the DMA transfer is completed
	
	DMA2->LISR = 0x00000000;	
	DMA2->HISR = 0x00000000;	
	
	DMA2_Stream0->PAR = (uint32_t)&ADC1->DR;							// peripheral port register address (ADC1)
	DMA2_Stream0->M0AR = (uint32_t)dBuff1;								// memory address of first buffer
	DMA2_Stream0->M1AR = (uint32_t)dBuff2;								// memory address of first buffer
	DMA2_Stream0->NDTR = size;											// number of samples to write
	
	DMA2_Stream0->CR &= ~DMA_SxCR_CHSEL;								// select channel 0 for ADC1
	DMA2_Stream0->CR |= DMA_SxCR_PL;									// select stream priority to very high
																		// - DMA is flow controller
																		// - Peripheral address pointer is fixed																		
	DMA2_Stream0->CR |= DMA_SxCR_MINC;									// Memory address pointer is incremented
																		// in accordance with the memory data size																	
	DMA2_Stream0->CR |= DMA_SxCR_DBM;									// Double buffer mode
	DMA2_Stream0->CR |= DMA_SxCR_PSIZE_0;								// Peripheral data size:
																		// - Half Word 16-bit
	DMA2_Stream0->CR |= DMA_SxCR_MSIZE_0;								// Memory data size:
																		// - Half Word 16-bit
	DMA2_Stream0->CR &= ~DMA_SxCR_DIR;									// Data transfer direction: 
																		// - 00 -> Peripheral-to-memory
	DMA2_Stream0->CR |= DMA_SxCR_EN;									// enable stream 0 (ADC1)
}
int main(void)
{
	initUSART2(USART2_BAUDRATE_460800);
	printUSART2("\nwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\n");
	printUSART2("w STM32F407 - CS43L22 Audio DAC I2S demo");
	printUSART2("\nwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\n");
	
	initDmaADC1(buff0, buff1, ADC_BUFF_SIZE);	

	// DERVISA
	initSYSTIMER();
	initADC3(&glob);
	
	// --------- PD0-PD3 pins for 7 segment display
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
	GPIOD->MODER |= 0x00000055;
	GPIOD->ODR |= 0x0000FFF0;
	GPIOD->OTYPER |= 0x00000000;
	GPIOD->OSPEEDR |= 0x000000FF;
	//--------- PE0-PE2 pins for 7 segment display
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOEEN;
	GPIOE->MODER |= 0x00000015;
	GPIOE->ODR |= 0x0000FFFC;
	GPIOE->OTYPER |= 0x00000000;
	GPIOE->OSPEEDR |= 0x0000003F;
	
	// --------- interrupt configuration - pin PE7 i PE8
	GPIOE->MODER &= ~0x0003C000;  	
	GPIOE->PUPDR |= 0x00028000;
	
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;								// enable clock on SYSCFG register
	NVIC_EnableIRQ(EXTI9_5_IRQn);
	SYSCFG->EXTICR[1] = SYSCFG_EXTICR2_EXTI7_PE;						// select PE7 as interrupt source p259
	SYSCFG->EXTICR[2] |= SYSCFG_EXTICR3_EXTI8_PE;						// select PE8 as interrupt source p259	
	EXTI->IMR = EXTI_IMR_MR7 | EXTI_IMR_MR8;							// enable interrupt on EXTI_Line7 and EXTI_Line8
	EXTI->EMR &= ~(EXTI_EMR_MR7) | ~(EXTI_EMR_MR8);						// disable event on EXTI_Line7 and EXTI_Line8
	EXTI->RTSR = EXTI_RTSR_TR7 | EXTI_RTSR_TR8;	
	EXTI->FTSR = 0x00000000;	

	// DERVISA

	initADC1();
	
	
	initCS43L22(99, 48000, (uint16_t *)g_dac_buff, DAC_BUFF_SIZE);
	
	printUSART2("-> SYS: init completed\n");
	uint32_t k = 0;
	float wet = 0.5;
	float feedback = 0.3;
	int c;
	double G = 1.5;
	for(c = 0; c < SAMPLE_HIST_SIZE; c++)
		samples[c] = (uint16_t)0;
	double up = 1.0;
	double down = 1.02;
	double drive = 70;
	double f = 500;
	uint16_t noise_gate = 3500;
	uint16_t tmp_buff[ADC_BUFF_SIZE];
	while(1)
	{	
		if(g_data_ready)
		{
			g_data_ready = 0;
			if((DMA2_Stream0->CR & DMA_SxCR_CT) == DMA_SxCR_CT)
            {
				memcpy(tmp_buff, buff1, ADC_BUFF_SIZE * 2);
            }
            else {
				memcpy(tmp_buff, buff0, ADC_BUFF_SIZE * 2);
            }
			// printUSART2("%f\n", gain);
			int i = 0;
			for(i = 0; i < ADC_BUFF_SIZE; i++){
				uint16_t tmp = tmp_buff[i];
				// do noise_gate with threshold
				// and decay time
				
				// TREMOLO
				if(change_effect == 5){
					float val = (((int16_t)(tmp)) - 2048)/2048.;
					static int i = 0;
					float new_val = val * (0.9 + 0.6*sinf(i/4000));
					i++;
					tmp = (new_val * 2048) + 2048;

				}

				if(change_effect == 1){
					float val = (((int16_t)(tmp)) - 2048)/2048.;
					// G oko 1
					G = glob/5. + 1;
					float new_val = 1 - exp(-fabs(G*val));
					new_val *= 3;
					//drive = 80;
					// drive 50-100
					//float a = G*sin(((drive+1)/102)*(PI/2));
					//float k = 2*a/(1-a);
					//float new_val = ((1+k)*val);
					tmp = (new_val * 2048) + 2048;
				}

				if(change_effect == 2){
					float val = (((int16_t)(tmp)) - 2048)/2048.;
					// drive = 30;
					// drive 0 -> 20
					drive = glob/3.;
					f = drive * -0.5;
					float new_val = f * (val*(abs(val)+drive)/(val*val + (drive-1)*abs(val) + 1));
					new_val /= 3;
					tmp = (new_val * 2048) + 2048;
				}
		
				// DELAY
				if(change_effect == 3)
				{
					uint16_t delayed_sample = samples[counter];
					feedback = glob/60.;
					if(tmp > 10)
						samples[counter] = (1-feedback)*tmp + feedback*delayed_sample;
					else 
						samples[counter] = 0;

					counter = (counter+1)%SAMPLE_HIST_SIZE;
					static double fl_gain = 1;
					static double fl = 0.9;
					if(delayed_sample != 0)
						tmp = ((1.0-wet)*tmp + wet * delayed_sample);	
				}
				// FLANGER
				if(change_effect == 6)
				{
					static double flange_counter = 0;
					uint16_t delayed_sample = samples[(int)floor(SAMPLE_HIST_SIZE/2 + SAMPLE_HIST_SIZE/2 * sin(flange_counter))];

					if(tmp > 10)
						samples[counter] = 0.5*tmp + 0.5*delayed_sample;
					else 
						samples[counter] = 0;

					flange_counter += 0.00005;
					if(flange_counter >= 2*PI)
						flange_counter = 0;

					counter = (counter+1)%SAMPLE_HIST_SIZE;
					static double fl_gain = 1;
					static double fl = 0.5;
					if(delayed_sample != 0)
						tmp = ((1.0-fl)*1.2*tmp + fl*fl_gain*delayed_sample);	
				}
				// HELICOPTER EASTER EGG
				if(change_effect == 10)
				{
					static double flange_counter = 0;
					uint16_t delayed_sample = samples[(int)floor(SAMPLE_HIST_SIZE/2 + SAMPLE_HIST_SIZE/2 * sin(glob*flange_counter))];

					if(tmp > 10)
						samples[counter] = (1.0 - feedback) * tmp + feedback * delayed_sample;
					else 
						samples[counter] = 0;

					flange_counter += 0.000010;
					if(flange_counter >= 2*PI)
						flange_counter = 0;

					counter = (counter+1)%SAMPLE_HIST_SIZE;

					if(delayed_sample != 0)
						tmp = ((1.0-wet) * tmp + wet * delayed_sample);	
				}
				// DELAY

				if(change_effect == 4){
					float val = (((int16_t)(tmp)) - 2048)/2048.;

					static float ylpast = 0;
					static float ybpast = 0;
					static float F1 = 0;
					static float Q1 = 0.1;
					float Fc = (1500-127)*(glob/120.0)+127;
					float yh = val - ylpast - Q1 * ybpast;                                
					float yb = F1 * yh + ybpast;
					float yl = F1 * yb + ylpast;
					
					F1 = (float)(2 * sinf((3.1415926535* Fc) / (float)48000));
					
					ybpast = yb;
					ylpast = yl;
					float new_val = yb*1.5;

					tmp = (new_val * 2048) + 2048;

				}

				// NOISE GATE
				static int sample_count = 0;
				if(1)
				{

				switch(gate){
					case CLOSED:
						if(tmp > noise_gate){
							gate = OPEN;
						}
						tmp = 0;
					break;
					case CLOSING:
						if(tmp > noise_gate){
							gate = OPEN;
						}else if(tmp <= noise_gate){
							sample_count++;
						}
						if(sample_count >= 200){
							gate = CLOSED;
							sample_count = 0;
						}
					break;
					case OPEN:
						if(tmp < noise_gate){
							gate = CLOSING;
							sample_count = 0;
						}
					break;
				}
				}
				tmp_buff[i] = tmp;
			}
			memcpy(g_dac_buff, tmp_buff, ADC_BUFF_SIZE * 2);
		}
		serviceIRQA();
	}
}
void TIM4_IRQHandler(void)
{
	g_data_ready = 1;
	TIM4->SR = 0x0000;
}

// DERVISA

void EXTI9_5_IRQHandler(void)
{
	if((EXTI->PR & EXTI_PR_PR7) == EXTI_PR_PR7)							// EXTI_Line7 interrupt pending?
	{
		if(g_gpioa_irq_state == (IRQ_IDLE))
		{
			g_gpioa_irq_state = (IRQ_DETECTED);
			if(change_effect == 9) 										// for 9 possible effects
			{
				change_effect = 0;
			}
			else change_effect++;
			GPIOD->ODR = arrayLED1[change_effect];
			GPIOE->ODR = arrayLED2[change_effect];
		}
		EXTI->PR = EXTI_PR_PR7;											// clear EXTI_Line0 interrupt flag
	}
	
	if((EXTI->PR & EXTI_PR_PR8) == EXTI_PR_PR8)							// EXTI_Line8 interrupt pending?
	{
		if(g_gpioa_irq_state == (IRQ_IDLE))
		{
			g_gpioa_irq_state = (IRQ_DETECTED);
			if(change_effect == 0) 										// for 9 possible effects
			{
				change_effect = 9;
			}
			else change_effect--;
			GPIOD->ODR = arrayLED1[change_effect];
			GPIOE->ODR = arrayLED2[change_effect];
		}
		EXTI->PR = EXTI_PR_PR8;											// clear EXTI_Line0 interrupt flag
	}
}


void serviceIRQA(void)
{
	switch(g_gpioa_irq_state)
	{
		case(IRQ_IDLE):
		{
			break;
		}
		case(IRQ_DETECTED):
		{
			g_irq_cnt++;
			g_gpioa_irq_state = (IRQ_WAIT4LOW); 
			break;
		}
		case(IRQ_WAIT4LOW):
		{
			if((GPIOE->IDR & 0x0180) == 0x0000) // 7th and 8th pin 
			{
				g_gpioa_irq_state = (IRQ_DEBOUNCE);
				g_irq_timer = getSYSTIMER(); 
			}
			break;
		}
		case(IRQ_DEBOUNCE):
		{
			if(chk4TimeoutSYSTIMER(g_irq_timer, 500000) == (SYSTIMER_TIMEOUT))
			{
				g_gpioa_irq_state = (IRQ_IDLE); 
			}
		}
		default:
		{
			break;
		}
	}
}

// DERVISA
