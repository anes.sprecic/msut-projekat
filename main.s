	.syntax unified
	.cpu cortex-m4
	.eabi_attribute 27, 3
	.eabi_attribute 28, 1
	.fpu fpv4-sp-d16
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.thumb
	.file	"main.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.align	2
	.thumb
	.thumb_func
	.type	NVIC_EnableIRQ, %function
NVIC_EnableIRQ:
.LFB95:
	.file 1 "/home/anes/msut/STM32F407/Libraries/CMSIS/Include/core_cm4.h"
	.loc 1 1073 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	sub	sp, sp, #12
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	mov	r3, r0
	strb	r3, [r7, #7]
	.loc 1 1075 0
	ldr	r1, .L2
	ldrsb	r3, [r7, #7]
	lsrs	r3, r3, #5
	ldrb	r2, [r7, #7]	@ zero_extendqisi2
	and	r2, r2, #31
	movs	r0, #1
	lsl	r2, r0, r2
	str	r2, [r1, r3, lsl #2]
	.loc 1 1076 0
	adds	r7, r7, #12
	.cfi_def_cfa_offset 4
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L3:
	.align	2
.L2:
	.word	-536813312
	.cfi_endproc
.LFE95:
	.size	NVIC_EnableIRQ, .-NVIC_EnableIRQ
	.align	2
	.thumb
	.thumb_func
	.type	NVIC_SetPriority, %function
NVIC_SetPriority:
.LFB101:
	.loc 1 1158 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	sub	sp, sp, #12
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	mov	r3, r0
	str	r1, [r7]
	strb	r3, [r7, #7]
	.loc 1 1159 0
	ldrsb	r3, [r7, #7]
	cmp	r3, #0
	bge	.L5
	.loc 1 1160 0
	ldr	r1, .L7
	ldrb	r3, [r7, #7]	@ zero_extendqisi2
	and	r3, r3, #15
	subs	r3, r3, #4
	ldr	r2, [r7]
	uxtb	r2, r2
	lsls	r2, r2, #4
	uxtb	r2, r2
	add	r3, r3, r1
	strb	r2, [r3, #24]
	b	.L4
.L5:
	.loc 1 1162 0
	ldr	r1, .L7+4
	ldrsb	r3, [r7, #7]
	ldr	r2, [r7]
	uxtb	r2, r2
	lsls	r2, r2, #4
	uxtb	r2, r2
	add	r3, r3, r1
	strb	r2, [r3, #768]
.L4:
	.loc 1 1163 0
	adds	r7, r7, #12
	.cfi_def_cfa_offset 4
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L8:
	.align	2
.L7:
	.word	-536810240
	.word	-536813312
	.cfi_endproc
.LFE101:
	.size	NVIC_SetPriority, .-NVIC_SetPriority
	.comm	dev_addr,1,1
	.global	gate
	.bss
	.type	gate, %object
	.size	gate, 1
gate:
	.space	1
	.comm	g_dac_buff,4,4
	.comm	buff0,4,4
	.comm	buff1,4,4
	.comm	samples,48000,4
	.global	counter
	.align	2
	.type	counter, %object
	.size	counter, 4
counter:
	.space	4
	.global	g_data_ready
	.align	2
	.type	g_data_ready, %object
	.size	g_data_ready, 4
g_data_ready:
	.space	4
	.global	g_irq_cnt
	.align	2
	.type	g_irq_cnt, %object
	.size	g_irq_cnt, 4
g_irq_cnt:
	.space	4
	.global	g_gpioa_irq_state
	.type	g_gpioa_irq_state, %object
	.size	g_gpioa_irq_state, 1
g_gpioa_irq_state:
	.space	1
	.global	g_irq_timer
	.align	2
	.type	g_irq_timer, %object
	.size	g_irq_timer, 4
g_irq_timer:
	.space	4
	.global	arrayLED1
	.data
	.align	2
	.type	arrayLED1, %object
	.size	arrayLED1, 20
arrayLED1:
	.short	-16
	.short	-7
	.short	-12
	.short	-16
	.short	-7
	.short	-14
	.short	-14
	.short	-8
	.short	-16
	.short	-16
	.global	arrayLED2
	.align	2
	.type	arrayLED2, %object
	.size	arrayLED2, 20
arrayLED2:
	.short	-4
	.short	-1
	.short	-6
	.short	-5
	.short	-7
	.short	-7
	.short	-8
	.short	-1
	.short	-8
	.short	-7
	.global	glob
	.bss
	.align	1
	.type	glob, %object
	.size	glob, 2
glob:
	.space	2
	.global	change_effect
	.align	2
	.type	change_effect, %object
	.size	change_effect, 4
change_effect:
	.space	4
	.text
	.align	2
	.global	initADC3
	.thumb
	.thumb_func
	.type	initADC3, %function
initADC3:
.LFB110:
	.file 2 "main.c"
	.loc 2 50 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	sub	sp, sp, #12
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #4]
	.loc 2 52 0
	ldr	r2, .L11
	ldr	r3, .L11
	ldr	r3, [r3, #48]
	orr	r3, r3, #4
	str	r3, [r2, #48]
	.loc 2 53 0
	ldr	r2, .L11
	ldr	r3, .L11
	ldr	r3, [r3, #68]
	orr	r3, r3, #1024
	str	r3, [r2, #68]
	.loc 2 54 0
	ldr	r2, .L11+4
	ldr	r3, .L11+4
	ldr	r3, [r3]
	orr	r3, r3, #3
	str	r3, [r2]
	.loc 2 55 0
	ldr	r2, .L11+4
	ldr	r3, .L11+4
	ldr	r3, [r3, #12]
	bic	r3, r3, #3
	str	r3, [r2, #12]
	.loc 2 57 0
	ldr	r3, .L11+8
	ldr	r2, .L11+12
	str	r2, [r3, #4]
	.loc 2 59 0
	ldr	r3, .L11+8
	movs	r2, #10
	str	r2, [r3, #52]
	.loc 2 60 0
	ldr	r3, .L11+8
	mov	r2, #512
	str	r2, [r3, #8]
	.loc 2 61 0
	ldr	r3, .L11+16
	movs	r2, #0
	str	r2, [r3, #4]
	.loc 2 62 0
	ldr	r2, .L11+8
	ldr	r3, .L11+8
	ldr	r3, [r3, #8]
	orr	r3, r3, #1
	str	r3, [r2, #8]
	.loc 2 66 0
	ldr	r2, .L11
	ldr	r3, .L11
	ldr	r3, [r3, #64]
	orr	r3, r3, #1
	str	r3, [r2, #64]
	.loc 2 67 0
	mov	r3, #1073741824
	movs	r2, #99
	strh	r2, [r3, #40]	@ movhi
	.loc 2 69 0
	mov	r3, #1073741824
	movs	r2, #84
	str	r2, [r3, #44]
	.loc 2 70 0
	mov	r3, #1073741824
	movs	r2, #132
	strh	r2, [r3]	@ movhi
	.loc 2 72 0
	mov	r3, #1073741824
	movs	r2, #0
	strh	r2, [r3, #4]	@ movhi
	.loc 2 73 0
	mov	r3, #1073741824
	movs	r2, #32
	strh	r2, [r3, #4]	@ movhi
	.loc 2 75 0
	mov	r2, #1073741824
	mov	r3, #1073741824
	ldrh	r3, [r3, #20]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #1
	uxth	r3, r3
	strh	r3, [r2, #20]	@ movhi
	.loc 2 76 0
	mov	r2, #1073741824
	mov	r3, #1073741824
	ldrh	r3, [r3]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #1
	uxth	r3, r3
	strh	r3, [r2]	@ movhi
	.loc 2 80 0
	ldr	r3, .L11+8
	mov	r2, #256
	str	r2, [r3, #8]
	.loc 2 81 0
	ldr	r2, .L11+8
	ldr	r3, .L11+8
	ldr	r3, [r3, #8]
	orr	r3, r3, #512
	str	r3, [r2, #8]
	.loc 2 82 0
	ldr	r2, .L11+8
	ldr	r3, .L11+8
	ldr	r3, [r3, #8]
	orr	r3, r3, #268435456
	str	r3, [r2, #8]
	.loc 2 83 0
	ldr	r2, .L11+8
	ldr	r3, .L11+8
	ldr	r3, [r3, #8]
	orr	r3, r3, #100663296
	str	r3, [r2, #8]
	.loc 2 84 0
	ldr	r2, .L11+8
	ldr	r3, .L11+8
	ldr	r3, [r3, #8]
	orr	r3, r3, #1
	str	r3, [r2, #8]
	.loc 2 88 0
	ldr	r2, .L11
	ldr	r3, .L11
	ldr	r3, [r3, #48]
	orr	r3, r3, #4194304
	str	r3, [r2, #48]
	.loc 2 90 0
	ldr	r3, .L11+20
	movs	r2, #0
	str	r2, [r3]
	.loc 2 91 0
	nop
.L10:
	.loc 2 91 0 is_stmt 0 discriminator 1
	ldr	r3, .L11+20
	ldr	r3, [r3]
	and	r3, r3, #1
	cmp	r3, #0
	bne	.L10
	.loc 2 93 0 is_stmt 1
	ldr	r3, .L11+24
	movs	r2, #0
	str	r2, [r3]
	.loc 2 94 0
	ldr	r3, .L11+24
	movs	r2, #0
	str	r2, [r3, #4]
	.loc 2 96 0
	ldr	r3, .L11+20
	ldr	r2, .L11+28
	str	r2, [r3, #8]
	.loc 2 97 0
	ldr	r2, .L11+20
	ldr	r3, [r7, #4]
	str	r3, [r2, #12]
	.loc 2 98 0
	ldr	r3, .L11+20
	movs	r2, #1
	str	r2, [r3, #4]
	.loc 2 100 0
	ldr	r2, .L11+20
	ldr	r3, .L11+20
	ldr	r3, [r3]
	orr	r3, r3, #67108864
	str	r3, [r2]
	.loc 2 101 0
	ldr	r2, .L11+20
	ldr	r3, .L11+20
	ldr	r3, [r3]
	orr	r3, r3, #196608
	str	r3, [r2]
	.loc 2 102 0
	ldr	r2, .L11+20
	ldr	r3, .L11+20
	ldr	r3, [r3]
	orr	r3, r3, #1024
	str	r3, [r2]
	.loc 2 104 0
	ldr	r2, .L11+20
	ldr	r3, .L11+20
	ldr	r3, [r3]
	orr	r3, r3, #256
	str	r3, [r2]
	.loc 2 105 0
	ldr	r2, .L11+20
	ldr	r3, .L11+20
	ldr	r3, [r3]
	orr	r3, r3, #2048
	str	r3, [r2]
	.loc 2 107 0
	ldr	r2, .L11+20
	ldr	r3, .L11+20
	ldr	r3, [r3]
	orr	r3, r3, #8192
	str	r3, [r2]
	.loc 2 109 0
	ldr	r2, .L11+20
	ldr	r3, .L11+20
	ldr	r3, [r3]
	bic	r3, r3, #192
	str	r3, [r2]
	.loc 2 111 0
	ldr	r2, .L11+20
	ldr	r3, .L11+20
	ldr	r3, [r3]
	orr	r3, r3, #1
	str	r3, [r2]
	.loc 2 113 0
	adds	r7, r7, #12
	.cfi_def_cfa_offset 4
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L12:
	.align	2
.L11:
	.word	1073887232
	.word	1073874944
	.word	1073816064
	.word	50333696
	.word	1073816320
	.word	1073898536
	.word	1073898496
	.word	1073816140
	.cfi_endproc
.LFE110:
	.size	initADC3, .-initADC3
	.align	2
	.global	initADC1
	.thumb
	.thumb_func
	.type	initADC1, %function
initADC1:
.LFB111:
	.loc 2 118 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 2 123 0
	ldr	r2, .L14
	ldr	r3, .L14
	ldr	r3, [r3, #48]
	orr	r3, r3, #1
	str	r3, [r2, #48]
	.loc 2 124 0
	ldr	r2, .L14
	ldr	r3, .L14
	ldr	r3, [r3, #68]
	orr	r3, r3, #512
	str	r3, [r2, #68]
	.loc 2 125 0
	ldr	r2, .L14+4
	ldr	r3, .L14+4
	ldr	r3, [r3]
	orr	r3, r3, #12
	str	r3, [r2]
	.loc 2 126 0
	ldr	r2, .L14+4
	ldr	r3, .L14+4
	ldr	r3, [r3, #12]
	bic	r3, r3, #12
	str	r3, [r2, #12]
	.loc 2 128 0
	ldr	r3, .L14+8
	mov	r2, #2048
	str	r2, [r3, #4]
	.loc 2 131 0
	ldr	r3, .L14+8
	movs	r2, #0
	str	r2, [r3, #12]
	.loc 2 132 0
	ldr	r3, .L14+8
	movs	r2, #0
	str	r2, [r3, #44]
	.loc 2 133 0
	ldr	r3, .L14+8
	movs	r2, #1
	str	r2, [r3, #52]
	.loc 2 134 0
	ldr	r3, .L14+8
	mov	r2, #512
	str	r2, [r3, #8]
	.loc 2 135 0
	ldr	r2, .L14+12
	ldr	r3, .L14+12
	ldr	r3, [r3, #4]
	orr	r3, r3, #196608
	str	r3, [r2, #4]
	.loc 2 136 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L15:
	.align	2
.L14:
	.word	1073887232
	.word	1073872896
	.word	1073815808
	.word	1073816320
	.cfi_endproc
.LFE111:
	.size	initADC1, .-initADC1
	.align	2
	.global	getADC1
	.thumb
	.thumb_func
	.type	getADC1, %function
getADC1:
.LFB112:
	.loc 2 139 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	sub	sp, sp, #12
	.cfi_def_cfa_offset 16
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 2 142 0
	ldr	r2, .L19
	ldr	r3, .L19
	ldr	r3, [r3, #8]
	orr	r3, r3, #1
	str	r3, [r2, #8]
	.loc 2 143 0
	ldr	r2, .L19
	ldr	r3, .L19
	ldr	r3, [r3, #8]
	orr	r3, r3, #1073741824
	str	r3, [r2, #8]
	.loc 2 145 0
	nop
.L17:
	.loc 2 145 0 is_stmt 0 discriminator 1
	ldr	r3, .L19
	ldr	r3, [r3]
	and	r3, r3, #2
	cmp	r3, #0
	beq	.L17
	.loc 2 147 0 is_stmt 1
	ldr	r3, .L19
	ldr	r3, [r3, #76]
	strh	r3, [r7, #6]	@ movhi
	.loc 2 149 0
	ldr	r2, .L19
	ldr	r3, .L19
	ldr	r3, [r3, #8]
	bic	r3, r3, #1
	str	r3, [r2, #8]
	.loc 2 150 0
	ldrh	r3, [r7, #6]
	.loc 2 151 0
	mov	r0, r3
	adds	r7, r7, #12
	.cfi_def_cfa_offset 4
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L20:
	.align	2
.L19:
	.word	1073815808
	.cfi_endproc
.LFE112:
	.size	getADC1, .-getADC1
	.align	2
	.global	initDmaADC1
	.thumb
	.thumb_func
	.type	initDmaADC1, %function
initDmaADC1:
.LFB113:
	.loc 2 154 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #16
	.cfi_def_cfa_offset 24
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r0, [r7, #12]
	str	r1, [r7, #8]
	mov	r3, r2
	strh	r3, [r7, #6]	@ movhi
	.loc 2 159 0
	ldr	r2, .L23
	ldr	r3, .L23
	ldr	r3, [r3, #48]
	orr	r3, r3, #4
	str	r3, [r2, #48]
	.loc 2 161 0
	ldr	r2, .L23+4
	ldr	r3, .L23+4
	ldr	r3, [r3]
	orr	r3, r3, #3072
	str	r3, [r2]
	.loc 2 162 0
	ldr	r2, .L23+4
	ldr	r3, .L23+4
	ldr	r3, [r3, #12]
	bic	r3, r3, #3072
	str	r3, [r2, #12]
	.loc 2 164 0
	ldr	r2, .L23
	ldr	r3, .L23
	ldr	r3, [r3, #68]
	orr	r3, r3, #256
	str	r3, [r2, #68]
	.loc 2 167 0
	ldr	r3, .L23+8
	mov	r2, #2048
	str	r2, [r3, #4]
	.loc 2 171 0
	ldr	r3, .L23+8
	movs	r2, #0
	str	r2, [r3, #12]
	.loc 2 172 0
	ldr	r3, .L23+8
	movs	r2, #0
	str	r2, [r3, #16]
	.loc 2 173 0
	ldr	r3, .L23+8
	movs	r2, #0
	str	r2, [r3, #44]
	.loc 2 174 0
	ldr	r3, .L23+8
	movs	r2, #0
	str	r2, [r3, #48]
	.loc 2 175 0
	ldr	r3, .L23+8
	movs	r2, #15
	str	r2, [r3, #52]
	.loc 2 176 0
	ldr	r3, .L23+8
	mov	r2, #512
	str	r2, [r3, #8]
	.loc 2 178 0
	ldr	r2, .L23+12
	ldr	r3, .L23+12
	ldr	r3, [r3, #4]
	orr	r3, r3, #65536
	str	r3, [r2, #4]
	.loc 2 184 0
	ldr	r2, .L23
	ldr	r3, .L23
	ldr	r3, [r3, #64]
	orr	r3, r3, #2
	str	r3, [r2, #64]
	.loc 2 186 0
	ldr	r3, .L23+16
	movs	r2, #6
	strh	r2, [r3, #40]	@ movhi
	.loc 2 190 0
	ldr	r2, .L23
	ldr	r3, .L23
	ldr	r3, [r3, #64]
	orr	r3, r3, #4
	str	r3, [r2, #64]
	.loc 2 192 0
	ldr	r3, .L23+20
	movs	r2, #1
	strh	r2, [r3, #28]	@ movhi
	.loc 2 193 0
	ldr	r2, .L23+20
	ldr	r3, .L23+20
	ldrh	r3, [r3, #32]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #10
	uxth	r3, r3
	strh	r3, [r2, #32]	@ movhi
	.loc 2 194 0
	ldr	r3, .L23+20
	movs	r2, #39
	strh	r2, [r3, #8]	@ movhi
	.loc 2 195 0
	ldr	r2, .L23+20
	ldr	r3, .L23+20
	ldrh	r3, [r3, #20]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #1
	uxth	r3, r3
	strh	r3, [r2, #20]	@ movhi
	.loc 2 196 0
	ldr	r2, .L23+20
	ldr	r3, .L23+20
	ldrh	r3, [r3]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #1
	uxth	r3, r3
	strh	r3, [r2]	@ movhi
	.loc 2 198 0
	ldr	r3, .L23+20
	movs	r2, #1
	strh	r2, [r3, #12]	@ movhi
	.loc 2 199 0
	movs	r0, #30
	movs	r1, #0
	bl	NVIC_SetPriority
	.loc 2 200 0
	movs	r0, #30
	bl	NVIC_EnableIRQ
	.loc 2 201 0
	ldr	r2, .L23+20
	ldrh	r3, [r7, #6]
	str	r3, [r2, #44]
	.loc 2 204 0
	ldr	r3, .L23+16
	movs	r2, #125
	str	r2, [r3, #44]
	.loc 2 206 0
	ldr	r3, .L23+16
	movs	r2, #132
	strh	r2, [r3]	@ movhi
	.loc 2 208 0
	ldr	r3, .L23+16
	movs	r2, #0
	strh	r2, [r3, #4]	@ movhi
	.loc 2 209 0
	ldr	r2, .L23+16
	ldr	r3, .L23+16
	ldrh	r3, [r3, #4]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #32
	uxth	r3, r3
	strh	r3, [r2, #4]	@ movhi
	.loc 2 212 0
	ldr	r2, .L23+16
	ldr	r3, .L23+16
	ldrh	r3, [r3, #20]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #1
	uxth	r3, r3
	strh	r3, [r2, #20]	@ movhi
	.loc 2 213 0
	ldr	r2, .L23+16
	ldr	r3, .L23+16
	ldrh	r3, [r3]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #1
	uxth	r3, r3
	strh	r3, [r2]	@ movhi
	.loc 2 218 0
	ldr	r3, .L23+8
	mov	r2, #256
	str	r2, [r3, #8]
	.loc 2 219 0
	ldr	r2, .L23+8
	ldr	r3, .L23+8
	ldr	r3, [r3, #8]
	orr	r3, r3, #512
	str	r3, [r2, #8]
	.loc 2 220 0
	ldr	r2, .L23+8
	ldr	r3, .L23+8
	ldr	r3, [r3, #8]
	orr	r3, r3, #268435456
	str	r3, [r2, #8]
	.loc 2 221 0
	ldr	r2, .L23+8
	ldr	r3, .L23+8
	ldr	r3, [r3, #8]
	orr	r3, r3, #134217728
	str	r3, [r2, #8]
	.loc 2 222 0
	ldr	r2, .L23+8
	ldr	r3, .L23+8
	ldr	r3, [r3, #8]
	orr	r3, r3, #1
	str	r3, [r2, #8]
	.loc 2 226 0
	ldr	r2, .L23
	ldr	r3, .L23
	ldr	r3, [r3, #48]
	orr	r3, r3, #4194304
	str	r3, [r2, #48]
	.loc 2 228 0
	ldr	r3, .L23+24
	movs	r2, #0
	str	r2, [r3]
	.loc 2 229 0
	nop
.L22:
	.loc 2 229 0 is_stmt 0 discriminator 1
	ldr	r3, .L23+24
	ldr	r3, [r3]
	and	r3, r3, #1
	cmp	r3, #0
	bne	.L22
	.loc 2 231 0 is_stmt 1
	ldr	r3, .L23+28
	movs	r2, #0
	str	r2, [r3]
	.loc 2 232 0
	ldr	r3, .L23+28
	movs	r2, #0
	str	r2, [r3, #4]
	.loc 2 234 0
	ldr	r3, .L23+24
	ldr	r2, .L23+32
	str	r2, [r3, #8]
	.loc 2 235 0
	ldr	r2, .L23+24
	ldr	r3, [r7, #12]
	str	r3, [r2, #12]
	.loc 2 236 0
	ldr	r2, .L23+24
	ldr	r3, [r7, #8]
	str	r3, [r2, #16]
	.loc 2 237 0
	ldr	r2, .L23+24
	ldrh	r3, [r7, #6]
	str	r3, [r2, #4]
	.loc 2 239 0
	ldr	r2, .L23+24
	ldr	r3, .L23+24
	ldr	r3, [r3]
	bic	r3, r3, #234881024
	str	r3, [r2]
	.loc 2 240 0
	ldr	r2, .L23+24
	ldr	r3, .L23+24
	ldr	r3, [r3]
	orr	r3, r3, #196608
	str	r3, [r2]
	.loc 2 243 0
	ldr	r2, .L23+24
	ldr	r3, .L23+24
	ldr	r3, [r3]
	orr	r3, r3, #1024
	str	r3, [r2]
	.loc 2 245 0
	ldr	r2, .L23+24
	ldr	r3, .L23+24
	ldr	r3, [r3]
	orr	r3, r3, #262144
	str	r3, [r2]
	.loc 2 246 0
	ldr	r2, .L23+24
	ldr	r3, .L23+24
	ldr	r3, [r3]
	orr	r3, r3, #2048
	str	r3, [r2]
	.loc 2 248 0
	ldr	r2, .L23+24
	ldr	r3, .L23+24
	ldr	r3, [r3]
	orr	r3, r3, #8192
	str	r3, [r2]
	.loc 2 250 0
	ldr	r2, .L23+24
	ldr	r3, .L23+24
	ldr	r3, [r3]
	bic	r3, r3, #192
	str	r3, [r2]
	.loc 2 252 0
	ldr	r2, .L23+24
	ldr	r3, .L23+24
	ldr	r3, [r3]
	orr	r3, r3, #1
	str	r3, [r2]
	.loc 2 253 0
	adds	r7, r7, #16
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L24:
	.align	2
.L23:
	.word	1073887232
	.word	1073874944
	.word	1073815552
	.word	1073816320
	.word	1073742848
	.word	1073743872
	.word	1073898512
	.word	1073898496
	.word	1073815628
	.cfi_endproc
.LFE113:
	.size	initDmaADC1, .-initDmaADC1
	.section	.rodata
	.align	2
.LC0:
	.ascii	"\012wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
	.ascii	"wwww\012\000"
	.align	2
.LC1:
	.ascii	"w STM32F407 - CS43L22 Audio DAC I2S demo\000"
	.align	2
.LC2:
	.ascii	"-> SYS: init completed\012\000"
	.global	__aeabi_f2d
	.global	__aeabi_dmul
	.global	__aeabi_dsub
	.global	__aeabi_d2f
	.global	__aeabi_i2d
	.global	__aeabi_dadd
	.global	__aeabi_ddiv
	.global	__aeabi_d2iz
	.global	__aeabi_dcmpge
	.global	__aeabi_d2uiz
	.text
	.align	2
	.global	main
	.thumb
	.thumb_func
	.type	main, %function
main:
.LFB114:
	.loc 2 255 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 136
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r4, r5, r7, r8, r9, r10, fp, lr}
	.cfi_def_cfa_offset 32
	.cfi_offset 4, -32
	.cfi_offset 5, -28
	.cfi_offset 7, -24
	.cfi_offset 8, -20
	.cfi_offset 9, -16
	.cfi_offset 10, -12
	.cfi_offset 11, -8
	.cfi_offset 14, -4
	fstmfdd	sp!, {d8}
	.cfi_def_cfa_offset 40
	.cfi_offset 80, -40
	.cfi_offset 81, -36
	sub	sp, sp, #136
	.cfi_def_cfa_offset 176
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 2 256 0
	movs	r0, #91
	bl	initUSART2
	.loc 2 257 0
	ldr	r0, .L64+8
	bl	printUSART2
	.loc 2 258 0
	ldr	r0, .L64+12
	bl	printUSART2
	.loc 2 259 0
	ldr	r0, .L64+8
	bl	printUSART2
	.loc 2 261 0
	ldr	r0, .L64+16
	ldr	r1, .L64+20
	movs	r2, #2
	bl	initDmaADC1
	.loc 2 264 0
	bl	initSYSTIMER
	.loc 2 265 0
	ldr	r0, .L64+24
	bl	initADC3
	.loc 2 268 0
	ldr	r2, .L64+28
	ldr	r3, .L64+28
	ldr	r3, [r3, #48]
	orr	r3, r3, #8
	str	r3, [r2, #48]
	.loc 2 269 0
	ldr	r2, .L64+32
	ldr	r3, .L64+32
	ldr	r3, [r3]
	orr	r3, r3, #85
	str	r3, [r2]
	.loc 2 270 0
	ldr	r2, .L64+32
	ldr	r3, .L64+32
	ldr	r3, [r3, #20]
	orr	r3, r3, #65280
	orr	r3, r3, #240
	str	r3, [r2, #20]
	.loc 2 271 0
	ldr	r2, .L64+32
	ldr	r3, .L64+32
	ldr	r3, [r3, #4]
	str	r3, [r2, #4]
	.loc 2 272 0
	ldr	r2, .L64+32
	ldr	r3, .L64+32
	ldr	r3, [r3, #8]
	orr	r3, r3, #255
	str	r3, [r2, #8]
	.loc 2 274 0
	ldr	r2, .L64+28
	ldr	r3, .L64+28
	ldr	r3, [r3, #48]
	orr	r3, r3, #16
	str	r3, [r2, #48]
	.loc 2 275 0
	ldr	r2, .L64+36
	ldr	r3, .L64+36
	ldr	r3, [r3]
	orr	r3, r3, #21
	str	r3, [r2]
	.loc 2 276 0
	ldr	r2, .L64+36
	ldr	r3, .L64+36
	ldr	r3, [r3, #20]
	orr	r3, r3, #65280
	orr	r3, r3, #252
	str	r3, [r2, #20]
	.loc 2 277 0
	ldr	r2, .L64+36
	ldr	r3, .L64+36
	ldr	r3, [r3, #4]
	str	r3, [r2, #4]
	.loc 2 278 0
	ldr	r2, .L64+36
	ldr	r3, .L64+36
	ldr	r3, [r3, #8]
	orr	r3, r3, #63
	str	r3, [r2, #8]
	.loc 2 281 0
	ldr	r2, .L64+36
	ldr	r3, .L64+36
	ldr	r3, [r3]
	bic	r3, r3, #245760
	str	r3, [r2]
	.loc 2 282 0
	ldr	r2, .L64+36
	ldr	r3, .L64+36
	ldr	r3, [r3, #12]
	orr	r3, r3, #163840
	str	r3, [r2, #12]
	.loc 2 284 0
	ldr	r2, .L64+28
	ldr	r3, .L64+28
	ldr	r3, [r3, #68]
	orr	r3, r3, #16384
	str	r3, [r2, #68]
	.loc 2 285 0
	movs	r0, #23
	bl	NVIC_EnableIRQ
	.loc 2 286 0
	ldr	r3, .L64+40
	mov	r2, #16384
	str	r2, [r3, #12]
	.loc 2 287 0
	ldr	r2, .L64+40
	ldr	r3, .L64+40
	ldr	r3, [r3, #16]
	orr	r3, r3, #4
	str	r3, [r2, #16]
	.loc 2 288 0
	ldr	r3, .L64+44
	mov	r2, #384
	str	r2, [r3]
	.loc 2 289 0
	ldr	r2, .L64+44
	ldr	r3, .L64+44
	ldr	r3, [r3, #4]
	str	r3, [r2, #4]
	.loc 2 290 0
	ldr	r3, .L64+44
	mov	r2, #384
	str	r2, [r3, #8]
	.loc 2 291 0
	ldr	r3, .L64+44
	movs	r2, #0
	str	r2, [r3, #12]
	.loc 2 295 0
	bl	initADC1
	.loc 2 298 0
	movs	r0, #99
	movw	r1, #48000
	ldr	r2, .L64+48
	movs	r3, #2
	bl	initCS43L22
	.loc 2 300 0
	ldr	r0, .L64+52
	bl	printUSART2
	.loc 2 301 0
	movs	r3, #0
	str	r3, [r7, #116]
	.loc 2 302 0
	mov	r3, #1056964608
	str	r3, [r7, #112]	@ float
	.loc 2 303 0
	ldr	r3, .L64+56
	str	r3, [r7, #132]	@ float
	.loc 2 305 0
	mov	r2, #0
	ldr	r3, .L64+60
	strd	r2, [r7, #104]
	.loc 2 306 0
	movs	r3, #0
	str	r3, [r7, #128]
	b	.L26
.L27:
	.loc 2 307 0 discriminator 3
	ldr	r2, .L64+64
	ldr	r3, [r7, #128]
	movs	r1, #0
	strh	r1, [r2, r3, lsl #1]	@ movhi
	.loc 2 306 0 discriminator 3
	ldr	r3, [r7, #128]
	adds	r3, r3, #1
	str	r3, [r7, #128]
.L26:
	.loc 2 306 0 is_stmt 0 discriminator 1
	ldr	r3, [r7, #128]
	movw	r2, #23999
	cmp	r3, r2
	ble	.L27
	.loc 2 308 0 is_stmt 1
	mov	r2, #0
	ldr	r3, .L64+68
	strd	r2, [r7, #96]
	.loc 2 309 0
	adr	r3, .L64
	ldrd	r2, [r3]
	strd	r2, [r7, #88]
	.loc 2 310 0
	mov	r2, #0
	ldr	r3, .L64+72
	strd	r2, [r7, #80]
	.loc 2 311 0
	mov	r2, #0
	ldr	r3, .L64+76
	strd	r2, [r7, #72]
	.loc 2 312 0
	movw	r3, #3500
	strh	r3, [r7, #70]	@ movhi
.L59:
	.loc 2 316 0
	ldr	r3, .L64+80
	ldr	r3, [r3]
	cmp	r3, #0
	beq	.L28
.LBB2:
	.loc 2 318 0
	ldr	r3, .L64+80
	movs	r2, #0
	str	r2, [r3]
	.loc 2 319 0
	ldr	r3, .L64+84
	ldr	r3, [r3]
	and	r3, r3, #524288
	cmp	r3, #0
	beq	.L29
	.loc 2 321 0
	ldr	r2, .L64+20
	add	r3, r7, #8
	ldr	r0, [r2]	@ unaligned
	str	r0, [r3]	@ unaligned
	b	.L30
.L29:
	.loc 2 324 0
	ldr	r2, .L64+16
	add	r3, r7, #8
	ldr	r0, [r2]	@ unaligned
	str	r0, [r3]	@ unaligned
.L30:
	.loc 2 327 0
	movs	r3, #0
	str	r3, [r7, #124]
	.loc 2 328 0
	movs	r3, #0
	str	r3, [r7, #124]
	b	.L31
.L65:
	.align	3
.L64:
	.word	-2147483648
	.word	1072714219
	.word	.LC0
	.word	.LC1
	.word	buff0
	.word	buff1
	.word	glob
	.word	1073887232
	.word	1073875968
	.word	1073876992
	.word	1073821696
	.word	1073822720
	.word	g_dac_buff
	.word	.LC2
	.word	1050253722
	.word	1073217536
	.word	samples
	.word	1072693248
	.word	1079083008
	.word	1082081280
	.word	g_data_ready
	.word	1073898512
.L58:
.LBB3:
	.loc 2 329 0
	ldr	r3, [r7, #124]
	lsls	r3, r3, #1
	add	r2, r7, #136
	add	r3, r3, r2
	ldrh	r3, [r3, #-128]	@ movhi
	strh	r3, [r7, #122]	@ movhi
	.loc 2 334 0
	ldr	r3, .L66
	ldr	r3, [r3]
	cmp	r3, #5
	bne	.L32
.LBB4:
	.loc 2 335 0
	ldrh	r3, [r7, #122]
	sxth	r3, r3
	sub	r3, r3, #2048
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	flds	s14, .L66+4
	fdivs	s15, s15, s14
	fsts	s15, [r7, #64]
	.loc 2 337 0
	ldr	r3, .L66+8
	ldr	r3, [r3]
	ldr	r2, .L66+12
	smull	r1, r2, r2, r3
	asrs	r2, r2, #8
	asrs	r3, r3, #31
	subs	r3, r2, r3
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	fcpys	s0, s15
	bl	sinf
	fcpys	s14, s0
	flds	s15, .L66+16
	fmuls	s15, s14, s15
	flds	s14, .L66+20
	fadds	s14, s15, s14
	flds	s15, [r7, #64]
	fmuls	s15, s14, s15
	fsts	s15, [r7, #60]
	.loc 2 338 0
	ldr	r3, .L66+8
	ldr	r3, [r3]
	adds	r3, r3, #1
	ldr	r2, .L66+8
	str	r3, [r2]
	.loc 2 339 0
	flds	s15, [r7, #60]
	flds	s14, .L66+4
	fmuls	s15, s15, s14
	flds	s14, .L66+4
	fadds	s15, s15, s14
	ftouizs	s15, s15
	fsts	s15, [r7]	@ int
	ldrh	r3, [r7]	@ movhi
	strh	r3, [r7, #122]	@ movhi
.L32:
.LBE4:
	.loc 2 343 0
	ldr	r3, .L66
	ldr	r3, [r3]
	cmp	r3, #1
	bne	.L33
.LBB5:
	.loc 2 344 0
	ldrh	r3, [r7, #122]
	sxth	r3, r3
	sub	r3, r3, #2048
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	flds	s14, .L66+4
	fdivs	s15, s15, s14
	fsts	s15, [r7, #56]
	.loc 2 346 0
	ldr	r3, .L66+24
	ldrh	r3, [r3]
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	fconsts	s14, #20
	fdivs	s15, s15, s14
	fconsts	s14, #112
	fadds	s15, s15, s14
	fmrs	r0, s15
	bl	__aeabi_f2d
	mov	r2, r0
	mov	r3, r1
	strd	r2, [r7, #104]
	.loc 2 347 0
	ldr	r0, [r7, #56]	@ float
	bl	__aeabi_f2d
	mov	r2, r0
	mov	r3, r1
	mov	r0, r2
	mov	r1, r3
	ldrd	r2, [r7, #104]
	bl	__aeabi_dmul
	mov	r2, r0
	mov	r3, r1
	mov	r4, r2
	bic	r5, r3, #-2147483648
	mov	r8, r4
	eor	r9, r5, #-2147483648
	fmdrr	d0, r8, r9
	bl	exp
	fmrrd	r2, r3, d0
	mov	r0, #0
	ldr	r1, .L66+28
	bl	__aeabi_dsub
	mov	r2, r0
	mov	r3, r1
	mov	r0, r2
	mov	r1, r3
	bl	__aeabi_d2f
	mov	r3, r0	@ float
	str	r3, [r7, #52]	@ float
	.loc 2 348 0
	flds	s15, [r7, #52]
	fconsts	s14, #8
	fmuls	s15, s15, s14
	fsts	s15, [r7, #52]
	.loc 2 354 0
	flds	s15, [r7, #52]
	flds	s14, .L66+4
	fmuls	s15, s15, s14
	flds	s14, .L66+4
	fadds	s15, s15, s14
	ftouizs	s15, s15
	fsts	s15, [r7]	@ int
	ldrh	r3, [r7]	@ movhi
	strh	r3, [r7, #122]	@ movhi
.L33:
.LBE5:
	.loc 2 357 0
	ldr	r3, .L66
	ldr	r3, [r3]
	cmp	r3, #2
	bne	.L34
.LBB6:
	.loc 2 358 0
	ldrh	r3, [r7, #122]
	sxth	r3, r3
	sub	r3, r3, #2048
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	flds	s14, .L66+4
	fdivs	s15, s15, s14
	fsts	s15, [r7, #48]
	.loc 2 361 0
	ldr	r3, .L66+24
	ldrh	r3, [r3]
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	fconsts	s14, #8
	fdivs	s15, s15, s14
	fmrs	r0, s15
	bl	__aeabi_f2d
	mov	r2, r0
	mov	r3, r1
	strd	r2, [r7, #80]
	.loc 2 362 0
	ldrd	r0, [r7, #80]
	mov	r2, #0
	ldr	r3, .L66+32
	bl	__aeabi_dmul
	mov	r2, r0
	mov	r3, r1
	strd	r2, [r7, #72]
	.loc 2 363 0
	ldr	r0, [r7, #48]	@ float
	bl	__aeabi_f2d
	mov	r10, r0
	mov	fp, r1
	ldr	r0, [r7, #48]	@ float
	bl	__aeabi_f2d
	mov	r2, r0
	mov	r3, r1
	fmdrr	d0, r2, r3
	bl	abs
	mov	r3, r0
	mov	r0, r3
	bl	__aeabi_i2d
	mov	r2, r0
	mov	r3, r1
	mov	r0, r2
	mov	r1, r3
	ldrd	r2, [r7, #80]
	bl	__aeabi_dadd
	mov	r2, r0
	mov	r3, r1
	mov	r0, r10
	mov	r1, fp
	bl	__aeabi_dmul
	mov	r2, r0
	mov	r3, r1
	mov	r10, r2
	mov	fp, r3
	flds	s14, [r7, #48]
	flds	s15, [r7, #48]
	fmuls	s15, s14, s15
	fmrs	r0, s15
	bl	__aeabi_f2d
	strd	r0, [r7]
	ldrd	r0, [r7, #80]
	mov	r2, #0
	ldr	r3, .L66+28
	bl	__aeabi_dsub
	mov	r2, r0
	mov	r3, r1
	fmdrr	d8, r2, r3
	ldr	r0, [r7, #48]	@ float
	bl	__aeabi_f2d
	mov	r2, r0
	mov	r3, r1
	fmdrr	d0, r2, r3
	bl	abs
	mov	r3, r0
	mov	r0, r3
	bl	__aeabi_i2d
	mov	r2, r0
	mov	r3, r1
	fmrrd	r0, r1, d8
	bl	__aeabi_dmul
	mov	r2, r0
	mov	r3, r1
	ldrd	r0, [r7]
	bl	__aeabi_dadd
	mov	r2, r0
	mov	r3, r1
	mov	r0, r2
	mov	r1, r3
	mov	r2, #0
	ldr	r3, .L66+28
	bl	__aeabi_dadd
	mov	r2, r0
	mov	r3, r1
	mov	r0, r10
	mov	r1, fp
	bl	__aeabi_ddiv
	mov	r2, r0
	mov	r3, r1
	mov	r0, r2
	mov	r1, r3
	ldrd	r2, [r7, #72]
	bl	__aeabi_dmul
	mov	r2, r0
	mov	r3, r1
	mov	r0, r2
	mov	r1, r3
	bl	__aeabi_d2f
	mov	r3, r0	@ float
	str	r3, [r7, #44]	@ float
	.loc 2 364 0
	flds	s15, [r7, #44]
	fconsts	s14, #8
	fdivs	s15, s15, s14
	fsts	s15, [r7, #44]
	.loc 2 365 0
	flds	s15, [r7, #44]
	flds	s14, .L66+4
	fmuls	s15, s15, s14
	flds	s14, .L66+4
	fadds	s15, s15, s14
	ftouizs	s15, s15
	fsts	s15, [r7]	@ int
	ldrh	r3, [r7]	@ movhi
	strh	r3, [r7, #122]	@ movhi
.L34:
.LBE6:
	.loc 2 369 0
	ldr	r3, .L66
	ldr	r3, [r3]
	cmp	r3, #3
	bne	.L35
.LBB7:
	.loc 2 371 0
	ldr	r3, .L66+36
	ldr	r3, [r3]
	ldr	r2, .L66+40
	ldrh	r3, [r2, r3, lsl #1]	@ movhi
	strh	r3, [r7, #42]	@ movhi
	.loc 2 372 0
	ldr	r3, .L66+24
	ldrh	r3, [r3]
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	flds	s14, .L66+44
	fdivs	s15, s15, s14
	fsts	s15, [r7, #132]
	.loc 2 373 0
	ldrh	r3, [r7, #122]
	cmp	r3, #10
	bls	.L36
	.loc 2 374 0
	ldr	r3, .L66+36
	ldr	r3, [r3]
	fconsts	s14, #112
	flds	s15, [r7, #132]
	fsubs	s14, s14, s15
	ldrh	r2, [r7, #122]
	fmsr	s15, r2	@ int
	fsitos	s15, s15
	fmuls	s14, s14, s15
	ldrh	r2, [r7, #42]
	fmsr	s15, r2	@ int
	fsitos	s13, s15
	flds	s15, [r7, #132]
	fmuls	s15, s13, s15
	fadds	s15, s14, s15
	ftouizs	s15, s15
	fsts	s15, [r7]	@ int
	ldrh	r2, [r7]	@ movhi
	uxth	r1, r2
	ldr	r2, .L66+40
	strh	r1, [r2, r3, lsl #1]	@ movhi
	b	.L37
.L67:
	.align	2
.L66:
	.word	change_effect
	.word	1157627904
	.word	i.7094
	.word	274877907
	.word	1058642330
	.word	1063675494
	.word	glob
	.word	1072693248
	.word	-1075838976
	.word	counter
	.word	samples
	.word	1114636288
.L36:
	.loc 2 376 0
	ldr	r3, .L68+24
	ldr	r3, [r3]
	ldr	r2, .L68+28
	movs	r1, #0
	strh	r1, [r2, r3, lsl #1]	@ movhi
.L37:
	.loc 2 378 0
	ldr	r3, .L68+24
	ldr	r3, [r3]
	adds	r2, r3, #1
	ldr	r3, .L68+32
	smull	r1, r3, r3, r2
	asrs	r1, r3, #9
	asrs	r3, r2, #31
	subs	r3, r1, r3
	movw	r1, #24000
	mul	r3, r1, r3
	subs	r3, r2, r3
	ldr	r2, .L68+24
	str	r3, [r2]
	.loc 2 381 0
	ldrh	r3, [r7, #42]
	cmp	r3, #0
	beq	.L35
	.loc 2 382 0
	fconsts	s14, #112
	flds	s15, [r7, #112]
	fsubs	s14, s14, s15
	ldrh	r3, [r7, #122]
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	fmuls	s14, s14, s15
	ldrh	r3, [r7, #42]
	fmsr	s15, r3	@ int
	fsitos	s13, s15
	flds	s15, [r7, #112]
	fmuls	s15, s13, s15
	fadds	s15, s14, s15
	ftouizs	s15, s15
	fsts	s15, [r7]	@ int
	ldrh	r3, [r7]	@ movhi
	strh	r3, [r7, #122]	@ movhi
.L35:
.LBE7:
	.loc 2 385 0
	ldr	r3, .L68+36
	ldr	r3, [r3]
	cmp	r3, #6
	bne	.L38
.LBB8:
	.loc 2 388 0
	ldr	r3, .L68+40
	fldd	d7, [r3]
	fcpys	s0, s14
	fcpys	s1, s15
	bl	sin
	fmrrd	r2, r3, d0
	mov	r0, r2
	mov	r1, r3
	adr	r3, .L68
	ldrd	r2, [r3]
	bl	__aeabi_dmul
	mov	r2, r0
	mov	r3, r1
	mov	r0, r2
	mov	r1, r3
	adr	r3, .L68
	ldrd	r2, [r3]
	bl	__aeabi_dadd
	mov	r2, r0
	mov	r3, r1
	fmdrr	d7, r2, r3
	fcpys	s0, s14
	fcpys	s1, s15
	bl	floor
	fmrrd	r2, r3, d0
	mov	r0, r2
	mov	r1, r3
	bl	__aeabi_d2iz
	mov	r2, r0
	ldr	r3, .L68+28
	ldrh	r3, [r3, r2, lsl #1]	@ movhi
	strh	r3, [r7, #40]	@ movhi
	.loc 2 390 0
	ldrh	r3, [r7, #122]
	cmp	r3, #10
	bls	.L39
	.loc 2 391 0
	ldr	r3, .L68+24
	ldr	r3, [r3]
	ldrh	r2, [r7, #122]
	fmsr	s15, r2	@ int
	fsitos	s15, s15
	fconsts	s14, #96
	fmuls	s14, s15, s14
	ldrh	r2, [r7, #40]
	fmsr	s15, r2	@ int
	fsitos	s15, s15
	fconsts	s13, #96
	fmuls	s15, s15, s13
	fadds	s15, s14, s15
	ftouizs	s15, s15
	fsts	s15, [r7]	@ int
	ldrh	r2, [r7]	@ movhi
	uxth	r1, r2
	ldr	r2, .L68+28
	strh	r1, [r2, r3, lsl #1]	@ movhi
	b	.L40
.L39:
	.loc 2 393 0
	ldr	r3, .L68+24
	ldr	r3, [r3]
	ldr	r2, .L68+28
	movs	r1, #0
	strh	r1, [r2, r3, lsl #1]	@ movhi
.L40:
	.loc 2 395 0
	ldr	r3, .L68+40
	ldrd	r2, [r3]
	mov	r0, r2
	mov	r1, r3
	adr	r3, .L68+8
	ldrd	r2, [r3]
	bl	__aeabi_dadd
	mov	r2, r0
	mov	r3, r1
	ldr	r1, .L68+40
	strd	r2, [r1]
	.loc 2 396 0
	ldr	r3, .L68+40
	ldrd	r2, [r3]
	mov	r0, r2
	mov	r1, r3
	adr	r3, .L68+16
	ldrd	r2, [r3]
	bl	__aeabi_dcmpge
	mov	r3, r0
	cmp	r3, #0
	beq	.L41
	.loc 2 397 0
	ldr	r1, .L68+40
	mov	r2, #0
	mov	r3, #0
	strd	r2, [r1]
.L41:
	.loc 2 399 0
	ldr	r3, .L68+24
	ldr	r3, [r3]
	adds	r2, r3, #1
	ldr	r3, .L68+32
	smull	r1, r3, r3, r2
	asrs	r1, r3, #9
	asrs	r3, r2, #31
	subs	r3, r1, r3
	movw	r1, #24000
	mul	r3, r1, r3
	subs	r3, r2, r3
	ldr	r2, .L68+24
	str	r3, [r2]
	.loc 2 402 0
	ldrh	r3, [r7, #40]
	cmp	r3, #0
	beq	.L38
	.loc 2 403 0
	ldr	r3, .L68+44
	ldrd	r2, [r3]
	mov	r0, #0
	ldr	r1, .L68+48
	bl	__aeabi_dsub
	mov	r2, r0
	mov	r3, r1
	mov	r0, r2
	mov	r1, r3
	mov	r2, #1073741824
	ldr	r3, .L68+52
	bl	__aeabi_dmul
	mov	r2, r0
	mov	r3, r1
	mov	r10, r2
	mov	fp, r3
	ldrh	r3, [r7, #122]
	mov	r0, r3
	bl	__aeabi_i2d
	mov	r2, r0
	mov	r3, r1
	mov	r0, r10
	mov	r1, fp
	bl	__aeabi_dmul
	mov	r2, r0
	mov	r3, r1
	mov	r10, r2
	mov	fp, r3
	ldr	r3, .L68+44
	ldrd	r0, [r3]
	ldr	r3, .L68+56
	ldrd	r2, [r3]
	bl	__aeabi_dmul
	mov	r2, r0
	mov	r3, r1
	fmdrr	d8, r2, r3
	ldrh	r3, [r7, #40]
	mov	r0, r3
	bl	__aeabi_i2d
	mov	r2, r0
	mov	r3, r1
	fmrrd	r0, r1, d8
	bl	__aeabi_dmul
	mov	r2, r0
	mov	r3, r1
	mov	r0, r10
	mov	r1, fp
	bl	__aeabi_dadd
	mov	r2, r0
	mov	r3, r1
	mov	r0, r2
	mov	r1, r3
	bl	__aeabi_d2uiz
	mov	r3, r0
	strh	r3, [r7, #122]	@ movhi
.L38:
.LBE8:
	.loc 2 406 0
	ldr	r3, .L68+36
	ldr	r3, [r3]
	cmp	r3, #10
	bne	.L43
.LBB9:
	.loc 2 409 0
	ldr	r3, .L68+60
	ldrh	r3, [r3]
	mov	r0, r3
	bl	__aeabi_i2d
	ldr	r3, .L68+64
	ldrd	r2, [r3]
	bl	__aeabi_dmul
	mov	r2, r0
	mov	r3, r1
	fmdrr	d7, r2, r3
	fcpys	s0, s14
	fcpys	s1, s15
	bl	sin
	fmrrd	r2, r3, d0
	mov	r0, r2
	mov	r1, r3
	adr	r3, .L68
	ldrd	r2, [r3]
	bl	__aeabi_dmul
	mov	r2, r0
	mov	r3, r1
	mov	r0, r2
	mov	r1, r3
	adr	r3, .L68
	ldrd	r2, [r3]
	bl	__aeabi_dadd
	mov	r2, r0
	mov	r3, r1
	fmdrr	d7, r2, r3
	fcpys	s0, s14
	fcpys	s1, s15
	bl	floor
	fmrrd	r2, r3, d0
	mov	r0, r2
	mov	r1, r3
	bl	__aeabi_d2iz
	mov	r2, r0
	ldr	r3, .L68+28
	ldrh	r3, [r3, r2, lsl #1]	@ movhi
	strh	r3, [r7, #38]	@ movhi
	.loc 2 411 0
	ldrh	r3, [r7, #122]
	cmp	r3, #10
	bls	.L44
	.loc 2 412 0
	ldr	r3, .L68+24
	ldr	r3, [r3]
	fconsts	s14, #112
	flds	s15, [r7, #132]
	fsubs	s14, s14, s15
	ldrh	r2, [r7, #122]
	fmsr	s15, r2	@ int
	fsitos	s15, s15
	fmuls	s14, s14, s15
	ldrh	r2, [r7, #38]
	fmsr	s15, r2	@ int
	fsitos	s13, s15
	flds	s15, [r7, #132]
	fmuls	s15, s13, s15
	fadds	s15, s14, s15
	ftouizs	s15, s15
	fsts	s15, [r7]	@ int
	ldrh	r2, [r7]	@ movhi
	uxth	r1, r2
	ldr	r2, .L68+28
	strh	r1, [r2, r3, lsl #1]	@ movhi
	b	.L45
.L69:
	.align	3
.L68:
	.word	0
	.word	1086812160
	.word	-536870912
	.word	1057634018
	.word	0
	.word	1075388922
	.word	counter
	.word	samples
	.word	91625969
	.word	change_effect
	.word	flange_counter.7104
	.word	fl.7107
	.word	1072693248
	.word	1072902963
	.word	fl_gain.7106
	.word	glob
	.word	flange_counter.7108
.L44:
	.loc 2 414 0
	ldr	r3, .L70+16
	ldr	r3, [r3]
	ldr	r2, .L70+20
	movs	r1, #0
	strh	r1, [r2, r3, lsl #1]	@ movhi
.L45:
	.loc 2 416 0
	ldr	r3, .L70+24
	ldrd	r2, [r3]
	mov	r0, r2
	mov	r1, r3
	adr	r3, .L70
	ldrd	r2, [r3]
	bl	__aeabi_dadd
	mov	r2, r0
	mov	r3, r1
	ldr	r1, .L70+24
	strd	r2, [r1]
	.loc 2 417 0
	ldr	r3, .L70+24
	ldrd	r2, [r3]
	mov	r0, r2
	mov	r1, r3
	adr	r3, .L70+8
	ldrd	r2, [r3]
	bl	__aeabi_dcmpge
	mov	r3, r0
	cmp	r3, #0
	beq	.L46
	.loc 2 418 0
	ldr	r1, .L70+24
	mov	r2, #0
	mov	r3, #0
	strd	r2, [r1]
.L46:
	.loc 2 420 0
	ldr	r3, .L70+16
	ldr	r3, [r3]
	adds	r2, r3, #1
	ldr	r3, .L70+28
	smull	r1, r3, r3, r2
	asrs	r1, r3, #9
	asrs	r3, r2, #31
	subs	r3, r1, r3
	movw	r1, #24000
	mul	r3, r1, r3
	subs	r3, r2, r3
	ldr	r2, .L70+16
	str	r3, [r2]
	.loc 2 422 0
	ldrh	r3, [r7, #38]
	cmp	r3, #0
	beq	.L43
	.loc 2 423 0
	fconsts	s14, #112
	flds	s15, [r7, #112]
	fsubs	s14, s14, s15
	ldrh	r3, [r7, #122]
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	fmuls	s14, s14, s15
	ldrh	r3, [r7, #38]
	fmsr	s15, r3	@ int
	fsitos	s13, s15
	flds	s15, [r7, #112]
	fmuls	s15, s13, s15
	fadds	s15, s14, s15
	ftouizs	s15, s15
	fsts	s15, [r7]	@ int
	ldrh	r3, [r7]	@ movhi
	strh	r3, [r7, #122]	@ movhi
.L43:
.LBE9:
	.loc 2 427 0
	ldr	r3, .L70+32
	ldr	r3, [r3]
	cmp	r3, #4
	bne	.L62
.LBB10:
	.loc 2 428 0
	ldrh	r3, [r7, #122]
	sxth	r3, r3
	sub	r3, r3, #2048
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	flds	s14, .L70+36
	fdivs	s15, s15, s14
	fsts	s15, [r7, #32]
	.loc 2 434 0
	ldr	r3, .L70+40
	ldrh	r3, [r3]
	fmsr	s15, r3	@ int
	fsitos	s15, s15
	flds	s14, .L70+44
	fdivs	s15, s15, s14
	flds	s14, .L70+48
	fmuls	s15, s15, s14
	flds	s14, .L70+52
	fadds	s15, s15, s14
	fsts	s15, [r7, #28]
	.loc 2 435 0
	ldr	r3, .L70+56
	flds	s15, [r3]
	flds	s14, [r7, #32]
	fsubs	s14, s14, s15
	ldr	r3, .L70+60
	flds	s13, [r3]
	ldr	r3, .L70+64
	flds	s15, [r3]
	fmuls	s15, s13, s15
	fsubs	s15, s14, s15
	fsts	s15, [r7, #24]
	.loc 2 436 0
	ldr	r3, .L70+68
	flds	s14, [r3]
	flds	s15, [r7, #24]
	fmuls	s14, s14, s15
	ldr	r3, .L70+64
	flds	s15, [r3]
	fadds	s15, s14, s15
	fsts	s15, [r7, #20]
	.loc 2 437 0
	ldr	r3, .L70+68
	flds	s14, [r3]
	flds	s15, [r7, #20]
	fmuls	s14, s14, s15
	ldr	r3, .L70+56
	flds	s15, [r3]
	fadds	s15, s14, s15
	fsts	s15, [r7, #16]
	.loc 2 439 0
	flds	s15, [r7, #28]
	flds	s14, .L70+72
	fmuls	s15, s15, s14
	flds	s14, .L70+76
	fdivs	s15, s15, s14
	fcpys	s0, s15
	bl	sinf
	fcpys	s15, s0
	fadds	s15, s15, s15
	ldr	r3, .L70+68
	fsts	s15, [r3]
	.loc 2 441 0
	ldr	r2, .L70+64
	ldr	r3, [r7, #20]	@ float
	str	r3, [r2]	@ float
	.loc 2 442 0
	ldr	r2, .L70+56
	ldr	r3, [r7, #16]	@ float
	str	r3, [r2]	@ float
	.loc 2 443 0
	flds	s15, [r7, #20]
	fconsts	s14, #120
	fmuls	s15, s15, s14
	fsts	s15, [r7, #12]
	.loc 2 445 0
	flds	s15, [r7, #12]
	flds	s14, .L70+36
	fmuls	s15, s15, s14
	flds	s14, .L70+36
	fadds	s15, s15, s14
	ftouizs	s15, s15
	fsts	s15, [r7]	@ int
	ldrh	r3, [r7]	@ movhi
	strh	r3, [r7, #122]	@ movhi
.L62:
.LBE10:
	.loc 2 454 0
	ldr	r3, .L70+80
	ldrb	r3, [r3]	@ zero_extendqisi2
	cmp	r3, #1
	beq	.L50
	cmp	r3, #2
	beq	.L51
	cmp	r3, #0
	beq	.L52
	b	.L49
.L50:
	.loc 2 456 0
	ldrh	r2, [r7, #122]
	ldrh	r3, [r7, #70]
	cmp	r2, r3
	bls	.L53
	.loc 2 457 0
	ldr	r3, .L70+80
	movs	r2, #0
	strb	r2, [r3]
.L53:
	.loc 2 459 0
	movs	r3, #0
	strh	r3, [r7, #122]	@ movhi
	.loc 2 460 0
	b	.L49
.L51:
	.loc 2 462 0
	ldrh	r2, [r7, #122]
	ldrh	r3, [r7, #70]
	cmp	r2, r3
	bls	.L54
	.loc 2 463 0
	ldr	r3, .L70+80
	movs	r2, #0
	strb	r2, [r3]
	b	.L55
.L54:
	.loc 2 464 0
	ldrh	r2, [r7, #122]
	ldrh	r3, [r7, #70]
	cmp	r2, r3
	bhi	.L55
	.loc 2 465 0
	ldr	r3, .L70+84
	ldr	r3, [r3]
	adds	r3, r3, #1
	ldr	r2, .L70+84
	str	r3, [r2]
.L55:
	.loc 2 467 0
	ldr	r3, .L70+84
	ldr	r3, [r3]
	cmp	r3, #199
	ble	.L56
	.loc 2 468 0
	ldr	r3, .L70+80
	movs	r2, #1
	strb	r2, [r3]
	.loc 2 469 0
	ldr	r3, .L70+84
	movs	r2, #0
	str	r2, [r3]
	.loc 2 471 0
	b	.L49
.L56:
	b	.L49
.L52:
	.loc 2 473 0
	ldrh	r2, [r7, #122]
	ldrh	r3, [r7, #70]
	cmp	r2, r3
	bcs	.L57
	.loc 2 474 0
	ldr	r3, .L70+80
	movs	r2, #2
	strb	r2, [r3]
	.loc 2 475 0
	ldr	r3, .L70+84
	movs	r2, #0
	str	r2, [r3]
	.loc 2 477 0
	b	.L63
.L57:
.L63:
	nop
.L49:
	.loc 2 480 0 discriminator 2
	ldr	r3, [r7, #124]
	lsls	r3, r3, #1
	add	r2, r7, #136
	add	r3, r3, r2
	ldrh	r2, [r7, #122]	@ movhi
	strh	r2, [r3, #-128]	@ movhi
.LBE3:
	.loc 2 328 0 discriminator 2
	ldr	r3, [r7, #124]
	adds	r3, r3, #1
	str	r3, [r7, #124]
.L31:
	.loc 2 328 0 is_stmt 0 discriminator 1
	ldr	r3, [r7, #124]
	cmp	r3, #1
	ble	.L58
	.loc 2 482 0 is_stmt 1
	ldr	r3, .L70+88
	mov	r2, r3
	add	r3, r7, #8
	ldr	r0, [r3]	@ unaligned
	str	r0, [r2]	@ unaligned
.L28:
.LBE2:
	.loc 2 484 0
	bl	serviceIRQA
	.loc 2 485 0
	b	.L59
.L71:
	.align	3
.L70:
	.word	-2147483648
	.word	1055193269
	.word	0
	.word	1075388922
	.word	counter
	.word	samples
	.word	flange_counter.7108
	.word	91625969
	.word	change_effect
	.word	1157627904
	.word	glob
	.word	1123024896
	.word	1152098304
	.word	1123942400
	.word	ylpast.7111
	.word	Q1.7114
	.word	ybpast.7112
	.word	F1.7113
	.word	1078530011
	.word	1195081728
	.word	gate
	.word	sample_count.7120
	.word	g_dac_buff
	.cfi_endproc
.LFE114:
	.size	main, .-main
	.align	2
	.global	TIM4_IRQHandler
	.thumb
	.thumb_func
	.type	TIM4_IRQHandler, %function
TIM4_IRQHandler:
.LFB115:
	.loc 2 488 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 2 489 0
	ldr	r3, .L73
	movs	r2, #1
	str	r2, [r3]
	.loc 2 490 0
	ldr	r3, .L73+4
	movs	r2, #0
	strh	r2, [r3, #16]	@ movhi
	.loc 2 491 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L74:
	.align	2
.L73:
	.word	g_data_ready
	.word	1073743872
	.cfi_endproc
.LFE115:
	.size	TIM4_IRQHandler, .-TIM4_IRQHandler
	.align	2
	.global	EXTI9_5_IRQHandler
	.thumb
	.thumb_func
	.type	EXTI9_5_IRQHandler, %function
EXTI9_5_IRQHandler:
.LFB116:
	.loc 2 496 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	push	{r7}
	.cfi_def_cfa_offset 4
	.cfi_offset 7, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 2 497 0
	ldr	r3, .L84
	ldr	r3, [r3, #20]
	and	r3, r3, #128
	cmp	r3, #0
	beq	.L76
	.loc 2 499 0
	ldr	r3, .L84+4
	ldrb	r3, [r3]
	uxtb	r3, r3
	cmp	r3, #0
	bne	.L77
	.loc 2 501 0
	ldr	r3, .L84+4
	movs	r2, #1
	strb	r2, [r3]
	.loc 2 502 0
	ldr	r3, .L84+8
	ldr	r3, [r3]
	cmp	r3, #9
	bne	.L78
	.loc 2 504 0
	ldr	r3, .L84+8
	movs	r2, #0
	str	r2, [r3]
	b	.L79
.L78:
	.loc 2 506 0
	ldr	r3, .L84+8
	ldr	r3, [r3]
	adds	r3, r3, #1
	ldr	r2, .L84+8
	str	r3, [r2]
.L79:
	.loc 2 507 0
	ldr	r2, .L84+12
	ldr	r3, .L84+8
	ldr	r3, [r3]
	ldr	r1, .L84+16
	ldrh	r3, [r1, r3, lsl #1]
	str	r3, [r2, #20]
	.loc 2 508 0
	ldr	r2, .L84+20
	ldr	r3, .L84+8
	ldr	r3, [r3]
	ldr	r1, .L84+24
	ldrh	r3, [r1, r3, lsl #1]
	str	r3, [r2, #20]
.L77:
	.loc 2 510 0
	ldr	r3, .L84
	movs	r2, #128
	str	r2, [r3, #20]
.L76:
	.loc 2 513 0
	ldr	r3, .L84
	ldr	r3, [r3, #20]
	and	r3, r3, #256
	cmp	r3, #0
	beq	.L75
	.loc 2 515 0
	ldr	r3, .L84+4
	ldrb	r3, [r3]
	uxtb	r3, r3
	cmp	r3, #0
	bne	.L81
	.loc 2 517 0
	ldr	r3, .L84+4
	movs	r2, #1
	strb	r2, [r3]
	.loc 2 518 0
	ldr	r3, .L84+8
	ldr	r3, [r3]
	cmp	r3, #0
	bne	.L82
	.loc 2 520 0
	ldr	r3, .L84+8
	movs	r2, #9
	str	r2, [r3]
	b	.L83
.L82:
	.loc 2 522 0
	ldr	r3, .L84+8
	ldr	r3, [r3]
	subs	r3, r3, #1
	ldr	r2, .L84+8
	str	r3, [r2]
.L83:
	.loc 2 523 0
	ldr	r2, .L84+12
	ldr	r3, .L84+8
	ldr	r3, [r3]
	ldr	r1, .L84+16
	ldrh	r3, [r1, r3, lsl #1]
	str	r3, [r2, #20]
	.loc 2 524 0
	ldr	r2, .L84+20
	ldr	r3, .L84+8
	ldr	r3, [r3]
	ldr	r1, .L84+24
	ldrh	r3, [r1, r3, lsl #1]
	str	r3, [r2, #20]
.L81:
	.loc 2 526 0
	ldr	r3, .L84
	mov	r2, #256
	str	r2, [r3, #20]
.L75:
	.loc 2 528 0
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	ldr	r7, [sp], #4
	.cfi_restore 7
	.cfi_def_cfa_offset 0
	bx	lr
.L85:
	.align	2
.L84:
	.word	1073822720
	.word	g_gpioa_irq_state
	.word	change_effect
	.word	1073875968
	.word	arrayLED1
	.word	1073876992
	.word	arrayLED2
	.cfi_endproc
.LFE116:
	.size	EXTI9_5_IRQHandler, .-EXTI9_5_IRQHandler
	.align	2
	.global	serviceIRQA
	.thumb
	.thumb_func
	.type	serviceIRQA, %function
serviceIRQA:
.LFB117:
	.loc 2 532 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	.loc 2 533 0
	ldr	r3, .L96
	ldrb	r3, [r3]
	uxtb	r3, r3
	cmp	r3, #3
	bhi	.L95
	adr	r2, .L89
	ldr	pc, [r2, r3, lsl #2]
	.p2align 2
.L89:
	.word	.L95+1
	.word	.L90+1
	.word	.L91+1
	.word	.L92+1
	.p2align 1
.L90:
	.loc 2 541 0
	ldr	r3, .L96+4
	ldr	r3, [r3]
	adds	r3, r3, #1
	ldr	r2, .L96+4
	str	r3, [r2]
	.loc 2 542 0
	ldr	r3, .L96
	movs	r2, #2
	strb	r2, [r3]
	.loc 2 543 0
	b	.L86
.L91:
	.loc 2 547 0
	ldr	r3, .L96+8
	ldr	r3, [r3, #16]
	and	r3, r3, #384
	cmp	r3, #0
	bne	.L94
	.loc 2 549 0
	ldr	r3, .L96
	movs	r2, #3
	strb	r2, [r3]
	.loc 2 550 0
	bl	getSYSTIMER
	mov	r2, r0
	ldr	r3, .L96+12
	str	r2, [r3]
	.loc 2 552 0
	b	.L86
.L94:
	b	.L86
.L92:
	.loc 2 556 0
	ldr	r3, .L96+12
	ldr	r3, [r3]
	mov	r0, r3
	ldr	r1, .L96+16
	bl	chk4TimeoutSYSTIMER
	mov	r3, r0
	cmp	r3, #0
	bne	.L87
	.loc 2 558 0
	ldr	r3, .L96
	movs	r2, #0
	strb	r2, [r3]
.L87:
	.loc 2 563 0
	nop
.L95:
	nop
.L86:
	.loc 2 566 0
	pop	{r7, pc}
.L97:
	.align	2
.L96:
	.word	g_gpioa_irq_state
	.word	g_irq_cnt
	.word	1073876992
	.word	g_irq_timer
	.word	500000
	.cfi_endproc
.LFE117:
	.size	serviceIRQA, .-serviceIRQA
	.bss
	.align	2
i.7094:
	.space	4
	.align	3
flange_counter.7104:
	.space	8
	.data
	.align	3
	.type	fl.7107, %object
	.size	fl.7107, 8
fl.7107:
	.word	0
	.word	1071644672
	.align	3
	.type	fl_gain.7106, %object
	.size	fl_gain.7106, 8
fl_gain.7106:
	.word	0
	.word	1072693248
	.bss
	.align	3
flange_counter.7108:
	.space	8
	.align	2
ylpast.7111:
	.space	4
	.data
	.align	2
	.type	Q1.7114, %object
	.size	Q1.7114, 4
Q1.7114:
	.word	1036831949
	.bss
	.align	2
ybpast.7112:
	.space	4
	.align	2
F1.7113:
	.space	4
	.align	2
sample_count.7120:
	.space	4
	.data
	.align	3
	.type	fl.7103, %object
	.size	fl.7103, 8
fl.7103:
	.word	-1073741824
	.word	1072483532
	.align	3
	.type	fl_gain.7102, %object
	.size	fl_gain.7102, 8
fl_gain.7102:
	.word	0
	.word	1072693248
	.text
.Letext0:
	.file 3 "/home/anes/msut/STM32F407/Libraries/CMSIS/ST/STM32F4xx/Include/stm32f4xx.h"
	.file 4 "/home/anes/msut/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/machine/_default_types.h"
	.file 5 "/home/anes/msut/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/stdint.h"
	.file 6 "i2c.h"
	.file 7 "<built-in>"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x12b4
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF275
	.byte	0x1
	.4byte	.LASF276
	.4byte	.LASF277
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.4byte	.LASF233
	.byte	0x1
	.byte	0x3
	.byte	0x91
	.4byte	0x260
	.uleb128 0x3
	.4byte	.LASF0
	.sleb128 -14
	.uleb128 0x3
	.4byte	.LASF1
	.sleb128 -12
	.uleb128 0x3
	.4byte	.LASF2
	.sleb128 -11
	.uleb128 0x3
	.4byte	.LASF3
	.sleb128 -10
	.uleb128 0x3
	.4byte	.LASF4
	.sleb128 -5
	.uleb128 0x3
	.4byte	.LASF5
	.sleb128 -4
	.uleb128 0x3
	.4byte	.LASF6
	.sleb128 -2
	.uleb128 0x3
	.4byte	.LASF7
	.sleb128 -1
	.uleb128 0x3
	.4byte	.LASF8
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF9
	.sleb128 1
	.uleb128 0x3
	.4byte	.LASF10
	.sleb128 2
	.uleb128 0x3
	.4byte	.LASF11
	.sleb128 3
	.uleb128 0x3
	.4byte	.LASF12
	.sleb128 4
	.uleb128 0x3
	.4byte	.LASF13
	.sleb128 5
	.uleb128 0x3
	.4byte	.LASF14
	.sleb128 6
	.uleb128 0x3
	.4byte	.LASF15
	.sleb128 7
	.uleb128 0x3
	.4byte	.LASF16
	.sleb128 8
	.uleb128 0x3
	.4byte	.LASF17
	.sleb128 9
	.uleb128 0x3
	.4byte	.LASF18
	.sleb128 10
	.uleb128 0x3
	.4byte	.LASF19
	.sleb128 11
	.uleb128 0x3
	.4byte	.LASF20
	.sleb128 12
	.uleb128 0x3
	.4byte	.LASF21
	.sleb128 13
	.uleb128 0x3
	.4byte	.LASF22
	.sleb128 14
	.uleb128 0x3
	.4byte	.LASF23
	.sleb128 15
	.uleb128 0x3
	.4byte	.LASF24
	.sleb128 16
	.uleb128 0x3
	.4byte	.LASF25
	.sleb128 17
	.uleb128 0x3
	.4byte	.LASF26
	.sleb128 18
	.uleb128 0x3
	.4byte	.LASF27
	.sleb128 19
	.uleb128 0x3
	.4byte	.LASF28
	.sleb128 20
	.uleb128 0x3
	.4byte	.LASF29
	.sleb128 21
	.uleb128 0x3
	.4byte	.LASF30
	.sleb128 22
	.uleb128 0x3
	.4byte	.LASF31
	.sleb128 23
	.uleb128 0x3
	.4byte	.LASF32
	.sleb128 24
	.uleb128 0x3
	.4byte	.LASF33
	.sleb128 25
	.uleb128 0x3
	.4byte	.LASF34
	.sleb128 26
	.uleb128 0x3
	.4byte	.LASF35
	.sleb128 27
	.uleb128 0x3
	.4byte	.LASF36
	.sleb128 28
	.uleb128 0x3
	.4byte	.LASF37
	.sleb128 29
	.uleb128 0x3
	.4byte	.LASF38
	.sleb128 30
	.uleb128 0x3
	.4byte	.LASF39
	.sleb128 31
	.uleb128 0x3
	.4byte	.LASF40
	.sleb128 32
	.uleb128 0x3
	.4byte	.LASF41
	.sleb128 33
	.uleb128 0x3
	.4byte	.LASF42
	.sleb128 34
	.uleb128 0x3
	.4byte	.LASF43
	.sleb128 35
	.uleb128 0x3
	.4byte	.LASF44
	.sleb128 36
	.uleb128 0x3
	.4byte	.LASF45
	.sleb128 37
	.uleb128 0x3
	.4byte	.LASF46
	.sleb128 38
	.uleb128 0x3
	.4byte	.LASF47
	.sleb128 39
	.uleb128 0x3
	.4byte	.LASF48
	.sleb128 40
	.uleb128 0x3
	.4byte	.LASF49
	.sleb128 41
	.uleb128 0x3
	.4byte	.LASF50
	.sleb128 42
	.uleb128 0x3
	.4byte	.LASF51
	.sleb128 43
	.uleb128 0x3
	.4byte	.LASF52
	.sleb128 44
	.uleb128 0x3
	.4byte	.LASF53
	.sleb128 45
	.uleb128 0x3
	.4byte	.LASF54
	.sleb128 46
	.uleb128 0x3
	.4byte	.LASF55
	.sleb128 47
	.uleb128 0x3
	.4byte	.LASF56
	.sleb128 48
	.uleb128 0x3
	.4byte	.LASF57
	.sleb128 49
	.uleb128 0x3
	.4byte	.LASF58
	.sleb128 50
	.uleb128 0x3
	.4byte	.LASF59
	.sleb128 51
	.uleb128 0x3
	.4byte	.LASF60
	.sleb128 52
	.uleb128 0x3
	.4byte	.LASF61
	.sleb128 53
	.uleb128 0x3
	.4byte	.LASF62
	.sleb128 54
	.uleb128 0x3
	.4byte	.LASF63
	.sleb128 55
	.uleb128 0x3
	.4byte	.LASF64
	.sleb128 56
	.uleb128 0x3
	.4byte	.LASF65
	.sleb128 57
	.uleb128 0x3
	.4byte	.LASF66
	.sleb128 58
	.uleb128 0x3
	.4byte	.LASF67
	.sleb128 59
	.uleb128 0x3
	.4byte	.LASF68
	.sleb128 60
	.uleb128 0x3
	.4byte	.LASF69
	.sleb128 61
	.uleb128 0x3
	.4byte	.LASF70
	.sleb128 62
	.uleb128 0x3
	.4byte	.LASF71
	.sleb128 63
	.uleb128 0x3
	.4byte	.LASF72
	.sleb128 64
	.uleb128 0x3
	.4byte	.LASF73
	.sleb128 65
	.uleb128 0x3
	.4byte	.LASF74
	.sleb128 66
	.uleb128 0x3
	.4byte	.LASF75
	.sleb128 67
	.uleb128 0x3
	.4byte	.LASF76
	.sleb128 68
	.uleb128 0x3
	.4byte	.LASF77
	.sleb128 69
	.uleb128 0x3
	.4byte	.LASF78
	.sleb128 70
	.uleb128 0x3
	.4byte	.LASF79
	.sleb128 71
	.uleb128 0x3
	.4byte	.LASF80
	.sleb128 72
	.uleb128 0x3
	.4byte	.LASF81
	.sleb128 73
	.uleb128 0x3
	.4byte	.LASF82
	.sleb128 74
	.uleb128 0x3
	.4byte	.LASF83
	.sleb128 75
	.uleb128 0x3
	.4byte	.LASF84
	.sleb128 76
	.uleb128 0x3
	.4byte	.LASF85
	.sleb128 77
	.uleb128 0x3
	.4byte	.LASF86
	.sleb128 78
	.uleb128 0x3
	.4byte	.LASF87
	.sleb128 79
	.uleb128 0x3
	.4byte	.LASF88
	.sleb128 80
	.uleb128 0x3
	.4byte	.LASF89
	.sleb128 81
	.byte	0
	.uleb128 0x4
	.4byte	.LASF90
	.byte	0x3
	.byte	0xef
	.4byte	0x25
	.uleb128 0x5
	.byte	0x1
	.byte	0x6
	.4byte	.LASF92
	.uleb128 0x4
	.4byte	.LASF91
	.byte	0x4
	.byte	0x1d
	.4byte	0x27d
	.uleb128 0x5
	.byte	0x1
	.byte	0x8
	.4byte	.LASF93
	.uleb128 0x4
	.4byte	.LASF94
	.byte	0x4
	.byte	0x29
	.4byte	0x28f
	.uleb128 0x5
	.byte	0x2
	.byte	0x5
	.4byte	.LASF95
	.uleb128 0x4
	.4byte	.LASF96
	.byte	0x4
	.byte	0x2b
	.4byte	0x2a1
	.uleb128 0x5
	.byte	0x2
	.byte	0x7
	.4byte	.LASF97
	.uleb128 0x4
	.4byte	.LASF98
	.byte	0x4
	.byte	0x3f
	.4byte	0x2b3
	.uleb128 0x5
	.byte	0x4
	.byte	0x5
	.4byte	.LASF99
	.uleb128 0x4
	.4byte	.LASF100
	.byte	0x4
	.byte	0x41
	.4byte	0x2c5
	.uleb128 0x5
	.byte	0x4
	.byte	0x7
	.4byte	.LASF101
	.uleb128 0x5
	.byte	0x8
	.byte	0x5
	.4byte	.LASF102
	.uleb128 0x5
	.byte	0x8
	.byte	0x7
	.4byte	.LASF103
	.uleb128 0x6
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x5
	.byte	0x4
	.byte	0x7
	.4byte	.LASF104
	.uleb128 0x4
	.4byte	.LASF105
	.byte	0x5
	.byte	0x15
	.4byte	0x272
	.uleb128 0x4
	.4byte	.LASF106
	.byte	0x5
	.byte	0x20
	.4byte	0x284
	.uleb128 0x4
	.4byte	.LASF107
	.byte	0x5
	.byte	0x21
	.4byte	0x296
	.uleb128 0x4
	.4byte	.LASF108
	.byte	0x5
	.byte	0x2c
	.4byte	0x2a8
	.uleb128 0x4
	.4byte	.LASF109
	.byte	0x5
	.byte	0x2d
	.4byte	0x2ba
	.uleb128 0x7
	.2byte	0xe04
	.byte	0x1
	.2byte	0x130
	.4byte	0x3db
	.uleb128 0x8
	.4byte	.LASF110
	.byte	0x1
	.2byte	0x132
	.4byte	0x3f2
	.byte	0
	.uleb128 0x8
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x133
	.4byte	0x3f7
	.byte	0x20
	.uleb128 0x8
	.4byte	.LASF112
	.byte	0x1
	.2byte	0x134
	.4byte	0x407
	.byte	0x80
	.uleb128 0x8
	.4byte	.LASF113
	.byte	0x1
	.2byte	0x135
	.4byte	0x3f7
	.byte	0xa0
	.uleb128 0x9
	.4byte	.LASF114
	.byte	0x1
	.2byte	0x136
	.4byte	0x40c
	.2byte	0x100
	.uleb128 0x9
	.4byte	.LASF115
	.byte	0x1
	.2byte	0x137
	.4byte	0x3f7
	.2byte	0x120
	.uleb128 0x9
	.4byte	.LASF116
	.byte	0x1
	.2byte	0x138
	.4byte	0x411
	.2byte	0x180
	.uleb128 0x9
	.4byte	.LASF117
	.byte	0x1
	.2byte	0x139
	.4byte	0x3f7
	.2byte	0x1a0
	.uleb128 0x9
	.4byte	.LASF118
	.byte	0x1
	.2byte	0x13a
	.4byte	0x416
	.2byte	0x200
	.uleb128 0x9
	.4byte	.LASF119
	.byte	0x1
	.2byte	0x13b
	.4byte	0x41b
	.2byte	0x220
	.uleb128 0xa
	.ascii	"IP\000"
	.byte	0x1
	.2byte	0x13c
	.4byte	0x43b
	.2byte	0x300
	.uleb128 0x9
	.4byte	.LASF120
	.byte	0x1
	.2byte	0x13d
	.4byte	0x440
	.2byte	0x3f0
	.uleb128 0x9
	.4byte	.LASF121
	.byte	0x1
	.2byte	0x13e
	.4byte	0x451
	.2byte	0xe00
	.byte	0
	.uleb128 0xb
	.4byte	0x314
	.4byte	0x3eb
	.uleb128 0xc
	.4byte	0x3eb
	.byte	0x7
	.byte	0
	.uleb128 0x5
	.byte	0x4
	.byte	0x7
	.4byte	.LASF122
	.uleb128 0xd
	.4byte	0x3db
	.uleb128 0xb
	.4byte	0x314
	.4byte	0x407
	.uleb128 0xc
	.4byte	0x3eb
	.byte	0x17
	.byte	0
	.uleb128 0xd
	.4byte	0x3db
	.uleb128 0xd
	.4byte	0x3db
	.uleb128 0xd
	.4byte	0x3db
	.uleb128 0xd
	.4byte	0x3db
	.uleb128 0xb
	.4byte	0x314
	.4byte	0x42b
	.uleb128 0xc
	.4byte	0x3eb
	.byte	0x37
	.byte	0
	.uleb128 0xb
	.4byte	0x2e8
	.4byte	0x43b
	.uleb128 0xc
	.4byte	0x3eb
	.byte	0xef
	.byte	0
	.uleb128 0xd
	.4byte	0x42b
	.uleb128 0xb
	.4byte	0x314
	.4byte	0x451
	.uleb128 0xe
	.4byte	0x3eb
	.2byte	0x283
	.byte	0
	.uleb128 0xd
	.4byte	0x314
	.uleb128 0xf
	.4byte	.LASF123
	.byte	0x1
	.2byte	0x13f
	.4byte	0x31f
	.uleb128 0x10
	.byte	0x8c
	.byte	0x1
	.2byte	0x150
	.4byte	0x57d
	.uleb128 0x8
	.4byte	.LASF124
	.byte	0x1
	.2byte	0x152
	.4byte	0x57d
	.byte	0
	.uleb128 0x8
	.4byte	.LASF125
	.byte	0x1
	.2byte	0x153
	.4byte	0x451
	.byte	0x4
	.uleb128 0x8
	.4byte	.LASF126
	.byte	0x1
	.2byte	0x154
	.4byte	0x451
	.byte	0x8
	.uleb128 0x8
	.4byte	.LASF127
	.byte	0x1
	.2byte	0x155
	.4byte	0x451
	.byte	0xc
	.uleb128 0x11
	.ascii	"SCR\000"
	.byte	0x1
	.2byte	0x156
	.4byte	0x451
	.byte	0x10
	.uleb128 0x11
	.ascii	"CCR\000"
	.byte	0x1
	.2byte	0x157
	.4byte	0x451
	.byte	0x14
	.uleb128 0x11
	.ascii	"SHP\000"
	.byte	0x1
	.2byte	0x158
	.4byte	0x592
	.byte	0x18
	.uleb128 0x8
	.4byte	.LASF128
	.byte	0x1
	.2byte	0x159
	.4byte	0x451
	.byte	0x24
	.uleb128 0x8
	.4byte	.LASF129
	.byte	0x1
	.2byte	0x15a
	.4byte	0x451
	.byte	0x28
	.uleb128 0x8
	.4byte	.LASF130
	.byte	0x1
	.2byte	0x15b
	.4byte	0x451
	.byte	0x2c
	.uleb128 0x8
	.4byte	.LASF131
	.byte	0x1
	.2byte	0x15c
	.4byte	0x451
	.byte	0x30
	.uleb128 0x8
	.4byte	.LASF132
	.byte	0x1
	.2byte	0x15d
	.4byte	0x451
	.byte	0x34
	.uleb128 0x8
	.4byte	.LASF133
	.byte	0x1
	.2byte	0x15e
	.4byte	0x451
	.byte	0x38
	.uleb128 0x8
	.4byte	.LASF134
	.byte	0x1
	.2byte	0x15f
	.4byte	0x451
	.byte	0x3c
	.uleb128 0x11
	.ascii	"PFR\000"
	.byte	0x1
	.2byte	0x160
	.4byte	0x5a7
	.byte	0x40
	.uleb128 0x11
	.ascii	"DFR\000"
	.byte	0x1
	.2byte	0x161
	.4byte	0x57d
	.byte	0x48
	.uleb128 0x11
	.ascii	"ADR\000"
	.byte	0x1
	.2byte	0x162
	.4byte	0x57d
	.byte	0x4c
	.uleb128 0x8
	.4byte	.LASF135
	.byte	0x1
	.2byte	0x163
	.4byte	0x5c1
	.byte	0x50
	.uleb128 0x8
	.4byte	.LASF136
	.byte	0x1
	.2byte	0x164
	.4byte	0x5db
	.byte	0x60
	.uleb128 0x8
	.4byte	.LASF111
	.byte	0x1
	.2byte	0x165
	.4byte	0x5cb
	.byte	0x74
	.uleb128 0x8
	.4byte	.LASF137
	.byte	0x1
	.2byte	0x166
	.4byte	0x451
	.byte	0x88
	.byte	0
	.uleb128 0x12
	.4byte	0x451
	.uleb128 0xb
	.4byte	0x2e8
	.4byte	0x592
	.uleb128 0xc
	.4byte	0x3eb
	.byte	0xb
	.byte	0
	.uleb128 0xd
	.4byte	0x582
	.uleb128 0xb
	.4byte	0x314
	.4byte	0x5a7
	.uleb128 0xc
	.4byte	0x3eb
	.byte	0x1
	.byte	0
	.uleb128 0x12
	.4byte	0x5ac
	.uleb128 0xd
	.4byte	0x597
	.uleb128 0xb
	.4byte	0x314
	.4byte	0x5c1
	.uleb128 0xc
	.4byte	0x3eb
	.byte	0x3
	.byte	0
	.uleb128 0x12
	.4byte	0x5c6
	.uleb128 0xd
	.4byte	0x5b1
	.uleb128 0xb
	.4byte	0x314
	.4byte	0x5db
	.uleb128 0xc
	.4byte	0x3eb
	.byte	0x4
	.byte	0
	.uleb128 0x12
	.4byte	0x5e0
	.uleb128 0xd
	.4byte	0x5cb
	.uleb128 0xf
	.4byte	.LASF138
	.byte	0x1
	.2byte	0x167
	.4byte	0x462
	.uleb128 0xd
	.4byte	0x2e8
	.uleb128 0xd
	.4byte	0x2fe
	.uleb128 0xd
	.4byte	0x309
	.uleb128 0x10
	.byte	0x50
	.byte	0x3
	.2byte	0x130
	.4byte	0x70c
	.uleb128 0x11
	.ascii	"SR\000"
	.byte	0x3
	.2byte	0x132
	.4byte	0x451
	.byte	0
	.uleb128 0x11
	.ascii	"CR1\000"
	.byte	0x3
	.2byte	0x133
	.4byte	0x451
	.byte	0x4
	.uleb128 0x11
	.ascii	"CR2\000"
	.byte	0x3
	.2byte	0x134
	.4byte	0x451
	.byte	0x8
	.uleb128 0x8
	.4byte	.LASF139
	.byte	0x3
	.2byte	0x135
	.4byte	0x451
	.byte	0xc
	.uleb128 0x8
	.4byte	.LASF140
	.byte	0x3
	.2byte	0x136
	.4byte	0x451
	.byte	0x10
	.uleb128 0x8
	.4byte	.LASF141
	.byte	0x3
	.2byte	0x137
	.4byte	0x451
	.byte	0x14
	.uleb128 0x8
	.4byte	.LASF142
	.byte	0x3
	.2byte	0x138
	.4byte	0x451
	.byte	0x18
	.uleb128 0x8
	.4byte	.LASF143
	.byte	0x3
	.2byte	0x139
	.4byte	0x451
	.byte	0x1c
	.uleb128 0x8
	.4byte	.LASF144
	.byte	0x3
	.2byte	0x13a
	.4byte	0x451
	.byte	0x20
	.uleb128 0x11
	.ascii	"HTR\000"
	.byte	0x3
	.2byte	0x13b
	.4byte	0x451
	.byte	0x24
	.uleb128 0x11
	.ascii	"LTR\000"
	.byte	0x3
	.2byte	0x13c
	.4byte	0x451
	.byte	0x28
	.uleb128 0x8
	.4byte	.LASF145
	.byte	0x3
	.2byte	0x13d
	.4byte	0x451
	.byte	0x2c
	.uleb128 0x8
	.4byte	.LASF146
	.byte	0x3
	.2byte	0x13e
	.4byte	0x451
	.byte	0x30
	.uleb128 0x8
	.4byte	.LASF147
	.byte	0x3
	.2byte	0x13f
	.4byte	0x451
	.byte	0x34
	.uleb128 0x8
	.4byte	.LASF148
	.byte	0x3
	.2byte	0x140
	.4byte	0x451
	.byte	0x38
	.uleb128 0x8
	.4byte	.LASF149
	.byte	0x3
	.2byte	0x141
	.4byte	0x451
	.byte	0x3c
	.uleb128 0x8
	.4byte	.LASF150
	.byte	0x3
	.2byte	0x142
	.4byte	0x451
	.byte	0x40
	.uleb128 0x8
	.4byte	.LASF151
	.byte	0x3
	.2byte	0x143
	.4byte	0x451
	.byte	0x44
	.uleb128 0x8
	.4byte	.LASF152
	.byte	0x3
	.2byte	0x144
	.4byte	0x451
	.byte	0x48
	.uleb128 0x11
	.ascii	"DR\000"
	.byte	0x3
	.2byte	0x145
	.4byte	0x451
	.byte	0x4c
	.byte	0
	.uleb128 0xf
	.4byte	.LASF153
	.byte	0x3
	.2byte	0x146
	.4byte	0x600
	.uleb128 0x10
	.byte	0xc
	.byte	0x3
	.2byte	0x148
	.4byte	0x749
	.uleb128 0x11
	.ascii	"CSR\000"
	.byte	0x3
	.2byte	0x14a
	.4byte	0x451
	.byte	0
	.uleb128 0x11
	.ascii	"CCR\000"
	.byte	0x3
	.2byte	0x14b
	.4byte	0x451
	.byte	0x4
	.uleb128 0x11
	.ascii	"CDR\000"
	.byte	0x3
	.2byte	0x14c
	.4byte	0x451
	.byte	0x8
	.byte	0
	.uleb128 0xf
	.4byte	.LASF154
	.byte	0x3
	.2byte	0x14e
	.4byte	0x718
	.uleb128 0x10
	.byte	0x18
	.byte	0x3
	.2byte	0x1d7
	.4byte	0x7ac
	.uleb128 0x11
	.ascii	"CR\000"
	.byte	0x3
	.2byte	0x1d9
	.4byte	0x451
	.byte	0
	.uleb128 0x8
	.4byte	.LASF155
	.byte	0x3
	.2byte	0x1da
	.4byte	0x451
	.byte	0x4
	.uleb128 0x11
	.ascii	"PAR\000"
	.byte	0x3
	.2byte	0x1db
	.4byte	0x451
	.byte	0x8
	.uleb128 0x8
	.4byte	.LASF156
	.byte	0x3
	.2byte	0x1dc
	.4byte	0x451
	.byte	0xc
	.uleb128 0x8
	.4byte	.LASF157
	.byte	0x3
	.2byte	0x1dd
	.4byte	0x451
	.byte	0x10
	.uleb128 0x11
	.ascii	"FCR\000"
	.byte	0x3
	.2byte	0x1de
	.4byte	0x451
	.byte	0x14
	.byte	0
	.uleb128 0xf
	.4byte	.LASF158
	.byte	0x3
	.2byte	0x1df
	.4byte	0x755
	.uleb128 0x10
	.byte	0x10
	.byte	0x3
	.2byte	0x1e1
	.4byte	0x7f6
	.uleb128 0x8
	.4byte	.LASF159
	.byte	0x3
	.2byte	0x1e3
	.4byte	0x451
	.byte	0
	.uleb128 0x8
	.4byte	.LASF160
	.byte	0x3
	.2byte	0x1e4
	.4byte	0x451
	.byte	0x4
	.uleb128 0x8
	.4byte	.LASF161
	.byte	0x3
	.2byte	0x1e5
	.4byte	0x451
	.byte	0x8
	.uleb128 0x8
	.4byte	.LASF162
	.byte	0x3
	.2byte	0x1e6
	.4byte	0x451
	.byte	0xc
	.byte	0
	.uleb128 0xf
	.4byte	.LASF163
	.byte	0x3
	.2byte	0x1e7
	.4byte	0x7b8
	.uleb128 0x10
	.byte	0x18
	.byte	0x3
	.2byte	0x237
	.4byte	0x859
	.uleb128 0x11
	.ascii	"IMR\000"
	.byte	0x3
	.2byte	0x239
	.4byte	0x451
	.byte	0
	.uleb128 0x11
	.ascii	"EMR\000"
	.byte	0x3
	.2byte	0x23a
	.4byte	0x451
	.byte	0x4
	.uleb128 0x8
	.4byte	.LASF164
	.byte	0x3
	.2byte	0x23b
	.4byte	0x451
	.byte	0x8
	.uleb128 0x8
	.4byte	.LASF165
	.byte	0x3
	.2byte	0x23c
	.4byte	0x451
	.byte	0xc
	.uleb128 0x8
	.4byte	.LASF166
	.byte	0x3
	.2byte	0x23d
	.4byte	0x451
	.byte	0x10
	.uleb128 0x11
	.ascii	"PR\000"
	.byte	0x3
	.2byte	0x23e
	.4byte	0x451
	.byte	0x14
	.byte	0
	.uleb128 0xf
	.4byte	.LASF167
	.byte	0x3
	.2byte	0x23f
	.4byte	0x802
	.uleb128 0x10
	.byte	0x28
	.byte	0x3
	.2byte	0x28e
	.4byte	0x8f1
	.uleb128 0x8
	.4byte	.LASF168
	.byte	0x3
	.2byte	0x290
	.4byte	0x451
	.byte	0
	.uleb128 0x8
	.4byte	.LASF169
	.byte	0x3
	.2byte	0x291
	.4byte	0x451
	.byte	0x4
	.uleb128 0x8
	.4byte	.LASF170
	.byte	0x3
	.2byte	0x292
	.4byte	0x451
	.byte	0x8
	.uleb128 0x8
	.4byte	.LASF171
	.byte	0x3
	.2byte	0x293
	.4byte	0x451
	.byte	0xc
	.uleb128 0x11
	.ascii	"IDR\000"
	.byte	0x3
	.2byte	0x294
	.4byte	0x451
	.byte	0x10
	.uleb128 0x11
	.ascii	"ODR\000"
	.byte	0x3
	.2byte	0x295
	.4byte	0x451
	.byte	0x14
	.uleb128 0x8
	.4byte	.LASF172
	.byte	0x3
	.2byte	0x296
	.4byte	0x5f6
	.byte	0x18
	.uleb128 0x8
	.4byte	.LASF173
	.byte	0x3
	.2byte	0x297
	.4byte	0x5f6
	.byte	0x1a
	.uleb128 0x8
	.4byte	.LASF174
	.byte	0x3
	.2byte	0x298
	.4byte	0x451
	.byte	0x1c
	.uleb128 0x11
	.ascii	"AFR\000"
	.byte	0x3
	.2byte	0x299
	.4byte	0x8f1
	.byte	0x20
	.byte	0
	.uleb128 0xd
	.4byte	0x597
	.uleb128 0xf
	.4byte	.LASF175
	.byte	0x3
	.2byte	0x29a
	.4byte	0x865
	.uleb128 0x10
	.byte	0x24
	.byte	0x3
	.2byte	0x2a0
	.4byte	0x94d
	.uleb128 0x8
	.4byte	.LASF176
	.byte	0x3
	.2byte	0x2a2
	.4byte	0x451
	.byte	0
	.uleb128 0x11
	.ascii	"PMC\000"
	.byte	0x3
	.2byte	0x2a3
	.4byte	0x451
	.byte	0x4
	.uleb128 0x8
	.4byte	.LASF177
	.byte	0x3
	.2byte	0x2a4
	.4byte	0x94d
	.byte	0x8
	.uleb128 0x8
	.4byte	.LASF178
	.byte	0x3
	.2byte	0x2a5
	.4byte	0x597
	.byte	0x18
	.uleb128 0x8
	.4byte	.LASF179
	.byte	0x3
	.2byte	0x2a6
	.4byte	0x451
	.byte	0x20
	.byte	0
	.uleb128 0xd
	.4byte	0x5b1
	.uleb128 0xf
	.4byte	.LASF180
	.byte	0x3
	.2byte	0x2a7
	.4byte	0x902
	.uleb128 0x10
	.byte	0x88
	.byte	0x3
	.2byte	0x2dd
	.4byte	0xaed
	.uleb128 0x11
	.ascii	"CR\000"
	.byte	0x3
	.2byte	0x2df
	.4byte	0x451
	.byte	0
	.uleb128 0x8
	.4byte	.LASF181
	.byte	0x3
	.2byte	0x2e0
	.4byte	0x451
	.byte	0x4
	.uleb128 0x8
	.4byte	.LASF182
	.byte	0x3
	.2byte	0x2e1
	.4byte	0x451
	.byte	0x8
	.uleb128 0x11
	.ascii	"CIR\000"
	.byte	0x3
	.2byte	0x2e2
	.4byte	0x451
	.byte	0xc
	.uleb128 0x8
	.4byte	.LASF183
	.byte	0x3
	.2byte	0x2e3
	.4byte	0x451
	.byte	0x10
	.uleb128 0x8
	.4byte	.LASF184
	.byte	0x3
	.2byte	0x2e4
	.4byte	0x451
	.byte	0x14
	.uleb128 0x8
	.4byte	.LASF185
	.byte	0x3
	.2byte	0x2e5
	.4byte	0x451
	.byte	0x18
	.uleb128 0x8
	.4byte	.LASF111
	.byte	0x3
	.2byte	0x2e6
	.4byte	0x314
	.byte	0x1c
	.uleb128 0x8
	.4byte	.LASF186
	.byte	0x3
	.2byte	0x2e7
	.4byte	0x451
	.byte	0x20
	.uleb128 0x8
	.4byte	.LASF187
	.byte	0x3
	.2byte	0x2e8
	.4byte	0x451
	.byte	0x24
	.uleb128 0x8
	.4byte	.LASF188
	.byte	0x3
	.2byte	0x2e9
	.4byte	0x597
	.byte	0x28
	.uleb128 0x8
	.4byte	.LASF189
	.byte	0x3
	.2byte	0x2ea
	.4byte	0x451
	.byte	0x30
	.uleb128 0x8
	.4byte	.LASF190
	.byte	0x3
	.2byte	0x2eb
	.4byte	0x451
	.byte	0x34
	.uleb128 0x8
	.4byte	.LASF191
	.byte	0x3
	.2byte	0x2ec
	.4byte	0x451
	.byte	0x38
	.uleb128 0x8
	.4byte	.LASF115
	.byte	0x3
	.2byte	0x2ed
	.4byte	0x314
	.byte	0x3c
	.uleb128 0x8
	.4byte	.LASF192
	.byte	0x3
	.2byte	0x2ee
	.4byte	0x451
	.byte	0x40
	.uleb128 0x8
	.4byte	.LASF193
	.byte	0x3
	.2byte	0x2ef
	.4byte	0x451
	.byte	0x44
	.uleb128 0x8
	.4byte	.LASF117
	.byte	0x3
	.2byte	0x2f0
	.4byte	0x597
	.byte	0x48
	.uleb128 0x8
	.4byte	.LASF194
	.byte	0x3
	.2byte	0x2f1
	.4byte	0x451
	.byte	0x50
	.uleb128 0x8
	.4byte	.LASF195
	.byte	0x3
	.2byte	0x2f2
	.4byte	0x451
	.byte	0x54
	.uleb128 0x8
	.4byte	.LASF196
	.byte	0x3
	.2byte	0x2f3
	.4byte	0x451
	.byte	0x58
	.uleb128 0x8
	.4byte	.LASF119
	.byte	0x3
	.2byte	0x2f4
	.4byte	0x314
	.byte	0x5c
	.uleb128 0x8
	.4byte	.LASF197
	.byte	0x3
	.2byte	0x2f5
	.4byte	0x451
	.byte	0x60
	.uleb128 0x8
	.4byte	.LASF198
	.byte	0x3
	.2byte	0x2f6
	.4byte	0x451
	.byte	0x64
	.uleb128 0x8
	.4byte	.LASF120
	.byte	0x3
	.2byte	0x2f7
	.4byte	0x597
	.byte	0x68
	.uleb128 0x8
	.4byte	.LASF199
	.byte	0x3
	.2byte	0x2f8
	.4byte	0x451
	.byte	0x70
	.uleb128 0x11
	.ascii	"CSR\000"
	.byte	0x3
	.2byte	0x2f9
	.4byte	0x451
	.byte	0x74
	.uleb128 0x8
	.4byte	.LASF200
	.byte	0x3
	.2byte	0x2fa
	.4byte	0x597
	.byte	0x78
	.uleb128 0x8
	.4byte	.LASF201
	.byte	0x3
	.2byte	0x2fb
	.4byte	0x451
	.byte	0x80
	.uleb128 0x8
	.4byte	.LASF202
	.byte	0x3
	.2byte	0x2fc
	.4byte	0x451
	.byte	0x84
	.byte	0
	.uleb128 0xf
	.4byte	.LASF203
	.byte	0x3
	.2byte	0x2fd
	.4byte	0x95e
	.uleb128 0x10
	.byte	0x54
	.byte	0x3
	.2byte	0x369
	.4byte	0xcd5
	.uleb128 0x11
	.ascii	"CR1\000"
	.byte	0x3
	.2byte	0x36b
	.4byte	0x5f6
	.byte	0
	.uleb128 0x8
	.4byte	.LASF111
	.byte	0x3
	.2byte	0x36c
	.4byte	0x2fe
	.byte	0x2
	.uleb128 0x11
	.ascii	"CR2\000"
	.byte	0x3
	.2byte	0x36d
	.4byte	0x5f6
	.byte	0x4
	.uleb128 0x8
	.4byte	.LASF188
	.byte	0x3
	.2byte	0x36e
	.4byte	0x2fe
	.byte	0x6
	.uleb128 0x8
	.4byte	.LASF204
	.byte	0x3
	.2byte	0x36f
	.4byte	0x5f6
	.byte	0x8
	.uleb128 0x8
	.4byte	.LASF115
	.byte	0x3
	.2byte	0x370
	.4byte	0x2fe
	.byte	0xa
	.uleb128 0x8
	.4byte	.LASF205
	.byte	0x3
	.2byte	0x371
	.4byte	0x5f6
	.byte	0xc
	.uleb128 0x8
	.4byte	.LASF117
	.byte	0x3
	.2byte	0x372
	.4byte	0x2fe
	.byte	0xe
	.uleb128 0x11
	.ascii	"SR\000"
	.byte	0x3
	.2byte	0x373
	.4byte	0x5f6
	.byte	0x10
	.uleb128 0x8
	.4byte	.LASF119
	.byte	0x3
	.2byte	0x374
	.4byte	0x2fe
	.byte	0x12
	.uleb128 0x11
	.ascii	"EGR\000"
	.byte	0x3
	.2byte	0x375
	.4byte	0x5f6
	.byte	0x14
	.uleb128 0x8
	.4byte	.LASF120
	.byte	0x3
	.2byte	0x376
	.4byte	0x2fe
	.byte	0x16
	.uleb128 0x8
	.4byte	.LASF206
	.byte	0x3
	.2byte	0x377
	.4byte	0x5f6
	.byte	0x18
	.uleb128 0x8
	.4byte	.LASF200
	.byte	0x3
	.2byte	0x378
	.4byte	0x2fe
	.byte	0x1a
	.uleb128 0x8
	.4byte	.LASF207
	.byte	0x3
	.2byte	0x379
	.4byte	0x5f6
	.byte	0x1c
	.uleb128 0x8
	.4byte	.LASF208
	.byte	0x3
	.2byte	0x37a
	.4byte	0x2fe
	.byte	0x1e
	.uleb128 0x8
	.4byte	.LASF209
	.byte	0x3
	.2byte	0x37b
	.4byte	0x5f6
	.byte	0x20
	.uleb128 0x8
	.4byte	.LASF210
	.byte	0x3
	.2byte	0x37c
	.4byte	0x2fe
	.byte	0x22
	.uleb128 0x11
	.ascii	"CNT\000"
	.byte	0x3
	.2byte	0x37d
	.4byte	0x451
	.byte	0x24
	.uleb128 0x11
	.ascii	"PSC\000"
	.byte	0x3
	.2byte	0x37e
	.4byte	0x5f6
	.byte	0x28
	.uleb128 0x8
	.4byte	.LASF211
	.byte	0x3
	.2byte	0x37f
	.4byte	0x2fe
	.byte	0x2a
	.uleb128 0x11
	.ascii	"ARR\000"
	.byte	0x3
	.2byte	0x380
	.4byte	0x451
	.byte	0x2c
	.uleb128 0x11
	.ascii	"RCR\000"
	.byte	0x3
	.2byte	0x381
	.4byte	0x5f6
	.byte	0x30
	.uleb128 0x8
	.4byte	.LASF212
	.byte	0x3
	.2byte	0x382
	.4byte	0x2fe
	.byte	0x32
	.uleb128 0x8
	.4byte	.LASF213
	.byte	0x3
	.2byte	0x383
	.4byte	0x451
	.byte	0x34
	.uleb128 0x8
	.4byte	.LASF214
	.byte	0x3
	.2byte	0x384
	.4byte	0x451
	.byte	0x38
	.uleb128 0x8
	.4byte	.LASF215
	.byte	0x3
	.2byte	0x385
	.4byte	0x451
	.byte	0x3c
	.uleb128 0x8
	.4byte	.LASF216
	.byte	0x3
	.2byte	0x386
	.4byte	0x451
	.byte	0x40
	.uleb128 0x8
	.4byte	.LASF217
	.byte	0x3
	.2byte	0x387
	.4byte	0x5f6
	.byte	0x44
	.uleb128 0x8
	.4byte	.LASF218
	.byte	0x3
	.2byte	0x388
	.4byte	0x2fe
	.byte	0x46
	.uleb128 0x11
	.ascii	"DCR\000"
	.byte	0x3
	.2byte	0x389
	.4byte	0x5f6
	.byte	0x48
	.uleb128 0x8
	.4byte	.LASF219
	.byte	0x3
	.2byte	0x38a
	.4byte	0x2fe
	.byte	0x4a
	.uleb128 0x8
	.4byte	.LASF220
	.byte	0x3
	.2byte	0x38b
	.4byte	0x5f6
	.byte	0x4c
	.uleb128 0x8
	.4byte	.LASF221
	.byte	0x3
	.2byte	0x38c
	.4byte	0x2fe
	.byte	0x4e
	.uleb128 0x11
	.ascii	"OR\000"
	.byte	0x3
	.2byte	0x38d
	.4byte	0x5f6
	.byte	0x50
	.uleb128 0x8
	.4byte	.LASF222
	.byte	0x3
	.2byte	0x38e
	.4byte	0x2fe
	.byte	0x52
	.byte	0
	.uleb128 0xf
	.4byte	.LASF223
	.byte	0x3
	.2byte	0x38f
	.4byte	0xaf9
	.uleb128 0x5
	.byte	0x1
	.byte	0x8
	.4byte	.LASF224
	.uleb128 0x5
	.byte	0x4
	.byte	0x4
	.4byte	.LASF225
	.uleb128 0x5
	.byte	0x8
	.byte	0x4
	.4byte	.LASF226
	.uleb128 0x5
	.byte	0x8
	.byte	0x4
	.4byte	.LASF227
	.uleb128 0x13
	.byte	0x1
	.byte	0x2
	.byte	0x14
	.4byte	0xd18
	.uleb128 0x3
	.4byte	.LASF228
	.sleb128 0
	.uleb128 0x3
	.4byte	.LASF229
	.sleb128 1
	.uleb128 0x3
	.4byte	.LASF230
	.sleb128 2
	.byte	0
	.uleb128 0x14
	.4byte	.LASF231
	.byte	0x1
	.2byte	0x430
	.4byte	.LFB95
	.4byte	.LFE95-.LFB95
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xd3e
	.uleb128 0x15
	.4byte	.LASF233
	.byte	0x1
	.2byte	0x430
	.4byte	0x260
	.uleb128 0x2
	.byte	0x91
	.sleb128 -9
	.byte	0
	.uleb128 0x14
	.4byte	.LASF232
	.byte	0x1
	.2byte	0x485
	.4byte	.LFB101
	.4byte	.LFE101-.LFB101
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xd73
	.uleb128 0x15
	.4byte	.LASF233
	.byte	0x1
	.2byte	0x485
	.4byte	0x260
	.uleb128 0x2
	.byte	0x91
	.sleb128 -9
	.uleb128 0x15
	.4byte	.LASF234
	.byte	0x1
	.2byte	0x485
	.4byte	0x314
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.uleb128 0x16
	.4byte	.LASF236
	.byte	0x2
	.byte	0x32
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xd97
	.uleb128 0x17
	.4byte	.LASF235
	.byte	0x2
	.byte	0x32
	.4byte	0xd97
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.byte	0
	.uleb128 0x18
	.byte	0x4
	.4byte	0x2fe
	.uleb128 0x19
	.4byte	.LASF256
	.byte	0x2
	.byte	0x75
	.4byte	.LFB111
	.4byte	.LFE111-.LFB111
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x1a
	.4byte	.LASF241
	.byte	0x2
	.byte	0x8a
	.4byte	0x2fe
	.4byte	.LFB112
	.4byte	.LFE112-.LFB112
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xdd6
	.uleb128 0x1b
	.4byte	.LASF243
	.byte	0x2
	.byte	0x8c
	.4byte	0x2fe
	.uleb128 0x2
	.byte	0x91
	.sleb128 -10
	.byte	0
	.uleb128 0x1c
	.4byte	.LASF237
	.byte	0x2
	.byte	0x99
	.4byte	.LFB113
	.4byte	.LFE113-.LFB113
	.uleb128 0x1
	.byte	0x9c
	.4byte	0xe16
	.uleb128 0x17
	.4byte	.LASF238
	.byte	0x2
	.byte	0x99
	.4byte	0xd97
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x17
	.4byte	.LASF239
	.byte	0x2
	.byte	0x99
	.4byte	0xd97
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.uleb128 0x17
	.4byte	.LASF240
	.byte	0x2
	.byte	0x99
	.4byte	0x2fe
	.uleb128 0x2
	.byte	0x91
	.sleb128 -18
	.byte	0
	.uleb128 0x1d
	.4byte	.LASF242
	.byte	0x2
	.byte	0xfe
	.4byte	0x2da
	.4byte	.LFB114
	.4byte	.LFE114-.LFB114
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x112c
	.uleb128 0x1e
	.ascii	"k\000"
	.byte	0x2
	.2byte	0x12d
	.4byte	0x314
	.uleb128 0x2
	.byte	0x91
	.sleb128 -60
	.uleb128 0x1e
	.ascii	"wet\000"
	.byte	0x2
	.2byte	0x12e
	.4byte	0xce8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x1f
	.4byte	.LASF244
	.byte	0x2
	.2byte	0x12f
	.4byte	0xce8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -44
	.uleb128 0x1e
	.ascii	"c\000"
	.byte	0x2
	.2byte	0x130
	.4byte	0x2da
	.uleb128 0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x1e
	.ascii	"G\000"
	.byte	0x2
	.2byte	0x131
	.4byte	0xcef
	.uleb128 0x3
	.byte	0x91
	.sleb128 -72
	.uleb128 0x1e
	.ascii	"up\000"
	.byte	0x2
	.2byte	0x134
	.4byte	0xcef
	.uleb128 0x3
	.byte	0x91
	.sleb128 -80
	.uleb128 0x1f
	.4byte	.LASF245
	.byte	0x2
	.2byte	0x135
	.4byte	0xcef
	.uleb128 0x3
	.byte	0x91
	.sleb128 -88
	.uleb128 0x1f
	.4byte	.LASF246
	.byte	0x2
	.2byte	0x136
	.4byte	0xcef
	.uleb128 0x3
	.byte	0x91
	.sleb128 -96
	.uleb128 0x1e
	.ascii	"f\000"
	.byte	0x2
	.2byte	0x137
	.4byte	0xcef
	.uleb128 0x3
	.byte	0x91
	.sleb128 -104
	.uleb128 0x1f
	.4byte	.LASF247
	.byte	0x2
	.2byte	0x138
	.4byte	0x2fe
	.uleb128 0x3
	.byte	0x91
	.sleb128 -106
	.uleb128 0x1f
	.4byte	.LASF248
	.byte	0x2
	.2byte	0x139
	.4byte	0x112c
	.uleb128 0x3
	.byte	0x91
	.sleb128 -168
	.uleb128 0x20
	.4byte	.LBB2
	.4byte	.LBE2-.LBB2
	.uleb128 0x1e
	.ascii	"i\000"
	.byte	0x2
	.2byte	0x147
	.4byte	0x2da
	.uleb128 0x2
	.byte	0x91
	.sleb128 -52
	.uleb128 0x20
	.4byte	.LBB3
	.4byte	.LBE3-.LBB3
	.uleb128 0x1e
	.ascii	"tmp\000"
	.byte	0x2
	.2byte	0x149
	.4byte	0x2fe
	.uleb128 0x2
	.byte	0x91
	.sleb128 -54
	.uleb128 0x1f
	.4byte	.LASF249
	.byte	0x2
	.2byte	0x1c2
	.4byte	0x2da
	.uleb128 0x5
	.byte	0x3
	.4byte	sample_count.7120
	.uleb128 0x21
	.4byte	.LBB4
	.4byte	.LBE4-.LBB4
	.4byte	0xf50
	.uleb128 0x1e
	.ascii	"val\000"
	.byte	0x2
	.2byte	0x14f
	.4byte	0xce8
	.uleb128 0x3
	.byte	0x91
	.sleb128 -112
	.uleb128 0x1e
	.ascii	"i\000"
	.byte	0x2
	.2byte	0x150
	.4byte	0x2da
	.uleb128 0x5
	.byte	0x3
	.4byte	i.7094
	.uleb128 0x1f
	.4byte	.LASF250
	.byte	0x2
	.2byte	0x151
	.4byte	0xce8
	.uleb128 0x3
	.byte	0x91
	.sleb128 -116
	.byte	0
	.uleb128 0x21
	.4byte	.LBB5
	.4byte	.LBE5-.LBB5
	.4byte	0xf7e
	.uleb128 0x1e
	.ascii	"val\000"
	.byte	0x2
	.2byte	0x158
	.4byte	0xce8
	.uleb128 0x3
	.byte	0x91
	.sleb128 -120
	.uleb128 0x1f
	.4byte	.LASF250
	.byte	0x2
	.2byte	0x15b
	.4byte	0xce8
	.uleb128 0x3
	.byte	0x91
	.sleb128 -124
	.byte	0
	.uleb128 0x21
	.4byte	.LBB6
	.4byte	.LBE6-.LBB6
	.4byte	0xfb9
	.uleb128 0x1e
	.ascii	"val\000"
	.byte	0x2
	.2byte	0x166
	.4byte	0xce8
	.uleb128 0x3
	.byte	0x91
	.sleb128 -128
	.uleb128 0x1f
	.4byte	.LASF250
	.byte	0x2
	.2byte	0x16b
	.4byte	0xce8
	.uleb128 0x3
	.byte	0x91
	.sleb128 -132
	.uleb128 0x22
	.ascii	"abs\000"
	.byte	0x7
	.byte	0
	.4byte	0x2da
	.uleb128 0x23
	.byte	0
	.byte	0
	.uleb128 0x21
	.4byte	.LBB7
	.4byte	.LBE7-.LBB7
	.4byte	0xffa
	.uleb128 0x1f
	.4byte	.LASF251
	.byte	0x2
	.2byte	0x173
	.4byte	0x2fe
	.uleb128 0x3
	.byte	0x91
	.sleb128 -134
	.uleb128 0x1f
	.4byte	.LASF252
	.byte	0x2
	.2byte	0x17b
	.4byte	0xcef
	.uleb128 0x5
	.byte	0x3
	.4byte	fl_gain.7102
	.uleb128 0x1e
	.ascii	"fl\000"
	.byte	0x2
	.2byte	0x17c
	.4byte	0xcef
	.uleb128 0x5
	.byte	0x3
	.4byte	fl.7103
	.byte	0
	.uleb128 0x21
	.4byte	.LBB8
	.4byte	.LBE8-.LBB8
	.4byte	0x104d
	.uleb128 0x1f
	.4byte	.LASF253
	.byte	0x2
	.2byte	0x183
	.4byte	0xcef
	.uleb128 0x5
	.byte	0x3
	.4byte	flange_counter.7104
	.uleb128 0x1f
	.4byte	.LASF251
	.byte	0x2
	.2byte	0x184
	.4byte	0x2fe
	.uleb128 0x3
	.byte	0x91
	.sleb128 -136
	.uleb128 0x1f
	.4byte	.LASF252
	.byte	0x2
	.2byte	0x190
	.4byte	0xcef
	.uleb128 0x5
	.byte	0x3
	.4byte	fl_gain.7106
	.uleb128 0x1e
	.ascii	"fl\000"
	.byte	0x2
	.2byte	0x191
	.4byte	0xcef
	.uleb128 0x5
	.byte	0x3
	.4byte	fl.7107
	.byte	0
	.uleb128 0x21
	.4byte	.LBB9
	.4byte	.LBE9-.LBB9
	.4byte	0x107d
	.uleb128 0x1f
	.4byte	.LASF253
	.byte	0x2
	.2byte	0x198
	.4byte	0xcef
	.uleb128 0x5
	.byte	0x3
	.4byte	flange_counter.7108
	.uleb128 0x1f
	.4byte	.LASF251
	.byte	0x2
	.2byte	0x199
	.4byte	0x2fe
	.uleb128 0x3
	.byte	0x91
	.sleb128 -138
	.byte	0
	.uleb128 0x20
	.4byte	.LBB10
	.4byte	.LBE10-.LBB10
	.uleb128 0x1e
	.ascii	"val\000"
	.byte	0x2
	.2byte	0x1ac
	.4byte	0xce8
	.uleb128 0x3
	.byte	0x91
	.sleb128 -144
	.uleb128 0x1f
	.4byte	.LASF254
	.byte	0x2
	.2byte	0x1ae
	.4byte	0xce8
	.uleb128 0x5
	.byte	0x3
	.4byte	ylpast.7111
	.uleb128 0x1f
	.4byte	.LASF255
	.byte	0x2
	.2byte	0x1af
	.4byte	0xce8
	.uleb128 0x5
	.byte	0x3
	.4byte	ybpast.7112
	.uleb128 0x1e
	.ascii	"F1\000"
	.byte	0x2
	.2byte	0x1b0
	.4byte	0xce8
	.uleb128 0x5
	.byte	0x3
	.4byte	F1.7113
	.uleb128 0x1e
	.ascii	"Q1\000"
	.byte	0x2
	.2byte	0x1b1
	.4byte	0xce8
	.uleb128 0x5
	.byte	0x3
	.4byte	Q1.7114
	.uleb128 0x1e
	.ascii	"Fc\000"
	.byte	0x2
	.2byte	0x1b2
	.4byte	0xce8
	.uleb128 0x3
	.byte	0x91
	.sleb128 -148
	.uleb128 0x1e
	.ascii	"yh\000"
	.byte	0x2
	.2byte	0x1b3
	.4byte	0xce8
	.uleb128 0x3
	.byte	0x91
	.sleb128 -152
	.uleb128 0x1e
	.ascii	"yb\000"
	.byte	0x2
	.2byte	0x1b4
	.4byte	0xce8
	.uleb128 0x3
	.byte	0x91
	.sleb128 -156
	.uleb128 0x1e
	.ascii	"yl\000"
	.byte	0x2
	.2byte	0x1b5
	.4byte	0xce8
	.uleb128 0x3
	.byte	0x91
	.sleb128 -160
	.uleb128 0x1f
	.4byte	.LASF250
	.byte	0x2
	.2byte	0x1bb
	.4byte	0xce8
	.uleb128 0x3
	.byte	0x91
	.sleb128 -164
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0xb
	.4byte	0x2fe
	.4byte	0x113c
	.uleb128 0xc
	.4byte	0x3eb
	.byte	0x1
	.byte	0
	.uleb128 0x24
	.4byte	.LASF257
	.byte	0x2
	.2byte	0x1e7
	.4byte	.LFB115
	.4byte	.LFE115-.LFB115
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x24
	.4byte	.LASF258
	.byte	0x2
	.2byte	0x1ef
	.4byte	.LFB116
	.4byte	.LFE116-.LFB116
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x25
	.4byte	.LASF259
	.byte	0x2
	.2byte	0x213
	.4byte	.LFB117
	.4byte	.LFE117-.LFB117
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x26
	.4byte	.LASF260
	.byte	0x1
	.2byte	0x51b
	.4byte	0x5fb
	.uleb128 0x27
	.4byte	.LASF261
	.byte	0x6
	.byte	0x14
	.4byte	0x5f1
	.uleb128 0x5
	.byte	0x3
	.4byte	dev_addr
	.uleb128 0x27
	.4byte	.LASF262
	.byte	0x2
	.byte	0x18
	.4byte	0xcfd
	.uleb128 0x5
	.byte	0x3
	.4byte	gate
	.uleb128 0x27
	.4byte	.LASF263
	.byte	0x2
	.byte	0x1f
	.4byte	0x11b1
	.uleb128 0x5
	.byte	0x3
	.4byte	g_dac_buff
	.uleb128 0xd
	.4byte	0x112c
	.uleb128 0x27
	.4byte	.LASF264
	.byte	0x2
	.byte	0x20
	.4byte	0x11c7
	.uleb128 0x5
	.byte	0x3
	.4byte	buff0
	.uleb128 0xd
	.4byte	0x112c
	.uleb128 0x27
	.4byte	.LASF265
	.byte	0x2
	.byte	0x21
	.4byte	0x11dd
	.uleb128 0x5
	.byte	0x3
	.4byte	buff1
	.uleb128 0xd
	.4byte	0x112c
	.uleb128 0xb
	.4byte	0x2fe
	.4byte	0x11f3
	.uleb128 0xe
	.4byte	0x3eb
	.2byte	0x5dbf
	.byte	0
	.uleb128 0x27
	.4byte	.LASF266
	.byte	0x2
	.byte	0x23
	.4byte	0x1204
	.uleb128 0x5
	.byte	0x3
	.4byte	samples
	.uleb128 0xd
	.4byte	0x11e2
	.uleb128 0x27
	.4byte	.LASF267
	.byte	0x2
	.byte	0x24
	.4byte	0x121a
	.uleb128 0x5
	.byte	0x3
	.4byte	counter
	.uleb128 0xd
	.4byte	0x2da
	.uleb128 0x27
	.4byte	.LASF268
	.byte	0x2
	.byte	0x26
	.4byte	0x121a
	.uleb128 0x5
	.byte	0x3
	.4byte	g_data_ready
	.uleb128 0x27
	.4byte	.LASF269
	.byte	0x2
	.byte	0x2a
	.4byte	0x451
	.uleb128 0x5
	.byte	0x3
	.4byte	g_irq_cnt
	.uleb128 0x27
	.4byte	.LASF270
	.byte	0x2
	.byte	0x2b
	.4byte	0x5f1
	.uleb128 0x5
	.byte	0x3
	.4byte	g_gpioa_irq_state
	.uleb128 0x27
	.4byte	.LASF271
	.byte	0x2
	.byte	0x2c
	.4byte	0x451
	.uleb128 0x5
	.byte	0x3
	.4byte	g_irq_timer
	.uleb128 0xb
	.4byte	0x2fe
	.4byte	0x1273
	.uleb128 0xc
	.4byte	0x3eb
	.byte	0x9
	.byte	0
	.uleb128 0x27
	.4byte	.LASF272
	.byte	0x2
	.byte	0x2d
	.4byte	0x1263
	.uleb128 0x5
	.byte	0x3
	.4byte	arrayLED1
	.uleb128 0x27
	.4byte	.LASF273
	.byte	0x2
	.byte	0x2e
	.4byte	0x1263
	.uleb128 0x5
	.byte	0x3
	.4byte	arrayLED2
	.uleb128 0x27
	.4byte	.LASF235
	.byte	0x2
	.byte	0x2f
	.4byte	0x2fe
	.uleb128 0x5
	.byte	0x3
	.4byte	glob
	.uleb128 0x27
	.4byte	.LASF274
	.byte	0x2
	.byte	0x30
	.4byte	0x451
	.uleb128 0x5
	.byte	0x3
	.4byte	change_effect
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF157:
	.ascii	"M1AR\000"
.LASF113:
	.ascii	"RSERVED1\000"
.LASF11:
	.ascii	"RTC_WKUP_IRQn\000"
.LASF16:
	.ascii	"EXTI2_IRQn\000"
.LASF231:
	.ascii	"NVIC_EnableIRQ\000"
.LASF25:
	.ascii	"DMA1_Stream6_IRQn\000"
.LASF52:
	.ascii	"TIM8_UP_TIM13_IRQn\000"
.LASF122:
	.ascii	"sizetype\000"
.LASF177:
	.ascii	"EXTICR\000"
.LASF235:
	.ascii	"glob\000"
.LASF202:
	.ascii	"PLLI2SCFGR\000"
.LASF145:
	.ascii	"SQR1\000"
.LASF146:
	.ascii	"SQR2\000"
.LASF147:
	.ascii	"SQR3\000"
.LASF108:
	.ascii	"int32_t\000"
.LASF65:
	.ascii	"DMA2_Stream1_IRQn\000"
.LASF126:
	.ascii	"VTOR\000"
.LASF59:
	.ascii	"SPI3_IRQn\000"
.LASF12:
	.ascii	"FLASH_IRQn\000"
.LASF86:
	.ascii	"DCMI_IRQn\000"
.LASF238:
	.ascii	"dBuff1\000"
.LASF239:
	.ascii	"dBuff2\000"
.LASF36:
	.ascii	"TIM2_IRQn\000"
.LASF220:
	.ascii	"DMAR\000"
.LASF116:
	.ascii	"ICPR\000"
.LASF165:
	.ascii	"FTSR\000"
.LASF72:
	.ascii	"CAN2_RX0_IRQn\000"
.LASF79:
	.ascii	"USART6_IRQn\000"
.LASF213:
	.ascii	"CCR1\000"
.LASF214:
	.ascii	"CCR2\000"
.LASF215:
	.ascii	"CCR3\000"
.LASF216:
	.ascii	"CCR4\000"
.LASF91:
	.ascii	"__uint8_t\000"
.LASF44:
	.ascii	"SPI2_IRQn\000"
.LASF51:
	.ascii	"TIM8_BRK_TIM12_IRQn\000"
.LASF195:
	.ascii	"AHB2LPENR\000"
.LASF127:
	.ascii	"AIRCR\000"
.LASF176:
	.ascii	"MEMRMP\000"
.LASF99:
	.ascii	"long int\000"
.LASF225:
	.ascii	"float\000"
.LASF124:
	.ascii	"CPUID\000"
.LASF76:
	.ascii	"DMA2_Stream5_IRQn\000"
.LASF242:
	.ascii	"main\000"
.LASF241:
	.ascii	"getADC1\000"
.LASF233:
	.ascii	"IRQn\000"
.LASF34:
	.ascii	"TIM1_TRG_COM_TIM11_IRQn\000"
.LASF164:
	.ascii	"RTSR\000"
.LASF167:
	.ascii	"EXTI_TypeDef\000"
.LASF193:
	.ascii	"APB2ENR\000"
.LASF194:
	.ascii	"AHB1LPENR\000"
.LASF7:
	.ascii	"SysTick_IRQn\000"
.LASF260:
	.ascii	"ITM_RxBuffer\000"
.LASF47:
	.ascii	"USART3_IRQn\000"
.LASF125:
	.ascii	"ICSR\000"
.LASF92:
	.ascii	"signed char\000"
.LASF105:
	.ascii	"uint8_t\000"
.LASF256:
	.ascii	"initADC1\000"
.LASF236:
	.ascii	"initADC3\000"
.LASF223:
	.ascii	"TIM_TypeDef\000"
.LASF21:
	.ascii	"DMA1_Stream2_IRQn\000"
.LASF111:
	.ascii	"RESERVED0\000"
.LASF188:
	.ascii	"RESERVED1\000"
.LASF93:
	.ascii	"unsigned char\000"
.LASF68:
	.ascii	"DMA2_Stream4_IRQn\000"
.LASF119:
	.ascii	"RESERVED4\000"
.LASF120:
	.ascii	"RESERVED5\000"
.LASF200:
	.ascii	"RESERVED6\000"
.LASF208:
	.ascii	"RESERVED7\000"
.LASF210:
	.ascii	"RESERVED8\000"
.LASF211:
	.ascii	"RESERVED9\000"
.LASF243:
	.ascii	"r_val\000"
.LASF163:
	.ascii	"DMA_TypeDef\000"
.LASF255:
	.ascii	"ybpast\000"
.LASF118:
	.ascii	"IABR\000"
.LASF40:
	.ascii	"I2C1_ER_IRQn\000"
.LASF74:
	.ascii	"CAN2_SCE_IRQn\000"
.LASF251:
	.ascii	"delayed_sample\000"
.LASF115:
	.ascii	"RESERVED2\000"
.LASF117:
	.ascii	"RESERVED3\000"
.LASF58:
	.ascii	"TIM5_IRQn\000"
.LASF161:
	.ascii	"LIFCR\000"
.LASF3:
	.ascii	"UsageFault_IRQn\000"
.LASF206:
	.ascii	"CCMR1\000"
.LASF207:
	.ascii	"CCMR2\000"
.LASF224:
	.ascii	"char\000"
.LASF138:
	.ascii	"SCB_Type\000"
.LASF131:
	.ascii	"DFSR\000"
.LASF183:
	.ascii	"AHB1RSTR\000"
.LASF96:
	.ascii	"__uint16_t\000"
.LASF190:
	.ascii	"AHB2ENR\000"
.LASF28:
	.ascii	"CAN1_RX0_IRQn\000"
.LASF130:
	.ascii	"HFSR\000"
.LASF38:
	.ascii	"TIM4_IRQn\000"
.LASF46:
	.ascii	"USART2_IRQn\000"
.LASF199:
	.ascii	"BDCR\000"
.LASF128:
	.ascii	"SHCSR\000"
.LASF121:
	.ascii	"STIR\000"
.LASF39:
	.ascii	"I2C1_EV_IRQn\000"
.LASF153:
	.ascii	"ADC_TypeDef\000"
.LASF178:
	.ascii	"RESERVED\000"
.LASF142:
	.ascii	"JOFR2\000"
.LASF257:
	.ascii	"TIM4_IRQHandler\000"
.LASF272:
	.ascii	"arrayLED1\000"
.LASF273:
	.ascii	"arrayLED2\000"
.LASF182:
	.ascii	"CFGR\000"
.LASF35:
	.ascii	"TIM1_CC_IRQn\000"
.LASF88:
	.ascii	"HASH_RNG_IRQn\000"
.LASF15:
	.ascii	"EXTI1_IRQn\000"
.LASF228:
	.ascii	"OPEN\000"
.LASF27:
	.ascii	"CAN1_TX_IRQn\000"
.LASF24:
	.ascii	"DMA1_Stream5_IRQn\000"
.LASF53:
	.ascii	"TIM8_TRG_COM_TIM14_IRQn\000"
.LASF78:
	.ascii	"DMA2_Stream7_IRQn\000"
.LASF75:
	.ascii	"OTG_FS_IRQn\000"
.LASF221:
	.ascii	"RESERVED13\000"
.LASF0:
	.ascii	"NonMaskableInt_IRQn\000"
.LASF253:
	.ascii	"flange_counter\000"
.LASF274:
	.ascii	"change_effect\000"
.LASF8:
	.ascii	"WWDG_IRQn\000"
.LASF264:
	.ascii	"buff0\000"
.LASF265:
	.ascii	"buff1\000"
.LASF84:
	.ascii	"OTG_HS_WKUP_IRQn\000"
.LASF192:
	.ascii	"APB1ENR\000"
.LASF1:
	.ascii	"MemoryManagement_IRQn\000"
.LASF205:
	.ascii	"DIER\000"
.LASF248:
	.ascii	"tmp_buff\000"
.LASF10:
	.ascii	"TAMP_STAMP_IRQn\000"
.LASF30:
	.ascii	"CAN1_SCE_IRQn\000"
.LASF90:
	.ascii	"IRQn_Type\000"
.LASF64:
	.ascii	"DMA2_Stream0_IRQn\000"
.LASF139:
	.ascii	"SMPR1\000"
.LASF140:
	.ascii	"SMPR2\000"
.LASF179:
	.ascii	"CMPCR\000"
.LASF174:
	.ascii	"LCKR\000"
.LASF42:
	.ascii	"I2C2_ER_IRQn\000"
.LASF170:
	.ascii	"OSPEEDR\000"
.LASF2:
	.ascii	"BusFault_IRQn\000"
.LASF181:
	.ascii	"PLLCFGR\000"
.LASF61:
	.ascii	"UART5_IRQn\000"
.LASF32:
	.ascii	"TIM1_BRK_TIM9_IRQn\000"
.LASF249:
	.ascii	"sample_count\000"
.LASF100:
	.ascii	"__uint32_t\000"
.LASF268:
	.ascii	"g_data_ready\000"
.LASF262:
	.ascii	"gate\000"
.LASF63:
	.ascii	"TIM7_IRQn\000"
.LASF102:
	.ascii	"long long int\000"
.LASF70:
	.ascii	"ETH_WKUP_IRQn\000"
.LASF267:
	.ascii	"counter\000"
.LASF85:
	.ascii	"OTG_HS_IRQn\000"
.LASF171:
	.ascii	"PUPDR\000"
.LASF201:
	.ascii	"SSCGR\000"
.LASF43:
	.ascii	"SPI1_IRQn\000"
.LASF226:
	.ascii	"double\000"
.LASF276:
	.ascii	"main.c\000"
.LASF4:
	.ascii	"SVCall_IRQn\000"
.LASF187:
	.ascii	"APB2RSTR\000"
.LASF263:
	.ascii	"g_dac_buff\000"
.LASF173:
	.ascii	"BSRRH\000"
.LASF172:
	.ascii	"BSRRL\000"
.LASF89:
	.ascii	"FPU_IRQn\000"
.LASF271:
	.ascii	"g_irq_timer\000"
.LASF60:
	.ascii	"UART4_IRQn\000"
.LASF198:
	.ascii	"APB2LPENR\000"
.LASF261:
	.ascii	"dev_addr\000"
.LASF50:
	.ascii	"OTG_FS_WKUP_IRQn\000"
.LASF18:
	.ascii	"EXTI4_IRQn\000"
.LASF62:
	.ascii	"TIM6_DAC_IRQn\000"
.LASF269:
	.ascii	"g_irq_cnt\000"
.LASF41:
	.ascii	"I2C2_EV_IRQn\000"
.LASF104:
	.ascii	"unsigned int\000"
.LASF133:
	.ascii	"BFAR\000"
.LASF197:
	.ascii	"APB1LPENR\000"
.LASF20:
	.ascii	"DMA1_Stream1_IRQn\000"
.LASF67:
	.ascii	"DMA2_Stream3_IRQn\000"
.LASF71:
	.ascii	"CAN2_TX_IRQn\000"
.LASF158:
	.ascii	"DMA_Stream_TypeDef\000"
.LASF250:
	.ascii	"new_val\000"
.LASF162:
	.ascii	"HIFCR\000"
.LASF185:
	.ascii	"AHB3RSTR\000"
.LASF227:
	.ascii	"long double\000"
.LASF252:
	.ascii	"fl_gain\000"
.LASF270:
	.ascii	"g_gpioa_irq_state\000"
.LASF19:
	.ascii	"DMA1_Stream0_IRQn\000"
.LASF166:
	.ascii	"SWIER\000"
.LASF186:
	.ascii	"APB1RSTR\000"
.LASF49:
	.ascii	"RTC_Alarm_IRQn\000"
.LASF123:
	.ascii	"NVIC_Type\000"
.LASF81:
	.ascii	"I2C3_ER_IRQn\000"
.LASF254:
	.ascii	"ylpast\000"
.LASF240:
	.ascii	"size\000"
.LASF87:
	.ascii	"CRYP_IRQn\000"
.LASF48:
	.ascii	"EXTI15_10_IRQn\000"
.LASF103:
	.ascii	"long long unsigned int\000"
.LASF107:
	.ascii	"uint16_t\000"
.LASF101:
	.ascii	"long unsigned int\000"
.LASF37:
	.ascii	"TIM3_IRQn\000"
.LASF209:
	.ascii	"CCER\000"
.LASF219:
	.ascii	"RESERVED12\000"
.LASF45:
	.ascii	"USART1_IRQn\000"
.LASF222:
	.ascii	"RESERVED14\000"
.LASF73:
	.ascii	"CAN2_RX1_IRQn\000"
.LASF26:
	.ascii	"ADC_IRQn\000"
.LASF247:
	.ascii	"noise_gate\000"
.LASF112:
	.ascii	"ICER\000"
.LASF204:
	.ascii	"SMCR\000"
.LASF246:
	.ascii	"drive\000"
.LASF196:
	.ascii	"AHB3LPENR\000"
.LASF94:
	.ascii	"__int16_t\000"
.LASF9:
	.ascii	"PVD_IRQn\000"
.LASF258:
	.ascii	"EXTI9_5_IRQHandler\000"
.LASF134:
	.ascii	"AFSR\000"
.LASF169:
	.ascii	"OTYPER\000"
.LASF56:
	.ascii	"FSMC_IRQn\000"
.LASF14:
	.ascii	"EXTI0_IRQn\000"
.LASF129:
	.ascii	"CFSR\000"
.LASF277:
	.ascii	"/home/anes/projekat\000"
.LASF23:
	.ascii	"DMA1_Stream4_IRQn\000"
.LASF77:
	.ascii	"DMA2_Stream6_IRQn\000"
.LASF189:
	.ascii	"AHB1ENR\000"
.LASF5:
	.ascii	"DebugMonitor_IRQn\000"
.LASF80:
	.ascii	"I2C3_EV_IRQn\000"
.LASF184:
	.ascii	"AHB2RSTR\000"
.LASF266:
	.ascii	"samples\000"
.LASF156:
	.ascii	"M0AR\000"
.LASF69:
	.ascii	"ETH_IRQn\000"
.LASF135:
	.ascii	"MMFR\000"
.LASF203:
	.ascii	"RCC_TypeDef\000"
.LASF132:
	.ascii	"MMFAR\000"
.LASF237:
	.ascii	"initDmaADC1\000"
.LASF95:
	.ascii	"short int\000"
.LASF13:
	.ascii	"RCC_IRQn\000"
.LASF234:
	.ascii	"priority\000"
.LASF141:
	.ascii	"JOFR1\000"
.LASF106:
	.ascii	"int16_t\000"
.LASF143:
	.ascii	"JOFR3\000"
.LASF144:
	.ascii	"JOFR4\000"
.LASF136:
	.ascii	"ISAR\000"
.LASF22:
	.ascii	"DMA1_Stream3_IRQn\000"
.LASF149:
	.ascii	"JDR1\000"
.LASF150:
	.ascii	"JDR2\000"
.LASF151:
	.ascii	"JDR3\000"
.LASF152:
	.ascii	"JDR4\000"
.LASF148:
	.ascii	"JSQR\000"
.LASF175:
	.ascii	"GPIO_TypeDef\000"
.LASF33:
	.ascii	"TIM1_UP_TIM10_IRQn\000"
.LASF259:
	.ascii	"serviceIRQA\000"
.LASF229:
	.ascii	"CLOSED\000"
.LASF244:
	.ascii	"feedback\000"
.LASF82:
	.ascii	"OTG_HS_EP1_OUT_IRQn\000"
.LASF275:
	.ascii	"GNU C 4.9.3 20141119 (release) [ARM/embedded-4_9-br"
	.ascii	"anch revision 218278] -mlittle-endian -mthumb -mcpu"
	.ascii	"=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu"
	.ascii	"=fpv4-sp-d16 -g -O0 -fsingle-precision-constant\000"
.LASF217:
	.ascii	"BDTR\000"
.LASF155:
	.ascii	"NDTR\000"
.LASF109:
	.ascii	"uint32_t\000"
.LASF160:
	.ascii	"HISR\000"
.LASF245:
	.ascii	"down\000"
.LASF168:
	.ascii	"MODER\000"
.LASF232:
	.ascii	"NVIC_SetPriority\000"
.LASF29:
	.ascii	"CAN1_RX1_IRQn\000"
.LASF31:
	.ascii	"EXTI9_5_IRQn\000"
.LASF159:
	.ascii	"LISR\000"
.LASF97:
	.ascii	"short unsigned int\000"
.LASF54:
	.ascii	"TIM8_CC_IRQn\000"
.LASF57:
	.ascii	"SDIO_IRQn\000"
.LASF137:
	.ascii	"CPACR\000"
.LASF212:
	.ascii	"RESERVED10\000"
.LASF218:
	.ascii	"RESERVED11\000"
.LASF114:
	.ascii	"ISPR\000"
.LASF17:
	.ascii	"EXTI3_IRQn\000"
.LASF6:
	.ascii	"PendSV_IRQn\000"
.LASF83:
	.ascii	"OTG_HS_EP1_IN_IRQn\000"
.LASF55:
	.ascii	"DMA1_Stream7_IRQn\000"
.LASF230:
	.ascii	"CLOSING\000"
.LASF98:
	.ascii	"__int32_t\000"
.LASF154:
	.ascii	"ADC_Common_TypeDef\000"
.LASF110:
	.ascii	"ISER\000"
.LASF180:
	.ascii	"SYSCFG_TypeDef\000"
.LASF191:
	.ascii	"AHB3ENR\000"
.LASF66:
	.ascii	"DMA2_Stream2_IRQn\000"
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 4.9.3 20141119 (release) [ARM/embedded-4_9-branch revision 218278]"
