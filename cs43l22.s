	.syntax unified
	.cpu cortex-m4
	.eabi_attribute 27, 3
	.eabi_attribute 28, 1
	.fpu fpv4-sp-d16
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.thumb
	.file	"cs43l22.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.comm	dev_addr,1,1
	.section	.rodata
	.align	2
.LC0:
	.ascii	"-> P1\012\000"
	.align	2
.LC1:
	.ascii	"-> P2\012\000"
	.align	2
.LC2:
	.ascii	"-> CS43L22: ID [%xb]\012\000"
	.text
	.align	2
	.global	initCS43L22
	.thumb
	.thumb_func
	.type	initCS43L22, %function
initCS43L22:
.LFB110:
	.file 1 "cs43l22.c"
	.loc 1 4 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	.cfi_offset 14, -4
	sub	sp, sp, #32
	.cfi_def_cfa_offset 40
	add	r7, sp, #0
	.cfi_def_cfa_register 7
	str	r1, [r7, #8]
	str	r2, [r7, #4]
	mov	r2, r3
	mov	r3, r0
	strb	r3, [r7, #15]
	mov	r3, r2	@ movhi
	strh	r3, [r7, #12]	@ movhi
	.loc 1 6 0
	ldrb	r3, [r7, #15]	@ zero_extendqisi2
	str	r3, [r7, #28]
	.loc 1 7 0
	ldr	r2, [r7, #28]
	mov	r3, r2
	lsls	r3, r3, #8
	subs	r3, r3, r2
	ldr	r2, .L5
	umull	r2, r3, r2, r3
	lsrs	r3, r3, #5
	strb	r3, [r7, #15]
	.loc 1 18 0
	ldr	r0, .L5+4
	bl	printUSART2
	.loc 1 19 0
	ldr	r2, .L5+8
	ldr	r3, .L5+8
	ldr	r3, [r3, #48]
	orr	r3, r3, #8
	str	r3, [r2, #48]
	.loc 1 20 0
	ldr	r2, .L5+12
	ldr	r3, .L5+12
	ldr	r3, [r3]
	orr	r3, r3, #256
	str	r3, [r2]
	.loc 1 21 0
	ldr	r2, .L5+12
	ldr	r3, .L5+12
	ldr	r3, [r3, #8]
	orr	r3, r3, #512
	str	r3, [r2, #8]
	.loc 1 23 0
	ldr	r2, .L5+8
	ldr	r3, .L5+8
	ldr	r3, [r3, #48]
	orr	r3, r3, #4
	str	r3, [r2, #48]
	.loc 1 24 0
	ldr	r2, .L5+16
	ldr	r3, .L5+16
	ldr	r3, [r3]
	orr	r3, r3, #35651584
	orr	r3, r3, #32768
	str	r3, [r2]
	.loc 1 25 0
	ldr	r3, .L5+16
	mov	r2, #1610612736
	str	r2, [r3, #32]
	.loc 1 26 0
	ldr	r3, .L5+16
	ldr	r2, .L5+20
	str	r2, [r3, #36]
	.loc 1 29 0
	ldr	r0, .L5+24
	bl	printUSART2
	.loc 1 30 0
	ldr	r2, .L5+8
	ldr	r3, .L5+8
	ldr	r3, [r3, #48]
	orr	r3, r3, #1
	str	r3, [r2, #48]
	.loc 1 31 0
	ldr	r2, .L5+28
	ldr	r3, .L5+28
	ldr	r3, [r3]
	bic	r3, r3, #768
	str	r3, [r2]
	.loc 1 32 0
	ldr	r2, .L5+28
	ldr	r3, .L5+28
	ldr	r3, [r3]
	orr	r3, r3, #512
	str	r3, [r2]
	.loc 1 33 0
	ldr	r2, .L5+28
	ldr	r3, .L5+28
	ldr	r3, [r3, #32]
	orr	r3, r3, #393216
	str	r3, [r2, #32]
	.loc 1 41 0
	movs	r0, #148
	bl	initI2C
	.loc 1 44 0
	ldr	r2, .L5+12
	ldr	r3, .L5+12
	ldr	r3, [r3, #20]
	bic	r3, r3, #16
	str	r3, [r2, #20]
	.loc 1 45 0
	movs	r0, #50
	bl	delay_ms
	.loc 1 46 0
	ldr	r2, .L5+12
	ldr	r3, .L5+12
	ldr	r3, [r3, #20]
	orr	r3, r3, #16
	str	r3, [r2, #20]
	.loc 1 47 0
	movs	r0, #50
	bl	delay_ms
	.loc 1 49 0
	add	r3, r7, #20
	movs	r0, #1
	mov	r1, r3
	movs	r2, #1
	bl	readI2C
	.loc 1 50 0
	ldrb	r3, [r7, #20]	@ zero_extendqisi2
	ldr	r0, .L5+32
	mov	r1, r3
	bl	printUSART2
	.loc 1 53 0
	movs	r3, #1
	strb	r3, [r7, #20]
	.loc 1 54 0
	add	r3, r7, #20
	movs	r0, #2
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 57 0
	movs	r3, #175
	strb	r3, [r7, #20]
	.loc 1 58 0
	add	r3, r7, #20
	movs	r0, #4
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 61 0
	movs	r3, #128
	strb	r3, [r7, #20]
	.loc 1 62 0
	add	r3, r7, #20
	movs	r0, #5
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 66 0
	movs	r3, #3
	strb	r3, [r7, #20]
	.loc 1 67 0
	add	r3, r7, #20
	movs	r0, #6
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 71 0
	movs	r3, #48
	strb	r3, [r7, #20]
	.loc 1 72 0
	add	r3, r7, #20
	movs	r0, #14
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 75 0
	movs	r3, #0
	strb	r3, [r7, #20]
	.loc 1 76 0
	add	r3, r7, #20
	movs	r0, #15
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 78 0
	movs	r3, #127
	strb	r3, [r7, #20]
	.loc 1 79 0
	add	r3, r7, #20
	movs	r0, #20
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 80 0
	movs	r3, #127
	strb	r3, [r7, #20]
	.loc 1 81 0
	add	r3, r7, #20
	movs	r0, #21
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 84 0
	movs	r3, #0
	strb	r3, [r7, #20]
	.loc 1 85 0
	add	r3, r7, #20
	movs	r0, #26
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 86 0
	movs	r3, #0
	strb	r3, [r7, #20]
	.loc 1 87 0
	add	r3, r7, #20
	movs	r0, #27
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 95 0
	movs	r3, #153
	strb	r3, [r7, #20]
	.loc 1 96 0
	add	r3, r7, #20
	movs	r0, #0
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 98 0
	movs	r3, #128
	strb	r3, [r7, #20]
	.loc 1 99 0
	add	r3, r7, #20
	movs	r0, #71
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 101 0
	movs	r3, #128
	strb	r3, [r7, #20]
	.loc 1 102 0
	add	r3, r7, #20
	movs	r0, #50
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 104 0
	movs	r3, #0
	strb	r3, [r7, #20]
	.loc 1 105 0
	add	r3, r7, #20
	movs	r0, #50
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 107 0
	movs	r3, #0
	strb	r3, [r7, #20]
	.loc 1 108 0
	add	r3, r7, #20
	movs	r0, #0
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 110 0
	movs	r3, #158
	strb	r3, [r7, #20]
	.loc 1 111 0
	add	r3, r7, #20
	movs	r0, #2
	mov	r1, r3
	movs	r2, #1
	bl	writeI2C
	.loc 1 114 0
	mov	r0, #500
	bl	delay_ms
.LBB2:
	.loc 1 117 0
	movw	r3, #48000
	str	r3, [r7, #24]
	.loc 1 118 0
	ldr	r2, .L5+8
	ldr	r3, .L5+8
	ldr	r3, [r3, #64]
	orr	r3, r3, #32768
	str	r3, [r2, #64]
	.loc 1 119 0
	ldr	r2, .L5+8
	ldr	r3, .L5+8
	ldr	r3, [r3, #48]
	orr	r3, r3, #4096
	str	r3, [r2, #48]
	.loc 1 127 0
	ldr	r3, [r7, #24]
	lsls	r3, r3, #8
	ldr	r2, .L5+36
	udiv	r3, r2, r3
	str	r3, [r7, #28]
	.loc 1 128 0
	ldr	r3, [r7, #28]
	adds	r3, r3, #5
	ldr	r2, .L5+40
	umull	r2, r3, r2, r3
	lsrs	r3, r3, #3
	str	r3, [r7, #28]
	.loc 1 131 0
	ldr	r3, [r7, #28]
	and	r3, r3, #1
	cmp	r3, #0
	beq	.L2
	.loc 1 133 0
	ldr	r3, [r7, #28]
	subs	r3, r3, #1
	str	r3, [r7, #28]
	.loc 1 134 0
	ldr	r3, [r7, #28]
	lsrs	r3, r3, #1
	str	r3, [r7, #28]
	.loc 1 135 0
	ldr	r2, .L5+44
	ldr	r3, [r7, #28]
	uxth	r3, r3
	orr	r3, r3, #256
	uxth	r3, r3
	strh	r3, [r2, #32]	@ movhi
	b	.L3
.L6:
	.align	2
.L5:
	.word	1374389535
	.word	.LC0
	.word	1073887232
	.word	1073875968
	.word	1073874944
	.word	394752
	.word	.LC1
	.word	1073872896
	.word	.LC2
	.word	860000000
	.word	-858993459
	.word	1073757184
.L2:
	.loc 1 139 0
	ldr	r2, .L7
	ldr	r3, [r7, #28]
	lsrs	r3, r3, #1
	uxth	r3, r3
	strh	r3, [r2, #32]	@ movhi
.L3:
	.loc 1 142 0
	ldr	r2, .L7
	ldr	r3, .L7
	ldrh	r3, [r3, #32]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #512
	uxth	r3, r3
	strh	r3, [r2, #32]	@ movhi
	.loc 1 143 0
	ldr	r2, .L7
	ldr	r3, .L7
	ldrh	r3, [r3, #4]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #2
	uxth	r3, r3
	strh	r3, [r2, #4]	@ movhi
	.loc 1 145 0
	ldr	r3, .L7
	movw	r2, #3608
	strh	r2, [r3, #28]	@ movhi
	.loc 1 154 0
	ldr	r2, .L7+4
	ldr	r3, .L7+4
	ldr	r3, [r3, #64]
	orr	r3, r3, #16
	str	r3, [r2, #64]
	.loc 1 155 0
	ldr	r3, .L7+8
	movs	r2, #6
	strh	r2, [r3, #40]	@ movhi
	.loc 1 157 0
	ldr	r3, .L7+8
	movs	r2, #125
	str	r2, [r3, #44]
	.loc 1 158 0
	ldr	r3, .L7+8
	movs	r2, #132
	strh	r2, [r3]	@ movhi
	.loc 1 160 0
	ldr	r3, .L7+8
	movs	r2, #32
	strh	r2, [r3, #4]	@ movhi
	.loc 1 162 0
	ldr	r2, .L7+8
	ldr	r3, .L7+8
	ldrh	r3, [r3, #20]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #1
	uxth	r3, r3
	strh	r3, [r2, #20]	@ movhi
	.loc 1 163 0
	ldr	r2, .L7+8
	ldr	r3, .L7+8
	ldrh	r3, [r3]	@ movhi
	uxth	r3, r3
	orr	r3, r3, #1
	uxth	r3, r3
	strh	r3, [r2]	@ movhi
	.loc 1 169 0
	ldr	r2, .L7+4
	ldr	r3, .L7+4
	ldr	r3, [r3, #48]
	orr	r3, r3, #2097152
	str	r3, [r2, #48]
	.loc 1 171 0
	ldr	r3, .L7+12
	movs	r2, #0
	str	r2, [r3]
	.loc 1 172 0
	nop
.L4:
	.loc 1 172 0 is_stmt 0 discriminator 1
	ldr	r3, .L7+12
	ldr	r3, [r3]
	and	r3, r3, #1
	cmp	r3, #0
	bne	.L4
	.loc 1 174 0 is_stmt 1
	ldr	r3, .L7+16
	mov	r2, #-1
	str	r2, [r3, #8]
	.loc 1 175 0
	ldr	r3, .L7+16
	mov	r2, #-1
	str	r2, [r3, #12]
	.loc 1 177 0
	ldr	r3, .L7+12
	ldr	r2, .L7+20
	str	r2, [r3, #8]
	.loc 1 178 0
	ldr	r2, .L7+12
	ldr	r3, [r7, #4]
	str	r3, [r2, #12]
	.loc 1 179 0
	ldr	r2, .L7+12
	ldrh	r3, [r7, #12]
	str	r3, [r2, #4]
	.loc 1 182 0
	ldr	r2, .L7+12
	ldr	r3, .L7+12
	ldr	r3, [r3]
	orr	r3, r3, #196608
	str	r3, [r2]
	.loc 1 185 0
	ldr	r2, .L7+12
	ldr	r3, .L7+12
	ldr	r3, [r3]
	orr	r3, r3, #1024
	str	r3, [r2]
	.loc 1 187 0
	ldr	r2, .L7+12
	ldr	r3, .L7+12
	ldr	r3, [r3]
	orr	r3, r3, #256
	str	r3, [r2]
	.loc 1 189 0
	ldr	r2, .L7+12
	ldr	r3, .L7+12
	ldr	r3, [r3]
	orr	r3, r3, #2048
	str	r3, [r2]
	.loc 1 191 0
	ldr	r2, .L7+12
	ldr	r3, .L7+12
	ldr	r3, [r3]
	orr	r3, r3, #8192
	str	r3, [r2]
	.loc 1 193 0
	ldr	r2, .L7+12
	ldr	r3, .L7+12
	ldr	r3, [r3]
	orr	r3, r3, #64
	str	r3, [r2]
	.loc 1 195 0
	ldr	r2, .L7+12
	ldr	r3, .L7+12
	ldr	r3, [r3]
	orr	r3, r3, #1
	str	r3, [r2]
.LBE2:
	.loc 1 198 0
	adds	r7, r7, #32
	.cfi_def_cfa_offset 8
	mov	sp, r7
	.cfi_def_cfa_register 13
	@ sp needed
	pop	{r7, pc}
.L8:
	.align	2
.L7:
	.word	1073757184
	.word	1073887232
	.word	1073745920
	.word	1073897608
	.word	1073897472
	.word	1073757196
	.cfi_endproc
.LFE110:
	.size	initCS43L22, .-initCS43L22
.Letext0:
	.file 2 "/home/anes/msut/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/machine/_default_types.h"
	.file 3 "/home/anes/msut/STM32F407/gcc-arm-none-eabi/arm-none-eabi/include/stdint.h"
	.file 4 "/home/anes/msut/STM32F407/Libraries/CMSIS/ST/STM32F4xx/Include/stm32f4xx.h"
	.file 5 "/home/anes/msut/STM32F407/Libraries/CMSIS/Include/core_cm4.h"
	.file 6 "i2c.h"
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.4byte	0x772
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.LASF97
	.byte	0x1
	.4byte	.LASF98
	.4byte	.LASF99
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF0
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x2
	.byte	0x1d
	.4byte	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF2
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x2
	.byte	0x2b
	.4byte	0x50
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF5
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x2
	.byte	0x3f
	.4byte	0x62
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF7
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x2
	.byte	0x41
	.4byte	0x74
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF9
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF10
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF11
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF12
	.uleb128 0x3
	.4byte	.LASF13
	.byte	0x3
	.byte	0x15
	.4byte	0x2c
	.uleb128 0x3
	.4byte	.LASF14
	.byte	0x3
	.byte	0x21
	.4byte	0x45
	.uleb128 0x3
	.4byte	.LASF15
	.byte	0x3
	.byte	0x2c
	.4byte	0x57
	.uleb128 0x3
	.4byte	.LASF16
	.byte	0x3
	.byte	0x2d
	.4byte	0x69
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF17
	.uleb128 0x5
	.4byte	0xb8
	.uleb128 0x6
	.4byte	0xb8
	.4byte	0xdf
	.uleb128 0x7
	.4byte	0xc3
	.byte	0x1
	.byte	0
	.uleb128 0x5
	.4byte	0x97
	.uleb128 0x5
	.4byte	0xa2
	.uleb128 0x5
	.4byte	0xad
	.uleb128 0x8
	.byte	0x18
	.byte	0x4
	.2byte	0x1d7
	.4byte	0x145
	.uleb128 0x9
	.ascii	"CR\000"
	.byte	0x4
	.2byte	0x1d9
	.4byte	0xca
	.byte	0
	.uleb128 0xa
	.4byte	.LASF18
	.byte	0x4
	.2byte	0x1da
	.4byte	0xca
	.byte	0x4
	.uleb128 0x9
	.ascii	"PAR\000"
	.byte	0x4
	.2byte	0x1db
	.4byte	0xca
	.byte	0x8
	.uleb128 0xa
	.4byte	.LASF19
	.byte	0x4
	.2byte	0x1dc
	.4byte	0xca
	.byte	0xc
	.uleb128 0xa
	.4byte	.LASF20
	.byte	0x4
	.2byte	0x1dd
	.4byte	0xca
	.byte	0x10
	.uleb128 0x9
	.ascii	"FCR\000"
	.byte	0x4
	.2byte	0x1de
	.4byte	0xca
	.byte	0x14
	.byte	0
	.uleb128 0xb
	.4byte	.LASF21
	.byte	0x4
	.2byte	0x1df
	.4byte	0xee
	.uleb128 0x8
	.byte	0x10
	.byte	0x4
	.2byte	0x1e1
	.4byte	0x18f
	.uleb128 0xa
	.4byte	.LASF22
	.byte	0x4
	.2byte	0x1e3
	.4byte	0xca
	.byte	0
	.uleb128 0xa
	.4byte	.LASF23
	.byte	0x4
	.2byte	0x1e4
	.4byte	0xca
	.byte	0x4
	.uleb128 0xa
	.4byte	.LASF24
	.byte	0x4
	.2byte	0x1e5
	.4byte	0xca
	.byte	0x8
	.uleb128 0xa
	.4byte	.LASF25
	.byte	0x4
	.2byte	0x1e6
	.4byte	0xca
	.byte	0xc
	.byte	0
	.uleb128 0xb
	.4byte	.LASF26
	.byte	0x4
	.2byte	0x1e7
	.4byte	0x151
	.uleb128 0x8
	.byte	0x28
	.byte	0x4
	.2byte	0x28e
	.4byte	0x227
	.uleb128 0xa
	.4byte	.LASF27
	.byte	0x4
	.2byte	0x290
	.4byte	0xca
	.byte	0
	.uleb128 0xa
	.4byte	.LASF28
	.byte	0x4
	.2byte	0x291
	.4byte	0xca
	.byte	0x4
	.uleb128 0xa
	.4byte	.LASF29
	.byte	0x4
	.2byte	0x292
	.4byte	0xca
	.byte	0x8
	.uleb128 0xa
	.4byte	.LASF30
	.byte	0x4
	.2byte	0x293
	.4byte	0xca
	.byte	0xc
	.uleb128 0x9
	.ascii	"IDR\000"
	.byte	0x4
	.2byte	0x294
	.4byte	0xca
	.byte	0x10
	.uleb128 0x9
	.ascii	"ODR\000"
	.byte	0x4
	.2byte	0x295
	.4byte	0xca
	.byte	0x14
	.uleb128 0xa
	.4byte	.LASF31
	.byte	0x4
	.2byte	0x296
	.4byte	0xe4
	.byte	0x18
	.uleb128 0xa
	.4byte	.LASF32
	.byte	0x4
	.2byte	0x297
	.4byte	0xe4
	.byte	0x1a
	.uleb128 0xa
	.4byte	.LASF33
	.byte	0x4
	.2byte	0x298
	.4byte	0xca
	.byte	0x1c
	.uleb128 0x9
	.ascii	"AFR\000"
	.byte	0x4
	.2byte	0x299
	.4byte	0x227
	.byte	0x20
	.byte	0
	.uleb128 0x5
	.4byte	0xcf
	.uleb128 0xb
	.4byte	.LASF34
	.byte	0x4
	.2byte	0x29a
	.4byte	0x19b
	.uleb128 0x8
	.byte	0x88
	.byte	0x4
	.2byte	0x2dd
	.4byte	0x3c7
	.uleb128 0x9
	.ascii	"CR\000"
	.byte	0x4
	.2byte	0x2df
	.4byte	0xca
	.byte	0
	.uleb128 0xa
	.4byte	.LASF35
	.byte	0x4
	.2byte	0x2e0
	.4byte	0xca
	.byte	0x4
	.uleb128 0xa
	.4byte	.LASF36
	.byte	0x4
	.2byte	0x2e1
	.4byte	0xca
	.byte	0x8
	.uleb128 0x9
	.ascii	"CIR\000"
	.byte	0x4
	.2byte	0x2e2
	.4byte	0xca
	.byte	0xc
	.uleb128 0xa
	.4byte	.LASF37
	.byte	0x4
	.2byte	0x2e3
	.4byte	0xca
	.byte	0x10
	.uleb128 0xa
	.4byte	.LASF38
	.byte	0x4
	.2byte	0x2e4
	.4byte	0xca
	.byte	0x14
	.uleb128 0xa
	.4byte	.LASF39
	.byte	0x4
	.2byte	0x2e5
	.4byte	0xca
	.byte	0x18
	.uleb128 0xa
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x2e6
	.4byte	0xb8
	.byte	0x1c
	.uleb128 0xa
	.4byte	.LASF41
	.byte	0x4
	.2byte	0x2e7
	.4byte	0xca
	.byte	0x20
	.uleb128 0xa
	.4byte	.LASF42
	.byte	0x4
	.2byte	0x2e8
	.4byte	0xca
	.byte	0x24
	.uleb128 0xa
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x2e9
	.4byte	0xcf
	.byte	0x28
	.uleb128 0xa
	.4byte	.LASF44
	.byte	0x4
	.2byte	0x2ea
	.4byte	0xca
	.byte	0x30
	.uleb128 0xa
	.4byte	.LASF45
	.byte	0x4
	.2byte	0x2eb
	.4byte	0xca
	.byte	0x34
	.uleb128 0xa
	.4byte	.LASF46
	.byte	0x4
	.2byte	0x2ec
	.4byte	0xca
	.byte	0x38
	.uleb128 0xa
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x2ed
	.4byte	0xb8
	.byte	0x3c
	.uleb128 0xa
	.4byte	.LASF48
	.byte	0x4
	.2byte	0x2ee
	.4byte	0xca
	.byte	0x40
	.uleb128 0xa
	.4byte	.LASF49
	.byte	0x4
	.2byte	0x2ef
	.4byte	0xca
	.byte	0x44
	.uleb128 0xa
	.4byte	.LASF50
	.byte	0x4
	.2byte	0x2f0
	.4byte	0xcf
	.byte	0x48
	.uleb128 0xa
	.4byte	.LASF51
	.byte	0x4
	.2byte	0x2f1
	.4byte	0xca
	.byte	0x50
	.uleb128 0xa
	.4byte	.LASF52
	.byte	0x4
	.2byte	0x2f2
	.4byte	0xca
	.byte	0x54
	.uleb128 0xa
	.4byte	.LASF53
	.byte	0x4
	.2byte	0x2f3
	.4byte	0xca
	.byte	0x58
	.uleb128 0xa
	.4byte	.LASF54
	.byte	0x4
	.2byte	0x2f4
	.4byte	0xb8
	.byte	0x5c
	.uleb128 0xa
	.4byte	.LASF55
	.byte	0x4
	.2byte	0x2f5
	.4byte	0xca
	.byte	0x60
	.uleb128 0xa
	.4byte	.LASF56
	.byte	0x4
	.2byte	0x2f6
	.4byte	0xca
	.byte	0x64
	.uleb128 0xa
	.4byte	.LASF57
	.byte	0x4
	.2byte	0x2f7
	.4byte	0xcf
	.byte	0x68
	.uleb128 0xa
	.4byte	.LASF58
	.byte	0x4
	.2byte	0x2f8
	.4byte	0xca
	.byte	0x70
	.uleb128 0x9
	.ascii	"CSR\000"
	.byte	0x4
	.2byte	0x2f9
	.4byte	0xca
	.byte	0x74
	.uleb128 0xa
	.4byte	.LASF59
	.byte	0x4
	.2byte	0x2fa
	.4byte	0xcf
	.byte	0x78
	.uleb128 0xa
	.4byte	.LASF60
	.byte	0x4
	.2byte	0x2fb
	.4byte	0xca
	.byte	0x80
	.uleb128 0xa
	.4byte	.LASF61
	.byte	0x4
	.2byte	0x2fc
	.4byte	0xca
	.byte	0x84
	.byte	0
	.uleb128 0xb
	.4byte	.LASF62
	.byte	0x4
	.2byte	0x2fd
	.4byte	0x238
	.uleb128 0x8
	.byte	0x24
	.byte	0x4
	.2byte	0x34f
	.4byte	0x4c5
	.uleb128 0x9
	.ascii	"CR1\000"
	.byte	0x4
	.2byte	0x351
	.4byte	0xe4
	.byte	0
	.uleb128 0xa
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x352
	.4byte	0xa2
	.byte	0x2
	.uleb128 0x9
	.ascii	"CR2\000"
	.byte	0x4
	.2byte	0x353
	.4byte	0xe4
	.byte	0x4
	.uleb128 0xa
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x354
	.4byte	0xa2
	.byte	0x6
	.uleb128 0x9
	.ascii	"SR\000"
	.byte	0x4
	.2byte	0x355
	.4byte	0xe4
	.byte	0x8
	.uleb128 0xa
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x356
	.4byte	0xa2
	.byte	0xa
	.uleb128 0x9
	.ascii	"DR\000"
	.byte	0x4
	.2byte	0x357
	.4byte	0xe4
	.byte	0xc
	.uleb128 0xa
	.4byte	.LASF50
	.byte	0x4
	.2byte	0x358
	.4byte	0xa2
	.byte	0xe
	.uleb128 0xa
	.4byte	.LASF63
	.byte	0x4
	.2byte	0x359
	.4byte	0xe4
	.byte	0x10
	.uleb128 0xa
	.4byte	.LASF54
	.byte	0x4
	.2byte	0x35a
	.4byte	0xa2
	.byte	0x12
	.uleb128 0xa
	.4byte	.LASF64
	.byte	0x4
	.2byte	0x35b
	.4byte	0xe4
	.byte	0x14
	.uleb128 0xa
	.4byte	.LASF57
	.byte	0x4
	.2byte	0x35c
	.4byte	0xa2
	.byte	0x16
	.uleb128 0xa
	.4byte	.LASF65
	.byte	0x4
	.2byte	0x35d
	.4byte	0xe4
	.byte	0x18
	.uleb128 0xa
	.4byte	.LASF59
	.byte	0x4
	.2byte	0x35e
	.4byte	0xa2
	.byte	0x1a
	.uleb128 0xa
	.4byte	.LASF66
	.byte	0x4
	.2byte	0x35f
	.4byte	0xe4
	.byte	0x1c
	.uleb128 0xa
	.4byte	.LASF67
	.byte	0x4
	.2byte	0x360
	.4byte	0xa2
	.byte	0x1e
	.uleb128 0xa
	.4byte	.LASF68
	.byte	0x4
	.2byte	0x361
	.4byte	0xe4
	.byte	0x20
	.uleb128 0xa
	.4byte	.LASF69
	.byte	0x4
	.2byte	0x362
	.4byte	0xa2
	.byte	0x22
	.byte	0
	.uleb128 0xb
	.4byte	.LASF70
	.byte	0x4
	.2byte	0x363
	.4byte	0x3d3
	.uleb128 0x8
	.byte	0x54
	.byte	0x4
	.2byte	0x369
	.4byte	0x6ad
	.uleb128 0x9
	.ascii	"CR1\000"
	.byte	0x4
	.2byte	0x36b
	.4byte	0xe4
	.byte	0
	.uleb128 0xa
	.4byte	.LASF40
	.byte	0x4
	.2byte	0x36c
	.4byte	0xa2
	.byte	0x2
	.uleb128 0x9
	.ascii	"CR2\000"
	.byte	0x4
	.2byte	0x36d
	.4byte	0xe4
	.byte	0x4
	.uleb128 0xa
	.4byte	.LASF43
	.byte	0x4
	.2byte	0x36e
	.4byte	0xa2
	.byte	0x6
	.uleb128 0xa
	.4byte	.LASF71
	.byte	0x4
	.2byte	0x36f
	.4byte	0xe4
	.byte	0x8
	.uleb128 0xa
	.4byte	.LASF47
	.byte	0x4
	.2byte	0x370
	.4byte	0xa2
	.byte	0xa
	.uleb128 0xa
	.4byte	.LASF72
	.byte	0x4
	.2byte	0x371
	.4byte	0xe4
	.byte	0xc
	.uleb128 0xa
	.4byte	.LASF50
	.byte	0x4
	.2byte	0x372
	.4byte	0xa2
	.byte	0xe
	.uleb128 0x9
	.ascii	"SR\000"
	.byte	0x4
	.2byte	0x373
	.4byte	0xe4
	.byte	0x10
	.uleb128 0xa
	.4byte	.LASF54
	.byte	0x4
	.2byte	0x374
	.4byte	0xa2
	.byte	0x12
	.uleb128 0x9
	.ascii	"EGR\000"
	.byte	0x4
	.2byte	0x375
	.4byte	0xe4
	.byte	0x14
	.uleb128 0xa
	.4byte	.LASF57
	.byte	0x4
	.2byte	0x376
	.4byte	0xa2
	.byte	0x16
	.uleb128 0xa
	.4byte	.LASF73
	.byte	0x4
	.2byte	0x377
	.4byte	0xe4
	.byte	0x18
	.uleb128 0xa
	.4byte	.LASF59
	.byte	0x4
	.2byte	0x378
	.4byte	0xa2
	.byte	0x1a
	.uleb128 0xa
	.4byte	.LASF74
	.byte	0x4
	.2byte	0x379
	.4byte	0xe4
	.byte	0x1c
	.uleb128 0xa
	.4byte	.LASF67
	.byte	0x4
	.2byte	0x37a
	.4byte	0xa2
	.byte	0x1e
	.uleb128 0xa
	.4byte	.LASF75
	.byte	0x4
	.2byte	0x37b
	.4byte	0xe4
	.byte	0x20
	.uleb128 0xa
	.4byte	.LASF69
	.byte	0x4
	.2byte	0x37c
	.4byte	0xa2
	.byte	0x22
	.uleb128 0x9
	.ascii	"CNT\000"
	.byte	0x4
	.2byte	0x37d
	.4byte	0xca
	.byte	0x24
	.uleb128 0x9
	.ascii	"PSC\000"
	.byte	0x4
	.2byte	0x37e
	.4byte	0xe4
	.byte	0x28
	.uleb128 0xa
	.4byte	.LASF76
	.byte	0x4
	.2byte	0x37f
	.4byte	0xa2
	.byte	0x2a
	.uleb128 0x9
	.ascii	"ARR\000"
	.byte	0x4
	.2byte	0x380
	.4byte	0xca
	.byte	0x2c
	.uleb128 0x9
	.ascii	"RCR\000"
	.byte	0x4
	.2byte	0x381
	.4byte	0xe4
	.byte	0x30
	.uleb128 0xa
	.4byte	.LASF77
	.byte	0x4
	.2byte	0x382
	.4byte	0xa2
	.byte	0x32
	.uleb128 0xa
	.4byte	.LASF78
	.byte	0x4
	.2byte	0x383
	.4byte	0xca
	.byte	0x34
	.uleb128 0xa
	.4byte	.LASF79
	.byte	0x4
	.2byte	0x384
	.4byte	0xca
	.byte	0x38
	.uleb128 0xa
	.4byte	.LASF80
	.byte	0x4
	.2byte	0x385
	.4byte	0xca
	.byte	0x3c
	.uleb128 0xa
	.4byte	.LASF81
	.byte	0x4
	.2byte	0x386
	.4byte	0xca
	.byte	0x40
	.uleb128 0xa
	.4byte	.LASF82
	.byte	0x4
	.2byte	0x387
	.4byte	0xe4
	.byte	0x44
	.uleb128 0xa
	.4byte	.LASF83
	.byte	0x4
	.2byte	0x388
	.4byte	0xa2
	.byte	0x46
	.uleb128 0x9
	.ascii	"DCR\000"
	.byte	0x4
	.2byte	0x389
	.4byte	0xe4
	.byte	0x48
	.uleb128 0xa
	.4byte	.LASF84
	.byte	0x4
	.2byte	0x38a
	.4byte	0xa2
	.byte	0x4a
	.uleb128 0xa
	.4byte	.LASF85
	.byte	0x4
	.2byte	0x38b
	.4byte	0xe4
	.byte	0x4c
	.uleb128 0xa
	.4byte	.LASF86
	.byte	0x4
	.2byte	0x38c
	.4byte	0xa2
	.byte	0x4e
	.uleb128 0x9
	.ascii	"OR\000"
	.byte	0x4
	.2byte	0x38d
	.4byte	0xe4
	.byte	0x50
	.uleb128 0xa
	.4byte	.LASF87
	.byte	0x4
	.2byte	0x38e
	.4byte	0xa2
	.byte	0x52
	.byte	0
	.uleb128 0xb
	.4byte	.LASF88
	.byte	0x4
	.2byte	0x38f
	.4byte	0x4d1
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF89
	.uleb128 0xc
	.4byte	.LASF100
	.byte	0x1
	.byte	0x3
	.4byte	.LFB110
	.4byte	.LFE110-.LFB110
	.uleb128 0x1
	.byte	0x9c
	.4byte	0x742
	.uleb128 0xd
	.4byte	.LASF90
	.byte	0x1
	.byte	0x3
	.4byte	0x97
	.uleb128 0x2
	.byte	0x91
	.sleb128 -25
	.uleb128 0xd
	.4byte	.LASF91
	.byte	0x1
	.byte	0x3
	.4byte	0xb8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0xe
	.ascii	"ptr\000"
	.byte	0x1
	.byte	0x3
	.4byte	0x742
	.uleb128 0x2
	.byte	0x91
	.sleb128 -36
	.uleb128 0xd
	.4byte	.LASF92
	.byte	0x1
	.byte	0x3
	.4byte	0xa2
	.uleb128 0x2
	.byte	0x91
	.sleb128 -28
	.uleb128 0xf
	.4byte	.LASF93
	.byte	0x1
	.byte	0x5
	.4byte	0x748
	.uleb128 0x2
	.byte	0x91
	.sleb128 -20
	.uleb128 0xf
	.4byte	.LASF94
	.byte	0x1
	.byte	0x6
	.4byte	0xb8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -12
	.uleb128 0x10
	.4byte	.LBB2
	.4byte	.LBE2-.LBB2
	.uleb128 0xf
	.4byte	.LASF91
	.byte	0x1
	.byte	0x75
	.4byte	0xb8
	.uleb128 0x2
	.byte	0x91
	.sleb128 -16
	.byte	0
	.byte	0
	.uleb128 0x11
	.byte	0x4
	.4byte	0xa2
	.uleb128 0x6
	.4byte	0x97
	.4byte	0x758
	.uleb128 0x7
	.4byte	0xc3
	.byte	0x3
	.byte	0
	.uleb128 0x12
	.4byte	.LASF95
	.byte	0x5
	.2byte	0x51b
	.4byte	0xe9
	.uleb128 0x13
	.4byte	.LASF96
	.byte	0x6
	.byte	0x14
	.4byte	0xdf
	.uleb128 0x5
	.byte	0x3
	.4byte	dev_addr
	.byte	0
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0
	.4byte	0
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.section	.debug_str,"MS",%progbits,1
.LASF51:
	.ascii	"AHB1LPENR\000"
.LASF75:
	.ascii	"CCER\000"
.LASF22:
	.ascii	"LISR\000"
.LASF29:
	.ascii	"OSPEEDR\000"
.LASF99:
	.ascii	"/home/anes/projekat\000"
.LASF41:
	.ascii	"APB1RSTR\000"
.LASF45:
	.ascii	"AHB2ENR\000"
.LASF85:
	.ascii	"DMAR\000"
.LASF21:
	.ascii	"DMA_Stream_TypeDef\000"
.LASF73:
	.ascii	"CCMR1\000"
.LASF74:
	.ascii	"CCMR2\000"
.LASF2:
	.ascii	"short int\000"
.LASF17:
	.ascii	"sizetype\000"
.LASF58:
	.ascii	"BDCR\000"
.LASF61:
	.ascii	"PLLI2SCFGR\000"
.LASF4:
	.ascii	"__uint16_t\000"
.LASF8:
	.ascii	"__uint32_t\000"
.LASF44:
	.ascii	"AHB1ENR\000"
.LASF26:
	.ascii	"DMA_TypeDef\000"
.LASF60:
	.ascii	"SSCGR\000"
.LASF65:
	.ascii	"TXCRCR\000"
.LASF37:
	.ascii	"AHB1RSTR\000"
.LASF36:
	.ascii	"CFGR\000"
.LASF53:
	.ascii	"AHB3LPENR\000"
.LASF13:
	.ascii	"uint8_t\000"
.LASF48:
	.ascii	"APB1ENR\000"
.LASF28:
	.ascii	"OTYPER\000"
.LASF91:
	.ascii	"sample_rate\000"
.LASF46:
	.ascii	"AHB3ENR\000"
.LASF25:
	.ascii	"HIFCR\000"
.LASF10:
	.ascii	"long long int\000"
.LASF78:
	.ascii	"CCR1\000"
.LASF79:
	.ascii	"CCR2\000"
.LASF80:
	.ascii	"CCR3\000"
.LASF81:
	.ascii	"CCR4\000"
.LASF62:
	.ascii	"RCC_TypeDef\000"
.LASF9:
	.ascii	"long unsigned int\000"
.LASF27:
	.ascii	"MODER\000"
.LASF42:
	.ascii	"APB2RSTR\000"
.LASF3:
	.ascii	"__uint8_t\000"
.LASF86:
	.ascii	"RESERVED13\000"
.LASF23:
	.ascii	"HISR\000"
.LASF88:
	.ascii	"TIM_TypeDef\000"
.LASF71:
	.ascii	"SMCR\000"
.LASF1:
	.ascii	"unsigned char\000"
.LASF77:
	.ascii	"RESERVED10\000"
.LASF83:
	.ascii	"RESERVED11\000"
.LASF84:
	.ascii	"RESERVED12\000"
.LASF49:
	.ascii	"APB2ENR\000"
.LASF87:
	.ascii	"RESERVED14\000"
.LASF0:
	.ascii	"signed char\000"
.LASF11:
	.ascii	"long long unsigned int\000"
.LASF16:
	.ascii	"uint32_t\000"
.LASF66:
	.ascii	"I2SCFGR\000"
.LASF12:
	.ascii	"unsigned int\000"
.LASF38:
	.ascii	"AHB2RSTR\000"
.LASF14:
	.ascii	"uint16_t\000"
.LASF96:
	.ascii	"dev_addr\000"
.LASF72:
	.ascii	"DIER\000"
.LASF35:
	.ascii	"PLLCFGR\000"
.LASF5:
	.ascii	"short unsigned int\000"
.LASF55:
	.ascii	"APB1LPENR\000"
.LASF20:
	.ascii	"M1AR\000"
.LASF89:
	.ascii	"char\000"
.LASF7:
	.ascii	"long int\000"
.LASF82:
	.ascii	"BDTR\000"
.LASF15:
	.ascii	"int32_t\000"
.LASF70:
	.ascii	"SPI_TypeDef\000"
.LASF68:
	.ascii	"I2SPR\000"
.LASF97:
	.ascii	"GNU C 4.9.3 20141119 (release) [ARM/embedded-4_9-br"
	.ascii	"anch revision 218278] -mlittle-endian -mthumb -mcpu"
	.ascii	"=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu"
	.ascii	"=fpv4-sp-d16 -g -O0 -fsingle-precision-constant\000"
.LASF19:
	.ascii	"M0AR\000"
.LASF93:
	.ascii	"data\000"
.LASF52:
	.ascii	"AHB2LPENR\000"
.LASF63:
	.ascii	"CRCPR\000"
.LASF24:
	.ascii	"LIFCR\000"
.LASF40:
	.ascii	"RESERVED0\000"
.LASF43:
	.ascii	"RESERVED1\000"
.LASF47:
	.ascii	"RESERVED2\000"
.LASF50:
	.ascii	"RESERVED3\000"
.LASF54:
	.ascii	"RESERVED4\000"
.LASF57:
	.ascii	"RESERVED5\000"
.LASF59:
	.ascii	"RESERVED6\000"
.LASF67:
	.ascii	"RESERVED7\000"
.LASF69:
	.ascii	"RESERVED8\000"
.LASF76:
	.ascii	"RESERVED9\000"
.LASF98:
	.ascii	"cs43l22.c\000"
.LASF95:
	.ascii	"ITM_RxBuffer\000"
.LASF92:
	.ascii	"size\000"
.LASF94:
	.ascii	"utmp32\000"
.LASF18:
	.ascii	"NDTR\000"
.LASF100:
	.ascii	"initCS43L22\000"
.LASF6:
	.ascii	"__int32_t\000"
.LASF90:
	.ascii	"volume\000"
.LASF30:
	.ascii	"PUPDR\000"
.LASF34:
	.ascii	"GPIO_TypeDef\000"
.LASF32:
	.ascii	"BSRRH\000"
.LASF39:
	.ascii	"AHB3RSTR\000"
.LASF31:
	.ascii	"BSRRL\000"
.LASF33:
	.ascii	"LCKR\000"
.LASF56:
	.ascii	"APB2LPENR\000"
.LASF64:
	.ascii	"RXCRCR\000"
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 4.9.3 20141119 (release) [ARM/embedded-4_9-branch revision 218278]"
